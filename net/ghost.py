#!/usr/bin/env python3
'''HTTP proxy serving local files for multiple domains.
Useful for serving a snapshot of the Internet.
A ghost of the Internet, here to haunt, dead or alive.
There are some special handling for Macromedia Flash
that can be turned off.
(Not calling it Adobe's since they're going out of their way
to bury it, so as to honor their wish to be disassociated.)

This is based on the approach that BlueMaxima's Flashpoint uses,
as detailed in their wiki's Linux support page:
https://bluemaxima.org/flashpoint/datahub/Linux_Support#Technologies
Whether by design or not, some Flash games require 
network to be available and/or be at a specific URL;
this provides a fully offline version of the "net";
all that needs to be done is to set up a directory
that has all the files that this "net" has.
Finally, an "Internet" that is under one's control!

If going for the full original experience on Linux
without using a proxy, try using Peppermint 10 Respin.
Its Firefox is ready-to-go with full Flash support,
without the time bomb in the later Flash versions.

More information regarding Flash:
https://gitlab.com/serenascopycat/grub-config/-/wikis/sysusage#macromedia-flash

Note that this implementation is only meant
to be used in trusted environments.
There is no hardening against evil maids,
hacking from the Internet or anything else.
In fact, may poison the browser cache.

HOWTO
1) Set up files in filesystem
   Set up a directory where each directory is a domain
      each domain's directory should have the full path layout for files
   e.g. (root)/example.com/subpath/file.txt
   Tip: Acquire a HAR (HTTP archive) from a browser's network inspector,
      then extract with https://github.com/dead-beef/har-extractor,
      being careful of Firefox capping files larger than 1MiB
2) Configure this script (optional)
   adjust port number (near end of script)
   adjust base path to be the directory containing the domain subdirectories
      GhostHTTPOfflineServerTransplantRequestDelegate.local_root
      optional: can also be specified
        via LOCAL_ROOT environment variable
        via command line
3) Run this script
   LOCAL_ROOT can be specified in one of the following ways:
     LOCAL_ROOT environment variable
     command line argument
     current working directory
   "-h" or "--help" for usage help
4) Configure apps to use HTTP proxy
   HTTPS and SOCKS proxy not supported
      HTTPS requires SSL certificates (probably dynamic certificate generation)
      workarounds
         change over to HTTP
         HTTPS can be added but left as an exercise for the user
            not about to add untested code for people to create issues for
         use this script as a mitmproxy addon
            pass '-s path/to/ghost.py' to mitmproxy
            add '--set upstream_cert=false' option
               avoid attempting to contact actual server to spoof certificate
               https://github.com/mitmproxy/mitmproxy/issues/3203#issuecomment-397799261
   in Linux, can do:
      http_proxy=localhost:8128 /path/to/bin/flashplayer http://example.com/example.swf
   in Windows, either the application supports own proxy configuration, or it uses system proxy
      may be able to configure a different user account and run app as that user
   may use browser extensions that allows for site-specific proxies
      e.g. FoxyProxy (?) (not tested, but sounds like it'll do)

Copycenter 2022 Serena's Copycat; licensed under CC0
'''

__version__ = '0.5.1'

import http.server
import io
import mimetypes
import os
import os.path
import re
import shutil
import urllib.parse

try:
    # https://docs.python.org/3.5/library/http.html#http.HTTPStatus
    # Introduced in Python 3.5
    from http import HTTPStatus as httpstatus
except ImportError:
    # For 3.4 or older (e.g. on Knoppix 7.6.1's Python 3.4.4)
    import http.client as httpstatus

class lazyevalprop(object):
    '''Decorates a function that derives the value of the field for which the function is named.
    Caches the derived value (lazy evaluation) into the __dict__ of the instance,
    so that, with this descriptor being non-data, the cached value has higher priority and
    is thus returned first.
    '''
    # This is a cheapo polyfill of @functools.cached_property introduced in Python 3.8,
    # but this must work in Python 3.6.9 (included in Peppermint 10 respin).
    def __init__(self, func):
        self._func = func
    def __set_name__(self, owner, name):
        self._targetfield = name
    def set_name_compat(self, owner, name):
        '''Conditionally __set_name__ if not previously automatically handled
        by Python 3.6.
        '''
        if not hasattr(self, "_targetfield") or self._targetfield is None:
            self.__set_name__(owner, name)
    def __get__(self, obj, objtype=None):
        result = self._func(obj)
        obj.__dict__[self._targetfield] = result
        return result
    @staticmethod
    def set_names(targetClass:type):
        '''Scans a class for lazyevalprop descriptors and
        calls their set_name_compat method,
        for older Python versions which do not call __set_name__.
        '''
        for p in targetClass.__dict__:
            fielddesc = targetClass.__dict__[p]
            if isinstance(fielddesc, lazyevalprop):
                fielddesc.set_name_compat(targetClass, p)
    @staticmethod
    def usedin(targetClass:type):
        lazyevalprop.set_names(targetClass)
        return targetClass

@lazyevalprop.usedin
class URLAccess(object):
    '''Access various components of a resource identifier.
    This is limited to the scope of use by ghost.
    Façade to various implementations, caching the accessed results.
    '''

    class Source(object):
        '''Abstract URL component provider.
        Should provide at least the server and resource, possibly via complete.
        '''
        SRC_FAST = 'fast'   # source variant for accessing most readily available result
        SRC_URL = 'url' # source variant for accessing from URL string
        SRC_REQ = 'request' # source variant for accessing from HTTP request
        def asvariant(self, variant:str):
            return self

    def __init__(self, source):
        self._source = source

    @lazyevalprop
    def source(self):
        return self._source
    def asvariant(self, variant:str):
        source = self.source
        sourcevariant = source.asvariant(variant)
        return None if sourcevariant is None else \
            self if sourcevariant is source else URLAccess(sourcevariant)

    @lazyevalprop
    def scheme(self):
        '''Get the scheme in URL.'''
        src = self.source
        return getattr(src, 'scheme', None) or \
            self.parsed.scheme
    @lazyevalprop
    def server(self):
        '''Get the server, which includes hostname and port, in URL.'''
        src = self.source
        return getattr(src, 'server', None) or \
            getattr(src, 'hostname', None) or \
            self.parsed.server
    @lazyevalprop
    def hostname(self):
        '''Get the hostname portion of the server, without port, in URL.'''
        src = self.source
        servertohostname = lambda server: server.split(':',1)[0] if server else None
        return getattr(src, 'hostname', None) or \
            servertohostname(getattr(src, 'server', None)) or \
            self.parsed.hostname
    @lazyevalprop
    def resource(self):
        '''Get the portion of the URL after the server.'''
        src = self.source
        return getattr(src, 'resource', None) or \
            getattr(src, 'path', None) or \
            self.parsed.path
    @lazyevalprop
    def path(self):
        '''Get the path portion of the URL, without query or whatever else.'''
        return getattr(self.source, 'path', None) or self.parsed.path
    @lazyevalprop
    def query(self):
        '''Get the query portion of the URL, without leading '?'.'''
        return getattr(self.source, 'query', None) or self.parsed.query
    @lazyevalprop
    def complete(self):
        '''Get the complete [possibly canonicalized] URL.'''
        src = self.source
        return getattr(src, 'complete', None) or ('http://%s/%s' %
            (self.server, self.resource.removeprefix('/')))

    @lazyevalprop
    def parsed(self):
        src = self.source
        for a in ('complete', 'resource', 'path'):
            url = getattr(src, a, None)
            if url: return urllib.parse.urlparse(url)
        raise NotImplementedError('No supported data available from source')

class URLAccessHTTPReqHandler(URLAccess.Source):
    def __init__(self, handler:http.server.BaseHTTPRequestHandler):
        self._handler = handler
    def asvariant(self, variant:str):
        return self if URLAccess.Source.SRC_URL == variant \
            else URLAccessHTTPReqHandlerPreferHeader(self._handler) if URLAccess.Source.SRC_FAST == variant or URLAccess.Source.SRC_REQ == variant else None
    @property
    def complete(self):
        # When HTTP server is acting as proxy, the resource is the full URL
        return self._handler.path
    @staticmethod
    def asaccess(handler:http.server.BaseHTTPRequestHandler):
        return URLAccess(URLAccessHTTPReqHandler(handler))

class URLAccessHTTPReqHandlerPreferHeader(URLAccessHTTPReqHandler):
    def __init__(self, handler:http.server.BaseHTTPRequestHandler):
        self._handler = handler
    def asvariant(self, variant:str):
        return self if URLAccess.Source.SRC_FAST == variant or URLAccess.Source.SRC_REQ == variant \
            else URLAccessHTTPReqHandler(self._handler) if URLAccess.Source.SRC_URL == variant else None
    @property
    def server(self):
        return self._handler.headers.get('host', None)
    @staticmethod
    def asaccess(handler:http.server.BaseHTTPRequestHandler):
        return URLAccess(URLAccessHTTPReqHandler(handler))

@lazyevalprop.usedin
class URLPathToMIMEMapper(object):
    '''Maps a URL path within a site to a MIME type.'''
    class MIMERule(object):
        '''A match/except rule resulting in the specified MIME type when matched.'''
        def __init__(self, mime, predicatelist:list):
            self._mime = mime
            self._predicatelist = predicatelist
        def get_mime(self):
            return self._mime
        def resolve(self, predicateretriever):
            URLPathToMIMEMapper.RulePredicateInclude.resolvepredicatesinlist(self._predicatelist, predicateretriever)
        def is_match(self, urlAccess:URLAccess):
            for predicate in self._predicatelist:
                if not predicate.is_match(urlAccess):
                    print('Failed to match on %s' % self._mime)
                    return False
            return True
    class RulePredicateInverse(object):
        '''Invert the referenced match/except'''
        def __init__(self, invertpredicate):
            self._target = invertpredicate
        def is_match(self, urlAccess:URLAccess):
            return not self._target.is_match(urlAccess)
    class RulePredicateBase(object):
        '''A match/except spec'''
        def __init__(self, componentretriever):
            self._componentretriever = componentretriever
        def is_match(self, urlAccess:URLAccess):
            return self.is_match_component(self._componentretriever(urlAccess))
        def is_match_component(self, component:str):
            return False
    @lazyevalprop.usedin
    class RulePredicateRegex(RulePredicateBase):
        '''A match/except spec (regex version)'''
        def __init__(self, componentretriever, spec):
            super().__init__(componentretriever)
            self._match = spec
        @lazyevalprop
        def matchRegex(self):
            return re.compile(self._match)
        def is_match_component(self, component:str):
            return self.matchRegex.search(component)
    class RulePredicatePrefix(RulePredicateBase):
        '''A match/except spec (filename/path prefix version)'''
        def __init__(self, componentretriever, spec):
            super().__init__(componentretriever)
            self._match = spec or ''
        def is_match_component(self, component:str):
            return component.startswith(self._match)
    class RulePredicateDirPrefix(RulePredicateBase):
        '''A match/except spec (filename/path directory prefix version)'''
        def __init__(self, componentretriever, spec):
            super().__init__(componentretriever)
            self._match = (('' if spec.startswith('/') else '/') + spec.strip('/')) if spec else ''
        def is_match_component(self, component:str):
            if component.startswith(self._match):
                componentlen = len(component)
                matchlen = len(self._match)
                return componentlen == matchlen or '/' == component[matchlen]
            else:
                return False
    class RulePredicateSuffix(RulePredicateBase):
        '''A match/except spec (filename/path suffix version)'''
        def __init__(self, componentretriever, spec):
            super().__init__(componentretriever)
            self._match = spec or ''
        def is_match_component(self, component:str):
            return component.endswith(self._match)
    class RulePredicateSubstr(RulePredicateBase):
        '''A match/except spec (filename/path substring version)'''
        def __init__(self, componentretriever, spec):
            super().__init__(componentretriever)
            self._match = spec or ''
        def is_match_component(self, component:str):
            return self._match in component
    class RulePredicateInclude(object):
        '''Reference another predicate'''
        def __init__(self, componentretriever, name):
            self._name = name
        def is_match(self, urlAccess:URLAccess):
            raise NotImplementedError("Unresolved predicate %s" % self._name)
        @staticmethod
        def try_resolve(predicate, predicateretriever):
            return predicateretriever(predicate._name) if isinstance(predicate, URLPathToMIMEMapper.RulePredicateInclude) else predicate
        @staticmethod
        def resolvepredicatesinlist(predicatelist, predicateretriever):
            poffset = 0 # positions to shift for list size change
            for p in range(0, len(predicatelist)):
                p += poffset
                predicate = predicatelist[p]
                resolved = URLPathToMIMEMapper.RulePredicateInclude.try_resolve(predicate, predicateretriever)
                if resolved is predicate or resolved is None:
                    pass
                elif isinstance(resolved, list):
                    predicatelist[p:p+1] = resolved
                    poffset += len(resolved) - 1
                else:
                    predicatelist[p] = resolved
        @staticmethod
        def try_getname(predicate):
            return predicate._name if isinstance(predicate, URLPathToMIMEMapper.RulePredicateInclude) else None
    @staticmethod
    def _named_token(component, arg):
        return arg

    class MatchExceptParser(object):
        class PredicateParseState(object):
            def _token_not_action(self):
                self._inverse = True
            def _token_include_action(self):
                self._method = URLPathToMIMEMapper.RulePredicateInclude
            def _token_name_action(self):
                self._method = URLPathToMIMEMapper._named_token
            def _token_regex_action(self):
                self._method = URLPathToMIMEMapper.RulePredicateRegex
            def _token_prefix_action(self):
                self._method = URLPathToMIMEMapper.RulePredicatePrefix
            def _token_dirprefix_action(self):
                self._method = URLPathToMIMEMapper.RulePredicateDirPrefix
            def _token_suffix_action(self):
                self._method = URLPathToMIMEMapper.RulePredicateSuffix
            def _token_substr_action(self):
                self._method = URLPathToMIMEMapper.RulePredicateSubstr
            def _token_path_action(self):
                self._component = URLPathToMIMEMapper.MatchExceptParser._componentpath
            def _token_complete_action(self):
                self._component = URLPathToMIMEMapper.MatchExceptParser._componentcomplete

            _matchsep = re.compile('[,;:/&]?\s+')
            _matchtokens = re.compile('[Rr]egexp?|(?:[Dd]ir)?[Pp]refix|[Ss]uffix|[Ss]ubstr(?:ing)?|[Nn]ot|[Pp]ath|[Cc]omplete|[Ii]nclude|[Nn]ame')
            _tokenactions = {
                'path': _token_path_action,
                'complete': _token_complete_action,
                'regex': _token_regex_action,
                'regexp': _token_regex_action,
                'dirprefix': _token_dirprefix_action,
                'prefix': _token_prefix_action,
                'suffix': _token_suffix_action,
                'substr': _token_substr_action,
                'substring': _token_substr_action,
                'not': _token_not_action,
                'include': _token_include_action,
                'name': _token_name_action,
            }
            def __init__(self):
                self._method = None
                self._component = URLPathToMIMEMapper.MatchExceptParser._componentpath
                self._inverse = False
            @staticmethod
            def skipseparator(matchexcept, cur):
                match = URLPathToMIMEMapper.MatchExceptParser.PredicateParseState._matchsep.match(matchexcept, cur)
                return match.end() if match else cur
            @staticmethod
            def gettoken(matchexcept, cur):
                return URLPathToMIMEMapper.MatchExceptParser.PredicateParseState._matchtokens.match(matchexcept, cur)
            def handletoken(self, token):
                URLPathToMIMEMapper.MatchExceptParser.PredicateParseState._tokenactions[token](self)
            def completepredicate(self, arg):
                if self._method is None:
                    raise ValueError('No matching method specified')
                else:
                    predicate = self._method(self._component, arg)
                    if self._inverse: predicate = URLPathToMIMEMapper.RulePredicateInverse(predicate)
                    return predicate

        @staticmethod
        def _componentcomplete(urlAccess):
            return urlAccess.complete
        @staticmethod
        def _componentpath(urlAccess):
            return urlAccess.path

        def parse(self, matchexcept):
            self._cur = 0
            self._matchexcept = matchexcept
            return self
        def __iter__(self):
            return self
        def __next__(self):
            if self._cur >= len(self._matchexcept): raise StopIteration()
            pstate = URLPathToMIMEMapper.MatchExceptParser.PredicateParseState()
            self._cur = pstate.skipseparator(self._matchexcept, self._cur)
            while self._cur < len(self._matchexcept):
                matched = pstate.gettoken(self._matchexcept, self._cur)
                if matched:
                    pstate.handletoken(matched.group(0).lower())
                    self._cur = matched.end()
                else:
                    end = self._matchexcept.find(self._matchexcept[self._cur], self._cur + 1)
                    if 0 > end: break   # can't find end of argument
                    arg = self._matchexcept[self._cur+1:end]
                    self._cur = end + 1
                    return pstate.completepredicate(arg)
            raise ValueError('Match/except spec terminated early')

    def __init__(self):
        self._rules = []

    @lazyevalprop
    def defaultparser(self):
        return URLPathToMIMEMapper.MatchExceptParser()
    def intern_rule(self, mime, matchexcept, registerpredicate):
        predicatelist = []
        name = None
        for p in self.defaultparser.parse(matchexcept):
            if isinstance(p, str):
                name = p
            else:
                predicatelist.append(p)
                if name is not None:
                    registerpredicate(name, p)
                    name = None
        if name is not None: registerpredicate(name, predicatelist)
        self._rules.append(URLPathToMIMEMapper.MIMERule(mime, predicatelist))
    def resolve(self, predicateretriever):
        for r in self._rules:
            r.resolve(predicateretriever)
    def get(self, urlAccess:URLAccess):
        for r in self._rules:
            if r.is_match(urlAccess): return r.get_mime()
        return None

class URLToMIMEMapper(object):
    '''Maps a URL to a MIME type.'''
    def __init__(self):
        self._sites = dict()
        self._nameregistry = {}

    def registerpredicate(self, name, predicate):
        if name in self._nameregistry: raise RuntimeError("Name %s reused to refer to predicate." % name)
        self._nameregistry[name] = predicate
    def retrievepredicate(self, name):
        return self._nameregistry[name]

    def intern_rules(self, mimetabtextstream):
        for line in mimetabtextstream:
            linecore = line.lstrip()
            if not linecore or "#" == linecore[0]: continue
            fields = line.rstrip("\r\n").split(maxsplit=2)
            if fields[0] not in self._sites: self._sites[fields[0]] = URLPathToMIMEMapper()
            self._sites[fields[0]].intern_rule(fields[1], fields[2], self.registerpredicate)
    def resolve_named_predicates(self):
        unresolvedlists = {}
        resolvednames = set()
        for n, p in self._nameregistry.items():
            if isinstance(p, list):
                referencednames = set()
                for predicate in p:
                    name = URLPathToMIMEMapper.RulePredicateInclude.try_getname(p)
                    if name is not None: referencednames.add(name)
                if 0 == len(referencednames):
                    resolvednames.add(n)
                else:
                    unresolvedlists[n] = referencednames
            else:
                while True:
                    resolved = URLPathToMIMEMapper.RulePredicateInclude.try_resolve(p, self.retrievepredicate)
                    if resolved is p or resolved is None:
                        break
                    else:
                        p = resolved
                self._nameregistry[n] = p
                resolvednames.add(n)
        while 0 < len(unresolvedlists):
            resolvednamescount = len(resolvednames)
            for n, refs in unresolvedlists.items():
                if refs <= resolvednames:
                    URLPathToMIMEMapper.RulePredicateInclude.resolvepredicatesinlist(self._nameregistry[n], self.retrievepredicate)
                    resolvednames.add(n)
            for n in resolvednames:
                if n in unresolvedlists: del unresolvedlists[n]
            if resolvednamescount == len(resolvednames):
                raise RuntimeError("Unresolved names %s" % ','.join(unresolvedlists.keys()))
    def resolve_rule_predicates(self):
        for r in self._sites.values():
            r.resolve(self.retrievepredicate)
    def process_rules(self, mimetabtextstream):
        self.intern_rules(mimetabtextstream)
        self.resolve_named_predicates()
        self.resolve_rule_predicates()
    def get(self, urlAccess:URLAccess):
        for d in DomainSpecificRequestDelegate.domain_match_iter(urlAccess.hostname):
            if d in self._sites:
                mime = self._sites[d].get(urlAccess)
                if mime is not None: return mime
        return None

class IONone(io.IOBase):
    '''No-op I/O.
    Slightly different from blackhole I/O or os.devnull in that no data is taken.'''

    def read(self, *_):
        return None

    def write(self, *_):
        return 0    # no bytes written

class GhostHTTPOfflineServerTransplantRequestDelegate(object):
    '''Serves files from local filesystem.'''

    class FuncHeaderMode(object):
        '''Use a function to override generated headers.'''
        def __init__(self, fn):
            self._fn = fn
        def create_header_dict(self):
            return {}
        def should_add_header(self, headername):
            return True
        def finalize_header_dict(self, urlaccess:URLAccess, localpath, header):
            fn(urlaccess, localpath, header)

    class DictHeaderMode(object):
        '''Use a dict of fixed overriding headers.'''
        def __init__(self, headerdict):
            '''headerdict is an immutable dict of headers'''
            self._headers = headerdict or {}
            # facilitates looking up case-insensitive header names
            self._names = GhostHTTPOfflineServerTransplantRequestDelegate.DictHeaderMode.as_lower_names(self._headers)
        @staticmethod
        def as_lower_names(headerdict):
            return set(map(lambda n: n.lower(), headerdict.keys()))
        def create_header_dict(self):
            return dict(self._headers)
        def should_add_header(self, headername):
            return headername.lower() not in self._names
        def finalize_header_dict(self, urlaccess:URLAccess, localpath, header):
            pass

    class MIMEHeader(object):
        def __init__(self, nextheader):
            self._nextheader = nextheader
            self._mime = URLToMIMEMapper()
        def intern_rules(self, mimetabtextstream):
            self._mime.intern_rules(mimetabtextstream)
        def create_header_dict(self):
            return self._nextheader.create_header_dict()
        def should_add_header(self, headername):
            # add Content-Type always, in case none of the rules match
            return 'content-type' == headername.lower() or self._nextheader.should_add_header(headername)
        def finalize_header_dict(self, urlaccess:URLAccess, localpath, header):
            mime = self._mime.get(urlaccess)
            print("got mime %s" % mime)
            if mime is not None: header['Content-Type'] = mime
            self._nextheader.finalize_header_dict(urlaccess, localpath, header)

    def __init__(self, local_path=(os.getenv(key='LOCALROOT') or os.getcwd()), http_headers_for_files=None):
        '''local_path is either
          a function that takes a parsed URL and HTTP headers and maps that to a local path,
          or a local filesystem path string that is the root from which to map URLs with,
          adding on the hostname and the URL path.
        http_headers_for_files is a dict of extra headers to send to the client when files are served.'''
        self._get_local_path = local_path if callable(local_path) \
            else GhostHTTPOfflineServerTransplantRequestDelegate.get_full_local_path_retriever(
                local_path or os.getenv(key='LOCALROOT') or os.getcwd())
        self._http_headers_for_files = \
            GhostHTTPOfflineServerTransplantRequestDelegate.DictHeaderMode(http_headers_for_files) \
                if http_headers_for_files is None \
            else GhostHTTPOfflineServerTransplantRequestDelegate.FuncHeaderMode(http_headers_for_files) \
                if callable(http_headers_for_files) \
            else http_headers_for_files \
                if callable(getattr(http_headers_for_files, 'create_header_dict', None)) and \
                    callable(getattr(http_headers_for_files, 'should_add_header', None)) and \
                    callable(getattr(http_headers_for_files, 'finalize_header_dict', None)) \
            else GhostHTTPOfflineServerTransplantRequestDelegate.DictHeaderMode(http_headers_for_files)

    @staticmethod
    def get_full_local_path_retriever(local_root):
        result = lambda urlAccess: \
            GhostHTTPOfflineServerTransplantRequestDelegate.finalize_local_path_from(
                GhostHTTPOfflineServerTransplantRequestDelegate.map_local_full_path_from(
                    local_root, urlAccess),
                urlAccess.query)
        result.local_root = local_root
        result.description = 'Serving files from ' + local_root
        return result

    @staticmethod
    def get_flat_local_path_retriever(local_root):
        result = lambda urlAccess: \
            GhostHTTPOfflineServerTransplantRequestDelegate.finalize_local_path_from(
                GhostHTTPOfflineServerTransplantRequestDelegate.map_local_flat_path_from(
                    local_root, urlAccess),
                urlAccess.query)
        result.local_root = local_root
        result.description = 'Serving files directly under ' + local_root
        return result

    @staticmethod
    def map_local_full_path_from(localroot, urlAccess:URLAccess):
        '''Map a URL path into a local filesystem path.
        localroot is the base directory where the paths are mapped to.
        Path will be built from localroot using the hostname and the URL path.'''
        # when acting as proxy, path is given as a full URL
        # so use parseresult instead of self.path for just the URL path segment
        return os.path.join(localroot, urlAccess.hostname,
                *urllib.parse.unquote(urlAccess.path).split('/'))

    @staticmethod
    def map_local_flat_path_from(localroot, urlAccess:URLAccess):
        '''Map a URL path into a local filesystem path.
        localroot is the base directory where the paths are mapped to.
        All URLs will map to files directly under localroot,
        so all sites and all paths will serve up the same set of files.'''
        return os.path.join(localroot, urllib.parse.unquote(urlAccess.path).rsplit('/', 1)[-1])

    @staticmethod
    def finalize_local_path_from(localpath, query):
        # On systems that support a less stringent set of filename chars,
        # look for a file named with the same query and use that
        if query:
            pathwithquery = localpath + '?' + query
            if os.path.exists(pathwithquery): return pathwithquery
        return localpath

    def describe_get_local_path(self):
        return self._get_local_path.description \
            if hasattr(self._get_local_path, 'description') \
            else 'Mapping files via ' + repr(self._get_local_path)

    def get_local_path(self, urlAccess:URLAccess):
        return self._get_local_path(urlAccess)

    mime_by_suffix = {
        'html': 'text/html',
        'htm': 'text/html',
    }
    @staticmethod
    def get_MIME_type_of(urlpath, localpath):
        # Need to serve HTML pages to host the SWF object for browser
        # browser depends on MIME to render HTML
        # drop queries: guess_type is unaware of queries and will include that
        result, _ = mimetypes.guess_type(urlpath, strict=False)
        if not result:
            parts = urlpath.rpartition('.')
            lastpart = parts[-1]
            result = lastpart and '/' not in lastpart and \
                (lastpart in GhostHTTPOfflineServerTransplantRequestDelegate.mime_by_suffix) and \
                GhostHTTPOfflineServerTransplantRequestDelegate.mime_by_suffix[lastpart] or \
                'application/octet-stream'
        return result

    @staticmethod
    def respond_not_found(reqhandler):
        '''Send a complete response indicating the target is not found.'''
        reqhandler.send_response(httpstatus.NOT_FOUND)
        reqhandler.send_header('Content-Type', 'text/plain')
        reqhandler.end_headers()
        reqhandler.write_response_text('File does not exist.')

    @staticmethod
    def respond_unreadable(reqhandler):
        '''Send a complete response indicating the target is not readable.'''
        reqhandler.send_response(httpstatus.FORBIDDEN)
        reqhandler.send_header('Content-Type', 'text/plain')
        reqhandler.end_headers()
        reqhandler.write_response_text('Read access denied.')

    @staticmethod
    def respond_with_file(reqhandler, localpath, headersmode):
        '''Send a complete response for a file, whether it can be accessed or not.'''
        try:
            filehandle = open(localpath, 'rb')
        except OSError:
            reqhandler.send_response(httpstatus.INTERNAL_SERVER_ERROR)
            reqhandler.send_header('Content-Type', 'text/plain')
            reqhandler.end_headers()
            reqhandler.write_response_text('Cannot open file.')
            return
        with filehandle:
            reqhandler.send_response(httpstatus.OK)
            headersdict = headersmode.create_header_dict()
            if headersmode.should_add_header('Content-Type'):
                headersdict['Content-Type'] = GhostHTTPOfflineServerTransplantRequestDelegate.\
                    get_MIME_type_of(reqhandler.urlaccess.path, localpath)
            try:
                statresult = os.fstat(filehandle.fileno())
                if headersmode.should_add_header('Content-Length'):
                    headersdict['Content-Length'] = str(statresult.st_size)
                if headersmode.should_add_header('Last-Modified'):
                    headersdict['Last-Modified'] = reqhandler.date_time_string(statresult.st_mtime)
            except:
                pass    # not show-stopper to be missing these headers
            headersmode.finalize_header_dict(reqhandler.urlaccess, localpath, headersdict)
            for n,v in headersdict.items():
                reqhandler.send_header(n, v)
            reqhandler.end_headers()
            shutil.copyfileobj(filehandle, reqhandler.response_content)

    def respond_for_dir(self, reqhandler, localpath):
        GhostHTTPOfflineServerTransplantRequestDelegate.respond_unreadable(reqhandler)

    def respond_for_file(self, reqhandler, localpath):
        GhostHTTPOfflineServerTransplantRequestDelegate.respond_with_file(
            reqhandler, localpath, headersmode=self._http_headers_for_files)

    def handle_available_request(self, reqhandler):
        '''Request delegate function: serve only files in the local filesystem.'''
        return self.handle_request(reqhandler, completemissing=False)

    def handle_request(self, reqhandler, completemissing=True, completedir=True):
        '''Request delegate function: serves file URLs or sends error responses.
        Set completemissing to True to send error response for missing files.
        Set completedir to True to send error response for directories.
        For both options, when not set, send no error but return False instead
        to indicate the next request delegate should take a swing instead.'''
        localpath = self.get_local_path(reqhandler.urlaccess)
        if not os.path.exists(localpath):
            if completemissing:
                reqhandler.log_message('Missing %s', localpath)
                GhostHTTPOfflineServerTransplantRequestDelegate.respond_not_found(reqhandler)
            return completemissing
        elif os.path.isdir(localpath):
            if completedir:
                reqhandler.log_message('Directory %s', localpath)
                self.respond_for_dir(reqhandler, localpath)
            return completedir
        else:
            reqhandler.log_message('About to serve %s', localpath)
            self.respond_for_file(reqhandler, localpath)
            return True

@lazyevalprop.usedin
class DelegatingRequestHandler(http.server.BaseHTTPRequestHandler):
    '''Handler for HTTP proxy, configurable to conditionally handle things differently.
    Delegates to "request delegate" functions in the delegates list.
    Request delegate functions take in this request handler as parameter
    and return whether it has responded to the request.
    Unlike BaseHTTPRequestHandler which requires all request methods to be explicitly implemented,
    This falls back to an implementation.
    '''

    delegates = [GhostHTTPOfflineServerTransplantRequestDelegate().handle_request]
    _null_io = IONone()

    # https://stackoverflow.com/questions/2405590/how-do-i-override-getattr-without-breaking-the-default-behavior/31944601#31944601
    # override so do_* methods defaults to handle_request_method
    # should at least cover HEAD, GET and POST, and maybe case-insensitive variants
    def __getattr__(self, name):
        return self.handle_request_method if name.startswith("do_") else super().__getattribute__(name)

    server_version = 'GHOST/' + __version__

    def version_string(self):
        # don't say too much
        return self.server_version

    @lazyevalprop
    def urlaccess(self):
        return URLAccessHTTPReqHandler.asaccess(self)

    @property
    def response_content(self):
        '''Output file-like object that sends body if requested by HTTP method.'''
        return self._null_io if 'HEAD' == self.command else self.wfile

    def write_response_text(self, stringtowrite, encoding='utf-8'):
        self.response_content.write(str(stringtowrite).encode(encoding));

    def unhandled_request_method(self):
        self.send_response(httpstatus.NOT_IMPLEMENTED)
        self.send_header('Content-Type', 'text/plain')
        self.end_headers()
        self.write_response_text('Server does not know how to handle request.')

    def handle_request_method(self):
        '''Default handler for all request commands/methods'''
        for d in self.__class__.delegates:
            if d(self):
                return
        # haven't returned by this point...
        self.unhandled_request_method()

    def do_CONNECT(self):
        # TODO loop back to self to see if it can be handled?
        # probably trying to connect to HTTPS server though
        self.unhandled_request_method()

class LoggingRequestDelegate(object):
    '''Log to a file requests that have reached this delegate.'''
    def __init__(self, outfile=None):
        self._out_file = outfile
        self._seen = set()
    def handle_request(self, reqhandler):
        '''Request delegate function: log request and pass through.'''
        if not self._out_file: return False
        url = reqhandler.urlaccess.complete
        if url not in self._seen:
            self._seen.add(url)
            url = url + "\n"
            self._out_file.write(url.encode('utf-8') if 'b' in self._out_file.mode else url)
        return False	# pass through (not handled, no response sent, just logged)

class DomainSpecificRequestDelegate(object):
    '''Special handling for specific domains.'''
    def __init__(self, domain_handlers=None):
        self._domain_handlers = domain_handlers or {}
    def set_domain_handler(self, domain, requestdelegate):
        self._domain_handlers[domain] = requestdelegate
    @staticmethod
    def domain_match_iter(domain):
        yield domain
        if '.' in domain:
            # look for wildcard domain names
            prefix, unused_sep, domain = domain.partition('.')
            if domain:
                # don't see how domain prefix could be ? but don't do it twice
                if '?' != prefix: yield '?.' + domain
                yield '*.' + domain
                while True:
                    unused_prefix, unused_sep, domain = domain.partition('.')
                    if domain:
                        yield '*.' + domain
                    else: break
        elif '?' != domain: yield '?' # don't see how domain could be ? but don't do it twice
        yield '*'
    def handle_request(self, reqhandler):
        '''Request delegate function: switches request delegates based on hostname.'''
        for d in DomainSpecificRequestDelegate.domain_match_iter(('host' in reqhandler.headers and reqhandler.headers['host'].split(':', 1)[0] or
                reqhandler.urlaccess.hostname).lower()):
            if d in self._domain_handlers and \
                    callable(self._domain_handlers[d]) and \
                    self._domain_handlers[d](reqhandler):
                return True
        return False

class SWFScanner(object):
    '''Scans an SWF stream.
    Re-implementation of things like hexagonit.swfheader,
    which unfortunately does not support LZMA-compressed SWFs:
    https://github.com/dokai/hexagonit-swfheader/issues/2
    Goals include:
    * avoid decompressing everything up front
    * use standard Python libraries (e.g. not use pylzma like https://github.com/OpenGG/swfzip)

    Requires Python 3.3 (LZMA introduced in 3.3)
    '''

    SWF_CVRNT = 'c' # compression variant, first letter of magic, as 1-byte bytes array
    SWF_VER = 'v'   # version
    SWF_SIZE = 'sz' # uncompressed size
    SWF_BNDW = 'w'  # bounds width in pixels
    SWF_BNDH = 'h'  # bounds height in pixels
    SWF_FRATE = 'r' # frame rate
    SWF_FCNT = 'f'  # frame count

    def __init__(self, binfile):
        self._binfile = binfile
        self.process_header()

    @staticmethod
    def intfromle(lebytes):
        '''Convert little-endian bytes to integer.
        Equivalent to struct.unpack('<I', lebytes), except length is auto-detected up to 4 bytes.
        '''
        result = lebytes[0]
        if 1 >= len(lebytes): return result
        result = lebytes[1] << 8 | result
        if 2 >= len(lebytes): return result
        result = lebytes[3] << 24 | lebytes[2] << 16 | result
        return result

    @staticmethod
    def unpack_swf_rect(bits, bound1, boundt):
        '''Extracts fields from variable-length RECT.
        Returns 4 fields as a list.'''
        # boundt: bound tail (without first byte containing the leading 3 bits)
        # first 2 cases are not tested; rather silly as they're not even 1 pixel
        if 1 == bits:
            return [(bound1[0] >> 2) & 0x01,
                (bound1[0] >> 1) & 0x01,
                (bound1[0] >> 0) & 0x01,
                (boundt[0] >> 7) & 0x01]
        elif 2 == bits:
            return [(bound1[0] >> 1) & 0x03,
                (bound1[0] & 0x01) << 1 | (boundt[0] >> 7),
                (boundt[0] >> 5) & 0x03,
                (boundt[0] >> 3) & 0x03]
        else:
            fields = [] # storage for xmin, xmax, ymin, ymax, to be returned
            # ready first 3 bits
            needbits = bits - 3
            bitbuf = bound1[0] & 0x07
            # marker in boundt: byte 0, bit 0
            tailcury, tailcuri = 0, 0
            while 4 > len(fields):
                # realign back to byte in boundt
                if 0 < tailcuri:
                    if 8 >= tailcuri + needbits:
                        bitbuf = (bitbuf << needbits) | \
                            ((boundt[tailcury] >> (8 - tailcuri - needbits)) & ((1 << needbits) - 1))
                        tailcuri = tailcuri + needbits
                        needbits = 0
                    else:
                        eatbits = 8 - tailcuri
                        assert eatbits <= needbits, 'Eating more bits than needed'
                        bitbuf = (bitbuf << eatbits) | (boundt[tailcury] & ((1 << eatbits) - 1))
                        needbits = needbits - eatbits
                        tailcury, tailcuri = tailcury + 1, 0    # move on to next byte
                if 0 < needbits:
                    while needbits >= 8:
                        needbits = needbits - 8
                        bitbuf = bitbuf << 8 | boundt[tailcury]
                        tailcury = tailcury + 1
                    if 0 < needbits:
                        bitbuf = (bitbuf << needbits) | (boundt[tailcury] >> (8 - needbits))
                    tailcuri = needbits
                fields.append(bitbuf)
                bitbuf, needbits = 0, bits  # reset for next field
            return fields

    def process_header(self):
        '''Reads SWF header, making a read() method available
        to read [decompressed] SWF tags.
        Returns a dictionary of header fields.'''
        headbytes = self._binfile.read(8);
        if b'WS' != headbytes[1:3]:
            return False
        result = {
            SWFScanner.SWF_CVRNT: headbytes[0:1],
            SWFScanner.SWF_VER: headbytes[3],
            SWFScanner.SWF_SIZE: SWFScanner.intfromle(headbytes[4:8])
        }
        if b'F' == result[SWFScanner.SWF_CVRNT]:
            self.read = self._binfile.read
        elif b'C' == result[SWFScanner.SWF_CVRNT]:
            import zlib
            decompressor = zlib.decompressobj()
            # assuming only needing at most b bytes to decompress to b bytes or more
            self.read = lambda b: decompressor.decompress(
                decompressor.unconsumed_tail if b <= len(decompressor.unconsumed_tail)
                else (decompressor.unconsumed_tail + self._binfile.read(512 if b < 512 else b))
                if decompressor.unconsumed_tail else self._binfile.read(512 if b < 512 else b), b)
        elif b'Z' == result[SWFScanner.SWF_CVRNT]:
            import lzma
            # https://github.com/OpenGG/swfzip/blob/master/swfzip.py#L110
            # various source code has this for SWF header for Z compressed
            # SWF magic 3 bytes, SWF version 1 byte, uncompressed size 4 bytes,
            # LZMA compressed size 4 bytes, LZMA properties 5 bytes
            # (is really LZMA properties 1 byte + LZMA dict size 4 bytes?)
            # LZMA ALONE is not compatible because it has
            # 1-byte LZMA properties, 4-byte dict size, 8-byte uncompressed size
            self._binfile.read(4)                 # LZMA compressed size
            # LZMA properties byte can be thought of as
            # a single packed 3-position mixed radix number (5,5,9)
            lzmaprop = self._binfile.read(1)[0]
            lzmalc = lzmaprop % 9       # read rightmost lc (radix 9: 0-8)
            lzmaprop = lzmaprop // 9    # drop off rightmost lc
            self.read = lzma.LZMAFile(filename=self._binfile, mode='r', format=lzma.FORMAT_RAW, filters=[ {
                    'id': lzma.FILTER_LZMA1,
                    'dict_size': SWFScanner.intfromle(self._binfile.read(4)),
                    'pb': lzmaprop // 5,
                    'lp': lzmaprop % 5,
                    'lc': lzmalc,
                } ]).read
        # RECT (variable-length) contains bounds of SWF
        bound1 = self.read(1)
        boundbits = (bound1[0] >> 3) & 0x1F
        # cheapo math.ceil by using integer division of negative number
        # rounds down towards -infinity, which then gets negated
        # so in the end it rounds towards +infinity
        boundt = self.read(-((boundbits * 4 + 5) // -8) - 1)
        boundxmin, boundxmax, boundymin, boundymax = SWFScanner.unpack_swf_rect(boundbits, bound1, boundt)
        # bounds is in twips (1/20 of a pixel)
        result[SWFScanner.SWF_BNDW] = (boundxmax - boundxmin) / 20
        result[SWFScanner.SWF_BNDH] = (boundymax - boundymin) / 20
        frameinfobytes = self.read(4)
        # frame rate (8.8, but little-endian so first byte is tenth-sec)
        result[SWFScanner.SWF_FRATE] = frameinfobytes[0] * 0.1 + frameinfobytes[1]
        # total frame count in SWF
        result[SWFScanner.SWF_FCNT] = SWFScanner.intfromle(frameinfobytes[2:4])
        self.header = result
        return True

    def readTag(self):
        '''Read an SWF tag.
        Returns a tuple of (tagtype, tagdata).'''
        taghead = self.read(2)
        tagdatalen = taghead[0] & 0x3F
        if 0x3F == tagdatalen:
            tagdatalen = SWFScanner.intfromle(self.read(4))
        tagdata = None
        if tagdatalen:
            # don't attempt to read 0 bytes; will wind up reading something
            tagdata = self.read(tagdatalen)
            while len(tagdata) < tagdatalen:
                tagdata = tagdata + self.read(tagdatalen - len(tagdata))
        return ((taghead[1] << 2) | (taghead[0] >> 6), tagdata)

    @staticmethod
    def scan(binfile, tagprocessors):
        scanner = SWFScanner(binfile)
        if tagprocessors:
            skiptagprocessors = set()
            proceed = True  # keep going as long as tags were processed
            while proceed:
                proceed, tagType, tagData = False, None, None
                for p in tagprocessors:
                    if p not in skiptagprocessors:
                        proceed = True
                        if tagType is None:
                            tagType, tagData = scanner.readTag()
                            if 0 == tagType:    # End tag
                                hasProcessed = False
                                break
                        if p(tagType, tagData):
                            skiptagprocessors.add(p)
        return scanner.header

class FlashHTMLRequestDelegate(object):
    '''Generates missing HTML for SWFs.
    Browsers typically don't directly render SWFs;
    they don't behave like Flash Player standalone projector which
    could resize and not worry about permissions like allowing script access.
    An HTML EMBED or OBJECT tag is used to show SWFs on a browser.
    To save the user from having to generate these manually, when
    receving a request for SWF with the extension missing or changed to HTML,
    this serves a simple template HTML page with an OBJECT tag for the SWF.
    The corresponding SWF must be available on the local path as
    this scans the SWF for some default values to use in the OBJECT tag.
    The URL must not already be mapped to a file locally,
    as that file will be served instead so as to not hide them.
    Users may additionally provide queries on the HTML URL to override properties
    on the OBJECT tag with the following prefixes to the queries:
    p_) set PARAM tags within the OBJECT tag to control Flash itself,
      for things like permissions to the SWF like allowing javascript access;
      p_ prefix is dropped and name and values are copied into PARAM tag;
    fv_) set flashVars PARAM to pass variables to the SWF;
      fv_ prefix is dropped before setting into flashVars;
    width=|height=) set width and height properties on OBJECT tag so
      browsers know the size to render the SWF as before downloading the SWF.
    '''

    # query params overrides
    _Q_FLASHVARSPREFIX = 'fv_'
    _Q_PARAMPREFIX = 'p_'
    _Q_WIDTHPARAM = 'width='
    _Q_HEIGHTPARAM = 'height='

    import re
    _suffix = re.compile(r'\.html?$', re.IGNORECASE)

    def __init__(self, ghost_get_local_path):
        self._ghost_get_local_path = ghost_get_local_path

    @staticmethod
    def scan_swf(binfile, tagprocessors):
        binfile.read(8);

    @staticmethod
    def _chomp_suffix(string):
        suffix_match = FlashHTMLRequestDelegate._suffix.search(string)
        return string[:suffix_match.start()] if suffix_match else string

    def handle_request(self, reqhandler):
        '''Request delegate function: generate HTML file for corresponding Flash SWF.'''
        localpath = self._ghost_get_local_path(reqhandler.urlaccess)
        if os.path.exists(localpath):
            return False
        else:
            localpath = FlashHTMLRequestDelegate._chomp_suffix(localpath)
            if os.path.isfile(localpath + ".swf"):
                # auto-generated: may have wrong settings such as flashvars
                reqhandler.send_response(httpstatus.NON_AUTHORITATIVE_INFORMATION)
                reqhandler.send_header('Content-Type', 'text/html; charset=UTF-8')
                reqhandler.send_header('Referrer-Policy', 'no-referrer')
                reqhandler.end_headers()
                path_split = reqhandler.urlaccess.path.rsplit('/', 1)
                swfparams = {
                        'allowFullScreen': 'true',
                        'allowNetworking': 'all',
                        'allowScriptAccess': 'always',
                        'base': '%s://%s%s/' % (reqhandler.urlaccess.scheme, reqhandler.urlaccess.server, path_split[0]),
                        'menu': 'true'
                    }
                path_split = FlashHTMLRequestDelegate._chomp_suffix(path_split[1])
                swfparams['movie'] = path_split + '.swf'
                swfw, swfh = 800, 600
                def attrscanner(tagType, tagData):
                    if 69 == tagType:   # FileAttributes
                        if tagData[0] & 0x20:
                            swfparams['wmode'] = 'gpu'
                        elif tagData[0] & 0x40:
                            swfparams['wmode'] = 'direct'
                    return True # either it's the first tag or it's not there
                def bgscanner(tagType, tagData):
                    if 1 == tagType:    # ShowFrame
                        return True
                    if 9 == tagType:    # SetBackgroundColor
                        swfparams['bgcolor'] = '#' + tagData.hex()
                        return True
                    return False
                with open(localpath + ".swf", mode='rb') as f:
                    swfheader = SWFScanner.scan(f, [ attrscanner, bgscanner ])
                    swfw, swfh = swfheader[SWFScanner.SWF_BNDW], swfheader[SWFScanner.SWF_BNDH]
                if reqhandler.urlaccess.query:
                    splitquery = reqhandler.urlaccess.query.split('&')
                    fvprefix = FlashHTMLRequestDelegate._Q_FLASHVARSPREFIX
                    pprefix = FlashHTMLRequestDelegate._Q_PARAMPREFIX
                    fvprefixlen, pprefixlen = len(fvprefix), len(pprefix)
                    # pass through flashvars to SWF
                    swfparams['flashVars'] = '&'.join(
                        map(lambda q: q[fvprefixlen:],
                            filter(lambda q: q.startswith(fvprefix),
                                splitquery)))
                    # override param tags and width and height
                    for q in splitquery:
                        if q.startswith(pprefix):
                            querykv = q.split('=',1)
                            swfparams[querykv[0][pprefixlen:]] = \
                                querykv[1] if 1 < len(querykv) else ''
                        elif q.startswith(FlashHTMLRequestDelegate._Q_WIDTHPARAM):
                            swfw = int(q[len(FlashHTMLRequestDelegate._Q_WIDTHPARAM):])
                        elif q.startswith(FlashHTMLRequestDelegate._Q_HEIGHTPARAM):
                            swfh = int(q[len(FlashHTMLRequestDelegate._Q_HEIGHTPARAM):])
                reqhandler.write_response_text('''<html>
<head><title>%s</title></head>
<body style="background: #112233; color: #88CC88;">
<object id="movie" type="application/x-shockwave-flash" data="%s" width="%i" height="%i">
%s
</object><br>%s</body>
</html>''' % (path_split, swfparams['movie'], swfw, swfh,
                    '\n'.join([ '\t<param name="%s" value="%s">' % (k,v)
                        for k,v in swfparams.items() ]),
                    path_split))
                return True
            else:
                return False

class FlashLocalAlternativeServerHistoryRequestHandler(DelegatingRequestHandler):
    '''Serves local files, with special Flash-specific handling.'''

    @staticmethod
    def delegate_request_gone(reqhandler):
        '''Request delegate function: respond with GONE.'''
        reqhandler.send_response(httpstatus.GONE)
        reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
        reqhandler.send_header('Cache-Control',
            'public, immutable, max-age=%i, stale-while-revalidate=%i, stale-if-error=%i' %
            ((24 * 60 * 60,) * 3))
        reqhandler.end_headers()
        reqhandler.write_response_text('No longer available.')
        return True

    @staticmethod
    def delegate_request_bad_gateway(reqhandler):
        '''Request delegate function: respond with BAD_GATEWAY.'''
        reqhandler.send_response(httpstatus.BAD_GATEWAY)
        reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
        reqhandler.send_header('Cache-Control',
            'public, immutable, max-age=%i, stale-while-revalidate=%i, stale-if-error=%i' %
            ((24 * 60 * 60,) * 3))
        reqhandler.end_headers()
        reqhandler.write_response_text('Server not accessible.')
        return True

    @staticmethod
    def delegate_request_firefox_detectportal(reqhandler):
        '''Request delegate function: Firefox portal detection.'''
        if '/success.txt' == reqhandler.urlaccess.path:
            msg = "success\n"
            reqhandler.send_response(httpstatus.OK)
            reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
            reqhandler.send_header('Content-Length', len(msg))
            reqhandler.send_header('Cache-Control',
                'public,must-revalidate,max-age=0,s-maxage=3600')
            reqhandler.end_headers()
            reqhandler.write_response_text(msg)
            return True
        return False

    @staticmethod
    def is_crossdomain_xml(reqhandler):
        return '/crossdomain.xml' == reqhandler.urlaccess.path

    @staticmethod
    def delegate_request_crossdomain_xml(reqhandler):
        '''Request delegate function: serves permissive crossdomain.xml to be used by Flash.'''
        if FlashLocalAlternativeServerHistoryRequestHandler.is_crossdomain_xml(reqhandler):
            reqhandler.send_response(httpstatus.OK)
            reqhandler.send_header('Content-Type', 'text/x-cross-domain-policy')
            # will cause security issue when going back online or
            # when xml lands in filesystem and gets served by ghost later
            # but functionality-wise this should be permissive enough to keep things working
            reqhandler.send_header('Cache-Control',
                'public, immutable, max-age=%i, stale-while-revalidate=%i, stale-if-error=%i' %
                ((24 * 60 * 60,) * 3))
            reqhandler.end_headers()
            reqhandler.write_response_text('''<?xml version="1.0"?>
<!DOCTYPE cross-domain-policy SYSTEM "http://www.adobe.com/xml/dtds/cross-domain-policy.dtd">
<cross-domain-policy>
    <site-control permitted-cross-domain-policies="all"/>
    <allow-access-from domain="*" secure="false"/>
    <allow-http-request-headers-from domain="*" headers="*" secure="false"/>
</cross-domain-policy>''')
            return True
        return False

    @staticmethod
    def delegate_request_orisinal_site(reqhandler):
        '''Request delegate function: serves control files of orisinal site.'''
        if reqhandler.path.startswith('http://www.ferryhalim.com/orisinal/g3/'):
            parsedpath = reqhandler.urlaccess.path;
            subpath = parsedpath[13:] if parsedpath.startswith("/orisinal/g3/") else parsedpath
            if 'hst.txt' == subpath:
                reqhandler.send_response(httpstatus.OK)
                reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
                reqhandler.end_headers()
                reqhandler.write_response_text('hst=./&')
                return True
            elif 'at0pp4.php' == subpath:
                # https://stackoverflow.com/questions/8534256/find-first-element-in-a-sequence-that-matches-a-predicate/8534381#8534381
                zx = next(filter(lambda q: q.startswith('zx='), reqhandler.urlaccess.query.split('&')), '')
                zx = zx[3:] if zx.startswith('zx=') else ''
                if zx:
                    reqhandler.send_response(httpstatus.OK)
                    reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
                    reqhandler.end_headers()
                    # 8 means frame 8?  Initial SWF chooses to start frame 8 or 6 depending on this.
                    reqhandler.write_response_text('FIXME: not giving away secret sauce')
                else:
                    reqhandler.send_response(httpstatus.BAD_REQUEST)
                    reqhandler.send_header('Content-Type', 'text/plain; charset=UTF-8')
                    reqhandler.end_headers()
                    reqhandler.write_response_text('Missing zx parameter.')
                return True
            else:
                return False

    @staticmethod
    def _gather_fallback_delegates():
        '''Gather list of delegates tied to the fallback ghost delegate.'''
        # avoid creating delegate up front by specifying next's default
        fallback_delegate = next(filter(lambda d: GhostHTTPOfflineServerTransplantRequestDelegate.handle_request.__qualname__ == d.__qualname__,
            DelegatingRequestHandler.delegates), None)
        fallback_delegate = fallback_delegate.__self__ if fallback_delegate \
            else GhostHTTPOfflineServerTransplantRequestDelegate()
        html_delegate = FlashHTMLRequestDelegate(fallback_delegate.get_local_path)
        # use handle_available_request instead of handle_request so it can fall back to crossdomain
        fallback_delegate = fallback_delegate.handle_available_request
        return [ html_delegate.handle_request, fallback_delegate ]

    @staticmethod
    def _gather_all_delegates(delegatecontainer):
        result = []
        result.append(DomainSpecificRequestDelegate({
                # chromium captive portal detection: contact random tld (dotless) domains
                '?': delegatecontainer.delegate_request_bad_gateway,
                # firefox captive portal detection: check for success at URL
                'detectportal.firefox.com': delegatecontainer.delegate_request_firefox_detectportal,
                'www.ferryhalim.com': delegatecontainer.delegate_request_orisinal_site,
                '*.api.playtomic.com': delegatecontainer.delegate_request_gone
            }).handle_request)
        result.extend(delegatecontainer._gather_fallback_delegates())
        result.append(delegatecontainer.delegate_request_crossdomain_xml)
        #result.append(LoggingRequestDelegate(open('/tmp/ghost_notfound.log', 'a', encoding='utf-8')).handle_request)
        # respond_not_found is not a request delegate: it doesn't return with True
        result.append(lambda reqhandler: GhostHTTPOfflineServerTransplantRequestDelegate.respond_not_found(reqhandler) or True)
        return result

FlashLocalAlternativeServerHistoryRequestHandler.delegates = FlashLocalAlternativeServerHistoryRequestHandler._gather_all_delegates(FlashLocalAlternativeServerHistoryRequestHandler)

# https://stackoverflow.com/questions/44834/what-does-all-mean-in-python#comment-113394665
__all__ = [
    GhostHTTPOfflineServerTransplantRequestDelegate.__name__,
    DelegatingRequestHandler.__name__,
    SWFScanner.__name__,
    FlashHTMLRequestDelegate.__name__,
    FlashLocalAlternativeServerHistoryRequestHandler.__name__,
]

if os.getenv(key='TEST', default='0').lower() in ['1', 'y', 'yes', 'yay', 'aye', 'true']:	# run tests
    print("Running tests because TEST environment variable is %s" % os.getenv(key='TEST', default='0'))
    import random
    import itertools
    import operator
    def testdomainmatch(expected, domain):
        try:
            next(itertools.dropwhile(operator.truth,
                itertools.starmap(operator.eq,
                    itertools.zip_longest(expected,
                        DomainSpecificRequestDelegate.domain_match_iter(domain)))))
        except StopIteration:
            pass    # the test, quite literally
        else:
            print("Unexpected domain match: '{domain}' yielded ({yields})".format(domain = domain,
				yields = ', '.join(DomainSpecificRequestDelegate.domain_match_iter(domain))))
    testdomainmatch([
            'invalid',
            '?',
            '*',
        ], 'invalid')
    testdomainmatch([
            'example.com',
            '?.com',
            '*.com',
            '*',
        ], 'example.com')
    testdomainmatch([
            'a.b.c.example.com',
            '?.b.c.example.com',
            '*.b.c.example.com',
            '*.c.example.com',
            '*.example.com',
            '*.com',
            '*',
        ], 'a.b.c.example.com')

    class URLAccessString(URLAccess.Source):
        def __init__(self, url:str):
            self._url = url
        def asvariant(self, variant:str):
            return self if URLAccess.Source.SRC_FAST == variant or URLAccess.Source.SRC_URL == variant else None
        @property
        def complete(self):
            return self._url
        @staticmethod
        def asaccess(url:str):
            return URLAccess(URLAccessString(url))
    # Test MIMETAB
    # the table should have 3 columns, separated by whitespace
    # - domain: domain name; may start with * or ? wildcards
    #   - * means 1 or more levels of subdomains
    #   - ? means direct subdomains
    #   - partial wildcards not supported (e.g. test*.example.com)
    # - mime type: the MIME type to use if matched
    # - match predicate list: rules for [not] matching
    #   - a predicate list is made up of predicates
    #     - optionally separated by ,;:/& and whitespace
    #     - all parts of predicate list must match
    #   - a predicate is made up of 2 parts
    #     - a number of tokens from the following categories
    #       - part of URL to match (path, complete)
    #       - how to match ([dir]prefix, suffix, substr, regex)
    #       - inversion (not)
    #       - reference (name, include)
    #         - names the following predicate
    #         - or name the entire predicate list if at the end
    #         - include includes the named predicate(s)
    #     - followed by argument
    #       - first character specifies ending delimiter
    #         - this delimiter cannot be escaped
    #       - remaining string specifies what to match
    mimemap = URLToMIMEMapper()
    mimeresult1 = 'example/x-test-match-1'
    mimeresult2 = 'example/x-test-match-2'
    mimeresult3 = 'example/x-test-match-3'
    mimemap.process_rules(io.StringIO('''
dummy!!!	example/invalid	name"test2": pathdirprefixnot"/some/path/to"
dummy!!!	example/invalid	include"test1", include"test2", name"test3"
dummy!!!	example/invalid	include"test3", name"test4"
*.example.com	{mimeresult1}	name"test1": pathsuffix".test"
test.example.com	{mimeresult3}	include"test3"
test.example.com	{mimeresult2}	include"test1"
'''.format(mimeresult1 = mimeresult1, mimeresult2 = mimeresult2, mimeresult3 = mimeresult3)))
    def testurl(expectedmime, url):
        resultmime = mimemap.get(URLAccessString.asaccess(url))
        if resultmime != expectedmime: print(
            "Got unexpected MIME {resultmime} instead of {expectedmime} for {url}".format(
                resultmime = resultmime, expectedmime = expectedmime, url = url))
    def testcomponents(expectedmime, server, path):
        return testurl(expectedmime, "https://{server}{path}?cache_buster={buster}".format(
            server = server, path = path, buster = random.randint(0,99999)))
    testcomponents(None, 'example.com', '/some/path/to/target.test')
    testcomponents(mimeresult2, 'test.example.com', '/some/path/to/target.test')
    testcomponents(mimeresult3, 'test.example.com', '/some/path/towards/target.test')
    testcomponents(mimeresult3, 'test.example.com', '/path/to/target.test')
    testcomponents(None, 'test.example.com', '/some/path/to/target.toast')
    testcomponents(mimeresult1, 'tested.example.com', '/some/path/to/target.test')
    print('done')
elif '__main__' == __name__:
    import sys
    defaultport = 8128
    addr = 'localhost'
    port = defaultport
    reqhandlercls = FlashLocalAlternativeServerHistoryRequestHandler
    localrootpath = None
    flatpath = False
    mimeheaders = None
    fileheaders = {}
    isswfscan = False
    argiter = iter(sys.argv)
    next(argiter)   # skip program name
    while True:
        try:
            v = next(argiter)
            if '-p' == v:
                port = int(next(argiter))
            elif '-m' == v:
                mimeheaders = GhostHTTPOfflineServerTransplantRequestDelegate.MIMEHeader(
                    GhostHTTPOfflineServerTransplantRequestDelegate.DictHeaderMode(fileheaders))
                with open(next(argiter), 'r') as mimetabtextstream:
                    mimeheaders.intern_rules(mimetabtextstream)
            elif '-g' == v:
                reqhandlercls = DelegatingRequestHandler
            elif '-h' == v or '--help' == v:
                cmdname = os.path.split(sys.argv[0])[1]
                print('''Serves local files over HTTP.
Usage: %s [-g] [-/] [-$] [-m MIMETAB] [-p PORT] [@ADDRESS] [LOCAL_ROOT]
  -g:           ghost mode: serve local files only;
                don't do special (Flash-specific / site-specific) handling
  -/:           flatten paths: map all URLs to files under LOCAL_ROOT
                all domains and URL paths will be served from the root
                (don't recurse into subdirectories under root)
  -$:           tell browser to avoid caching
                add "Cache-Control: no-store" header to files served
                may affect performance negatively
  -m MIMETAB:   a table specifying the MIME type to use for each URL
                see test in source for documentation and sample
  -p PORT:      listen on specified PORT; defaults to %i
  @ADDRESS:     IP:Port to listen at; may omit either IP or port
                :PORT to omit IP and bind to all interfaces
                IP to omit port and use default
                when both -p and @ are specified, the last port is used
  LOCAL_ROOT:   local directory from which files to be served are looked up
                can also be specified via LOCAL_ROOT environment variable
                defaults to current working directory otherwise
Usage: %s SWF_File [SWF_File...]
  extract information from SWF
  for debugging issues with automatic HTML generation''' %
                    (cmdname, defaultport, cmdname))
                exit()
            elif '-/' == v:
                flatpath = True
            elif '-$' == v:
                fileheaders['Cache-Control'] = 'no-store'
            elif v.startswith('@'):
                ipport = v[1:].split(':', 1)
                addr = ipport[0]
                if 1 < len(ipport): port = int(ipport[1])
            else:
                if os.path.isfile(v):   # SWF scan
                    print('Trying to scan specified SWF file %s.' % v)
                    isswfscan = True
                    if 'swfcmprdisp' not in globals(): swfcmprdisp = {
                            b'F': 'None',
                            b'C': 'ZLIB',
                            b'Z': 'LZMA'
                        }
                    with open(v, 'rb') as binfile:
                        scanner = SWFScanner(binfile)
                        print('SWF %i header:\n\tCompression: %s (%i bytes decompressed)\n\t%i frames of %ix%i @ %.1f fps' % (
                                scanner.header[SWFScanner.SWF_VER],
                                swfcmprdisp[scanner.header[SWFScanner.SWF_CVRNT]]
                                    if scanner.header[SWFScanner.SWF_CVRNT] in swfcmprdisp
                                    else scanner.header[SWFScanner.SWF_CVRNT],
                                scanner.header[SWFScanner.SWF_SIZE],
                                scanner.header[SWFScanner.SWF_FCNT],
                                scanner.header[SWFScanner.SWF_BNDW],
                                scanner.header[SWFScanner.SWF_BNDH],
                                scanner.header[SWFScanner.SWF_FRATE]
                            ))
                        want = { 'bg', 'attr' }
                        def procShowFrame(tagdata):
                            if 'bg' in want: want.remove('bg')
                        def procSetBackgroundColor(tagdata):
                            if 'bg' in want:
                                print('BG: #%s' % tagdata.hex())
                                want.remove('bg')
                        def procFileAttributes(tagdata):
                            if 'attr' in want:
                                flags = []
                                if tagdata[0] & 0x40: flags.append('UseDirectBlit')
                                if tagdata[0] & 0x20: flags.append('UseGPU')
                                if tagdata[0] & 0x10:
                                    flags.append('HasMetadata')
                                    want.add('meta')
                                if tagdata[0] & 0x08: flags.append('isAS3')
                                if tagdata[0] & 0x01: flags.append('UseNetwork')
                                print('Flags: %s' % (', '.join(flags) if flags else '<none>'))
                                want.remove('attr')
                        def procMetadata(tagdata):
                            if 'meta' in want:
                                # encoding for SWF ver <= 5 could be SJIS
                                print('Metadata: %s' % (tagdata.decode(encoding='utf-8')
                                    if 6 <= scanner.header[SWFScanner.SWF_VER]
                                    else tagdata.decode(encoding='cp1252')))
                                want.remove('meta')
                        procs = {
                                1: procShowFrame,
                                9: procSetBackgroundColor,
                                69: procFileAttributes,
                                77: procMetadata
                            }
                        while want:
                            tagtype, tagdata = scanner.readTag()
                            if 0 == tagtype:    # End
                                break
                            elif tagtype in procs:
                                procs[tagtype](tagdata)
                            if 'attr' in want: want.remove('attr')
                else:
                    localrootpath = v
        except StopIteration:
            break

    if not localrootpath and isswfscan:
        # all paths were files: scan SWF only and quit
        exit()

    if localrootpath or flatpath or fileheaders or mimeheaders:
        ghostrdcls=GhostHTTPOfflineServerTransplantRequestDelegate
        if not localrootpath: localrootpath = os.getenv(key='LOCALROOT') or os.getcwd()
        fallbackdelegate = ghostrdcls(
            local_path=(ghostrdcls.get_flat_local_path_retriever if flatpath
                else ghostrdcls.get_full_local_path_retriever)(localrootpath),
            http_headers_for_files=mimeheaders or fileheaders)
        replacedelegates = {
                ghostrdcls.handle_request.__qualname__:
                    fallbackdelegate.handle_request,
                ghostrdcls.handle_available_request.__qualname__:
                    fallbackdelegate.handle_available_request,
                FlashHTMLRequestDelegate.handle_request.__qualname__:
                    FlashHTMLRequestDelegate(fallbackdelegate.get_local_path).handle_request
            }
        reqhandlercls.delegates = [
            # replace delegate so to not mess with default delegate which may be used elsewhere
            replacedelegates[d.__qualname__]
            if d.__qualname__ in replacedelegates
            else d for d in reqhandlercls.delegates
        ]

    # find the ghost and see what files it's using
    print(next(filter(
        lambda p: p,
        map(lambda d: d.__self__
                if hasattr(d, '__self__') and
                    GhostHTTPOfflineServerTransplantRequestDelegate.__name__ ==
                    d.__self__.__class__.__name__
                else None,
            reqhandlercls.delegates))).describe_get_local_path())
    mimetypes.init()
    http.server.HTTPServer((addr, port), reqhandlercls).serve_forever()
elif __name__.startswith('__mitmproxy_script__.'):
    # last part of name changes depending on file name
    # print('module is named: %s' % __name__)
    import logging
    import mitmproxy.addonmanager
    import mitmproxy.ctx
    import mitmproxy.http
    import mitmproxy.version
    import typing
    try:	# restructured in commit 9409bf03681f1f1068f52cc6a5d4575dbace0dbc
        mitmRequest = mitmproxy.http.Request
    except AttributeError:	# older than 7.0.0 (e.g. 6.0.2 on Kali 2021.1)
        mitmRequest = mitmproxy.http.HTTPRequest
    try:	# restructured in commit 9409bf03681f1f1068f52cc6a5d4575dbace0dbc
        mitmResponse = mitmproxy.http.Response
    except AttributeError:	# older than 7.0.0 (e.g. 6.0.2 on Kali 2021.1)
        mitmResponse = mitmproxy.http.HTTPResponse
    class URLAccessMitMReq(URLAccess.Source):
        def __init__(self, req:mitmRequest):
            self._req = req
        def asvariant(self, variant:str):
            return self if URLAccess.Source.SRC_FAST == variant or URLAccess.Source.SRC_REQ == variant else None
        @property
        def server(self):
            return 'host' in headers and headers['host']
        @property
        def hostname(self):
            return self._req.pretty_host
        @property
        def resource(self):
            return self._req.path
        @property
        def path(self):
            return None
        @property
        def complete(self):
            return self._req.url
        @staticmethod
        def asaccess(req:mitmRequest):
            return URLAccess(URLAccessMitMReq(req))
    class GHOST:
        '''mitmproxy's map_local addon, ghost style'''
        def load(self, loader: mitmproxy.addonmanager.Loader):
            logging.info('setting up on load')
            loader.add_option(
                name="ghost.fallthrough",
                typespec=bool,
                default=True,
                help="Fall through to mitmproxy default handling for when ghost fails to process the request.  Selectively patches files from local filesystem when enabled (true).  Answers with HTTP error codes when disabled (false).",
            )
            loader.add_option(
                name="ghost.plain",
                typespec=bool,
                default=False,
                help="Disable special handling for things like Flash",
            )
            loader.add_option(
                name="ghost.disablecache",
                typespec=bool,
                default=False,
                help="Add a response header to tell the browser to avoid caching the response",
            )
            loader.add_option(
                name="ghost.mapflat",
                typespec=bool,
                default=False,
                help="When looking up files, look only at the specified directory; do not use path information",
            )
            loader.add_option(
                name="ghost.mimetab",
                typespec=typing.Optional[str],
                default=None,
                help="Specify path to file containing MIME mappings based on URL",
            )
            loader.add_option(
                name="ghost.localroot",
                typespec=typing.Optional[str],
                default=None,
                help="Specify base directory from which to look up files",
            )
        @staticmethod
        def get_full_local_path_retriever(local_root):
            result = lambda urlAccess: \
                GhostHTTPOfflineServerTransplantRequestDelegate.finalize_local_path_from(
                    GhostHTTPOfflineServerTransplantRequestDelegate.map_local_full_path_from(
                        local_root, urlAccess),
                    urlAccess.query)
            result.local_root = local_root
            result.description = 'Serving files from ' + local_root
            return result
        @staticmethod
        def get_flat_local_path_retriever(local_root):
            result = lambda urlAccess: \
                GhostHTTPOfflineServerTransplantRequestDelegate.finalize_local_path_from(
                    GhostHTTPOfflineServerTransplantRequestDelegate.map_local_flat_path_from(
                        local_root, urlAccess.resource),
                    '')	 # FIXME split out query
            result.local_root = local_root
            result.description = 'Serving files directly under ' + local_root
            return result
        def configure(self, updated):
            if "ghost.mapflat" in updated or "ghost.localroot" in updated:
                self._local_path = (GHOST.get_flat_local_path_retriever
                    if getattr(mitmproxy.ctx.options, 'ghost.mapflat')
                    else GHOST.get_full_local_path_retriever)(
                        getattr(mitmproxy.ctx.options, 'ghost.localroot') or
                            os.getenv(key='LOCALROOT') or os.getcwd())
            if "ghost.mimetab" in updated:
                self._mime = URLToMIMEMapper()
                with open(getattr(mitmproxy.ctx.options, 'ghost.mimetab'), 'r') as mimetabtextstream:
                    self._mime.intern_rules(mimetabtextstream)
        @staticmethod
        def shouldfallthrough() -> bool:
            result = getattr(mitmproxy.ctx.options, 'ghost.fallthrough')
            return getattr(mitmproxy.ctx.options, 'upstream_cert') if result is None else result
        def request(self, flow: mitmproxy.http.HTTPFlow) -> None:
            if flow.response or flow.error or not flow.live: return
            urlAccess = URLAccessMitMReq.asaccess(flow.request)
            localpath = self._local_path(urlAccess)
            if not os.path.exists(localpath):
                logging.info('Missing %s', localpath)
                if not GHOST.shouldfallthrough():
                    flow.response = mitmResponse.make(404, 'File not found.', {
                        'Server': mitmproxy.version.MITMPROXY + ' GHOST/' + __version__,
                        'Content-Type': 'text/plain' })
            elif os.path.isdir(localpath):
                logging.info('Directory %s', localpath)
                if not GHOST.shouldfallthrough():
                    flow.response = mitmResponse.make(404, 'Path not found.', {
                        'Server': mitmproxy.version.MITMPROXY + ' GHOST/' + __version__,
                        'Content-Type': 'text/plain' })
            else:
                logging.info('About to serve %s', localpath)
                headers = { 'Server': mitmproxy.version.MITMPROXY + ' GHOST/' + __version__ }
                mimetype = self._mime.get(urlAccess)
                if mimetype is None:
                    mimetype = mimetypes.guess_type(str(localpath))[0]
                if mimetype:
                    headers["Content-Type"] = mimetype
                try:
                    with open(localpath, 'rb', buffering=0) as f:
                        contents = f.readall()
                except OSError as e:
                    logging.warning("Could not read file: %s" % e)
                    headers['Content-Type'] = 'text/plain'
                    flow.response = mitmResponse.make(500, 'Error retrieving file.', headers)
                else:
                    if contents:
#                        print('serving %s' % localpath)
                        flow.response = mitmResponse.make(200, contents, headers)
            if not (flow.response or flow.error or GHOST.shouldfallthrough()):
                headers['Content-Type'] = 'text/plain'
                flow.response = mitmResponse.make(500, 'Error processing request.', headers)

    addons = [GHOST()]
