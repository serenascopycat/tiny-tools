## Backup and Restore

Android backup and restore can be quirky at times.
As much as `adb` is meant to be fully backwards compatible,
in reality, at least for backup, that's not the case
(restore is fine though).
In addition, there may be other gotchas.

While this [feature is going away in the future](https://www.androidpolice.com/2019/05/13/adbs-backup-and-restore-functionality-will-go-away-in-future-android-release/),
it's still useful, esp. when accessing Google isn't an option.

In the end, to make sure the backed up data is complete,
we need at least the device with the data to backup,
and another fresh device to test restore.
It's totally possible for certain data to be skipped in the backup,
and restoring to the same device doesn't help to detect that situation;
not to mention possible mistakes that would wipe out the data on the original device
(e.g. accidentally restoring an older backup before the new one is completed).

### No Backup

Apps can be [marked to have backup skipped](https://developer.android.com/guide/topics/manifest/application-element#allowbackup).
`adb backup` would be useless for those apps.

### Excluded Data

Apps can choose to exclude certain files from backup.  [Shattered Pixel Dungeon](https://f-droid.org/en/packages/com.shatteredpixel.shatteredpixeldungeon/) has an [example](https://github.com/00-Evan/shattered-pixel-dungeon/blob/83452db0e0596e81ed75199f645a5100fe70b9cd/android/src/main/java/com/shatteredpixel/shatteredpixeldungeon/android/AndroidBackupHandler.java#L50)...  don't stop mid-run and change phones; it won't work!

### External Files

`adb backup` backs up the settings database and stuff that's within the application space, but it doesn't back up stuff outside.
Some apps (e.g. games) write to external storage (`$EXTERNAL_STORAGE/Android/data/<package_id>`), so make sure to check for that as well.

Some apps may have their own directory on the external SD card, but outside the Android data directory.  Some examples:

- [ZeptoLab's Pudding Monsters](https://www.zeptolab.com/games/pudding_monsters)
   - `ZeptoLab/Pudding Monsters`
- [OsmAnd](https://osmand.net/)
   - `osmand`

A bunch of other map apps do this too.

### `adb` Version Compatibility

When backing up and restoring, make sure to use the right version of `adb` to interact with the Android device.
Using the wrong version could lead to a bad backup, or a failed restore even though the backup file has the right data.
Newer `adb`'s `restore` appears to be backwards-compatible, but not `backup`.

Might be better to check `adbd` version, but the `/sbin` directory disallows access for non-root users.
Have to rely on Android version instead.

Some tests to create backups without password, using:

- app
   - `com.TUREZURE.Invader`
      - for all Android versions except 8.0.0 and 12
         - app doesn't start there for whatever reason (e.g. multiple users?)
   - `com.heigames.nomcat`
      - for Android version 8.0.0 and 12
- system
   - Knoppix 7.6.1 (32-bit)
      - can't be used for 23.1.0 or higher which require 64-bit Linux
   - Kali 2021.1 (64-bit)
      - doesn't have all the dependencies for older `adb`
      - sometimes segfaults (e.g. on r23)

Backup is considered PASSing if it can be extracted.
Restore is considered PASSing if the highscores are restored.

<table border="1">
<thead>
<tr><th rowspan="3">Operation</th><th colspan="10"><code>adb</code> version; Platform Tools Revisions</th></tr>
<tr><th>1.0.29</th><th colspan="2">1.0.31</th><th colspan="5">1.0.32</th><th colspan="2">1.0.36</th></tr>
<tr><th>r14</th>
<th>r17</th><th>r20</th>
<th>r21</th><th>r22</th><th>r23</th><th>r23.0.1</th><th>r23.1.0</th>
<th>r24</th><th>r25</th></tr>
</thead>
<tbody>
<!-- Pass: r14, r17, r20, r21, r22; Fail: r23, r23.0.1, r23.1.0, r24, r25 Creates a 41-byte backup -->
<tr><td>Backup from 4.0.4</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td></tr>
<!-- Pass: r14, r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25 -->
<tr><td>Restore to 4.0.4</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r14, r17, r20, r21, r22; Fail: r23, r23.0.1, r23.1.0, r24, r25 Creates a 41-byte backup -->
<tr><td>Backup from 4.2.2</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td></tr>
<!-- Pass: r14, r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25 -->
<tr><td>Restore to 4.2.2</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r17, r20, r21, r22; Fail: r14 <code>adb devices</code> list device as offline, r23, r23.0.1, r23.1.0, r24, r25 creates a 41-byte backup -->
<tr><td>Backup from 4.4.2</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td></tr>
<!-- Pass: r14, r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Restore to 4.4.2</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Backup from 5.0.2</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Restore to 5.0.2</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Backup from 6.0.1</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Restore to 6.0.1</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r17, r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Backup from 7.0</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r24, r25; Fail: r14 <code>adb devices</code> list device as offline, r17, r20, r21, r22, r23, r23.0.1, r23.1.0 does nothing -->
<tr><td>Restore to 7.0</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r17, r20, r21, r22, r23, r23.01, r23.1.0, r24, r25; Fail: r14 <code>adb devices</code> list device as offline -->
<tr><td>Backup from 8.0.0</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r24, r25; Fail: r14 <code>adb devices</code> list device as offline, r17, r20, r21, r22, r23, r23.0.1 W BackupManagerService: java.io.EOFException: Unexpected end of ZLIB input stream, r23.1.0 does nothing -->
<tr><td>Restore to 8.0.0</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14, r17 <code>adb devices</code> does not list device -->
<tr><td>Backup from 9</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r24, r25; Fail: r14, r17 no device listed, r20, r21, r22, r23, r23.0.1 W BackupManagerService: java.io.EOFException: Unexpected end of ZLIB input stream, r23.1.0 does nothing -->
<tr><td>Restore to 9</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td></tr>
</tbody>
<tbody>
<!-- Pass: r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14, r17 <code>adb devices</code> does not list device -->
<tr><td>Backup from 12</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
<!-- Pass: r20, r21, r22, r23, r23.0.1, r23.1.0, r24, r25; Fail: r14, r17 <code>adb devices</code> does not list device -->
<tr><td>Restore to 12</td><td>FAIL</td><td>FAIL</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td><td>PASS</td></tr>
</tbody>
</table>

[This Android Stack Exchange question](https://android.stackexchange.com/questions/83080/adb-backup-creates-0-byte-file-prompts-for-current-backup-password-even-though) have some hints on why there's breakage at 1.0.32 and the links to adb 1.0.31.

For Android older than 5 (i.e. 4.0 ICS to 4.4 KitKat), use `adb` version 1.0.31 for backup.  It is available in platform tools r20.
With a higher version, could wind up [getting 41-byte files](https://android.stackexchange.com/questions/143367/creating-a-backup-with-adb-results-in-41-byte-file).
Google has pretty much given up on Android 4.*, so this might get tricky.

1.0.32 changes the handling of commands passed from `adb backup` to Android in an incompatible way such that older `adbd` on older Android versions will get confused.
Might be [possible to mimic this new version with older versions](https://issuetracker.google.com/issues/37096097#comment7), but YMMV, esp. with Windows Command Prompt and POSIX shell quoting being different.
However, given that it eventually gives out in restoring to newer Android versions, can't just stick with the old version for backup and restores.
There are multiple revisions of 1.0.32.
[r23 is for Android 6](https://web.archive.org/web/20160507104625/http://developer.android.com/intl/ja/tools/revisions/platforms.html) and is the version that fails to back up from Android 4.*.
[64-bit Linux required starting with platform tools r23.1.0](https://web.archive.org/web/20180603082522/https://developer.android.com/studio/releases/sdk-tools).

For Android 7+, use `adb` version 1.0.36+ for restore.  64-bit system is required when on Linux.

### Android Version Compatibility

The following has not been confirmed.  The backup file format version may not matter.
There are checks for installed app being compatible with the saved app info in the backup, such as version and certificate.

After backing up, restoring could be tricky as older Android versions may not accept a backup made on newer Android versions.
It's mentioned in the [help for online backup](https://support.google.com/android/answer/2819582?hl=en):

> Restoring data varies by phone and Android version. You can't restore a backup from a higher Android version onto a phone running a lower Android version.

Tried to restore a backup made on 8.1.0 on a 7.0 system; didn't restore; app doesn't even close on restore.  Restored on 12 (app closes during restore).

I guess that old phone in the drawer isn't that useful anymore to use as a backup for running apps requiring your app data.

Here's a list of versions listed in the backup created by the specified version of Android:

|Android|Backup file|
|-------|----------:|
|4.0.4  |          1|
|4.2.2  |          1|
|4.4.2  |          1|
|5.0.2  |          3|
|6.0    |          3|
|7.0    |          4|
|8.0.0  |          5|
|8.1.0  |          5|
|9      |          5|
|10     |          5|
|12     |          5|

(There's a backup version 2 done by Kitkat MR2).

Can look for `BACKUP_FILE_VERSION` in AOSP:

- `frameworks/base/services/backup/java/com/android/server/backup/UserBackupManagerService.java`
   - for Android [10](https://cs.android.com/android/platform/superproject/+/android-10.0.0_r1:frameworks/base/services/backup/java/com/android/server/backup/UserBackupManagerService.java;drc=e0399328c95ffbe9fd03e97f91c5ee0622a27374;l=192?q=BACKUP_FILE_VERSION)+
- `frameworks/base/services/backup/java/com/android/server/backup/BackupManagerService.java`
   - for Android [Lollipop](https://cs.android.com/android/platform/superproject/+/android-5.0.0_r1.0.1:frameworks/base/services/backup/java/com/android/server/backup/BackupManagerService.java;drc=7f0b453bb006fe274da68d04cad54f1ac39b3268;l=185?q=BACKUP_FILE_VERSION) to [Pie](https://cs.android.com/android/platform/superproject/+/android-9.0.0_r61:frameworks/base/services/backup/java/com/android/server/backup/BackupManagerService.java;drc=92892163d58b580056d38f6ca2c93fb714b9e4b8;l=191?q=BACKUP_FILE_VERSION)
   - has a version history describing the format changes
- `frameworks/base/services/java/com/android/server/BackupManagerService.java`
   - for Android [ICS](https://cs.android.com/android/platform/superproject/+/android-4.0.1_r1:frameworks/base/services/java/com/android/server/BackupManagerService.java;drc=b9c1acfb0b4a41ffb5a4d9c38ef298c3a1eb9599;l=140?q=BACKUP_FILE_VERSION) to [Kitkat](https://cs.android.com/android/platform/superproject/+/android-4.4.3_r1:frameworks/base/services/java/com/android/server/BackupManagerService.java;drc=4bda43777c72259cb5f1fb923a99584fb53a88ce;l=151?q=BACKUP_FILE_VERSION)

The backup version is the 2nd line in the backup header, which contains ASCII text describing the format of the blob that follows.

### Troubleshooting

#### Restore doesn't work
Here are some symptoms and their possible cause/resolution:
- Symptom: open app does not close
   - Cause: backup may not contain the data for that package
      - incorrect package name specified during backup creation
   - Cause: backup version may be incorrect (Android does not recognize too high backup version)
- Symptom: open app closes / restarts, but nothing changed
   - Cause: certain parts may not be backed up
      - see [Excluded Data](#excluded-data) (developer explicitly excluded the piece you are looking at)
   - Cause: the piece is saved in shared storage
      - see [External Data](#External Files)
      - back up shared storage instead (but too late now unless you happen to have the source still available)
- Other causes
   - App may have declared that the recorded app version and the installed version must match

### Quirks

#### Backup and restore not working reliably
Kept getting an empty backup, first with v1.0.41, then with v1.0.36.
Then tried to start the app and have the backup process close it to ensure the data is up-to-date.
That way I know the package name is correct since it would close the app during backup.
It would then give me a backup that I can restore on another phone.

However, cannot restore to the source phone.  It would just wipe out the existing data and nothing would be restored.

Running `adb logcat -s 'BackupManagerService'`:

```
10-31 21:39:55.946  1808  6745 D BackupManagerService: Unable to create agent for <package name>
```

Normally should get:

```
11-01 14:37:43.900   756  6779 I BackupManagerService: --- Performing full-dataset restore ---
11-01 14:37:43.939   756  6779 I BackupManagerService: Sig + version match; taking data
11-01 14:37:43.939   756  6779 D BackupManagerService: Need to launch agent for <package name>
11-01 14:37:43.939   756  6779 D BackupManagerService: Clearing app data preparatory to full restore
11-01 14:37:44.410   756  6779 D BackupManagerService: awaiting agent for ApplicationInfo{83ec854 <package name>}
11-01 14:37:44.746   756  1193 D BackupManagerService: agentConnected pkg=<package name> agent=android.os.BinderProxy@746c8f2
11-01 14:37:44.747   756  6779 I BackupManagerService: got agent android.app.IBackupAgent$Stub$Proxy@7897b43
11-01 14:37:44.748   756  6779 D BackupManagerService: Invoking agent to restore file <file>
```

When backing up:

```
11-01 14:42:14.933   756  2995 V BackupManagerService: Requesting backup: apks=false obb=false shared=false all=false system=true includekeyvalue=false pkgs=[Ljava.lang.String;@304dd5e
11-01 14:42:14.933   756  2995 I BackupManagerService: Beginning adb backup...
11-01 14:42:14.934   756  2995 D BackupManagerService: Starting backup confirmation UI, token=1062394088
11-01 14:42:14.985   756  2995 D BackupManagerService: Waiting for backup completion...
11-01 14:42:22.051   756  3025 D BackupManagerService: acknowledgeAdbBackupOrRestore : token=1062394088 allow=true
11-01 14:42:22.055   756  7387 I BackupManagerService: --- Performing adb backup ---
11-01 14:42:22.085   756  7387 I BackupManagerService: --- Performing full backup for package <package name> ---
11-01 14:42:22.143   756  7387 D BackupManagerService: awaiting agent for ApplicationInfo{83ec854 <package name>}
11-01 14:42:22.289   756  3026 D BackupManagerService: agentConnected pkg=<package name> agent=android.os.BinderProxy@252bafc
11-01 14:42:22.289   756  7387 I BackupManagerService: got agent android.app.IBackupAgent$Stub$Proxy@d1a9585
11-01 14:42:22.294   756  7426 D BackupManagerService: Calling doFullBackup() on <package name>
11-01 14:42:22.444   756  2995 D BackupManagerService: Adb backup processing complete.
11-01 14:42:22.446   756  7387 D BackupManagerService: Full backup pass complete.
11-01 14:42:45.043   756  2875 D BackupManagerService: Timeout message received for token=3d6a0665
```

Not sure if it's related to encryption.
Encrypted phone (encrypted storage with a password set) makes sure the backup is encrypted too.
