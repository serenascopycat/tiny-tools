#!/bin/sh
### WHAT THIS DOES #####################
# Extract files from unencrypted Android ADB backup.
# https://android.stackexchange.com/questions/28481/how-do-you-extract-an-apps-data-from-a-full-backup-made-through-adb-backup/78183#78183
# This will NOT handle all backups;
# only meant to handle simple cases without requiring additional software
# by mangling the backup to look similar to standard file types.
### HOWTO ##############################
# 1) Run:
#  i) parameter1: path of backup file
# Extracted files will be placed in a subdirectory of current working directory
# with the same name but a .out suffix.
### LICENSING ##########################
# Copyleft 2014 Kari, Serena's Copycat; licensed under CC BY-SA 3.0
# backup_as_gzipped_tar() adapted from StackExchange answer;
# see question link for code; licensed under CC BY-SA 3.0
# Copycenter 2022 Serena's Copycat; portions licensed under CC0
# Portions beside the adapted code in backup_as_gzipped_tar()
# may be used as an independent component with license under CC0

# get name of backup, without .ab or .bak suffix, with .out appended
get_basename() {
	local name="$( basename "${1}" )"
	for suffix in .ab .bak ; do
		local name_wo_suffix="${name%${suffix}}"
		[ "${name}!" = "${name_wo_suffix}!" ] || {
			name="${name_wo_suffix}"
			break
		}
	done
	echo "${name}.out"
}

# Dump a mangled backup that looks like a .tgz
backup_as_gzipped_tar() {
	local backup="${1}"
	local headerlinesbytes
	# verify magic in first line, then check 4th line to make sure it's unencrypted
	# limit to 512 bytes first in case it's gibberish with no newlines
	if [ 'ANDROID BACKUP!' = "$( head -c 15 "${backup}" )!" ] && \
			headerlinesbytes="$( head -c 512 "${backup}" | head -n 4 | wc -c )" && \
			[ 'none!' = "$( head -c "${headerlinesbytes}" "${backup}" | head -n 4 | tail -n 1 )!" ] ; then
		printf '\037\213\010\000\000\000\000\000'
		tail -c +$(( ${headerlinesbytes} + 1 )) "${backup}"
	fi
}

has_contents() {
	for f in "${out_dir}"/* ; do
		[ -e "${f}" ] && return 0
	done
	return 1
}

extract_to() {
	local backup="${1}"
	local out_dir="${2:-"$( get_basename "${1}" )"}"
	[ -d "${out_dir}" ] || mkdir "${out_dir}"
	has_contents "${out_dir}" && { echo "${out_dir} already has contents; not extracting." ; return ; }
	if ! { backup_as_gzipped_tar "${backup}" | tar xfvz - -C "${out_dir}" || has_contents "${out_dir}" ; } ; then
		echo "Retrying."
		# in case tar doesn't even try if gzip fails somewhere
		backup_as_gzipped_tar "${backup}" | zcat | tar xfv - -C "${out_dir}"
	fi
}

[ -n "${1}" ] && { set -e ; extract_to "${1}" ; }
