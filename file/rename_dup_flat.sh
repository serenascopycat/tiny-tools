#!/bin/sh
### WHAT THIS DOES #####################
# Tags duplicate files by walking directory tree, and in a specified directory
# compares the files with the same name, and, if matched, 
# adds extension .dup[date] to the file in the specified directory.
# This differs from the tree version in that instead of parallel tree,
# this only renames files directly in the specified directory.
### HOWTO ##############################
# 1) CD into diretory with the original files (won't be renamed)
# 2) Run script with argument
#  a) directory containing files to rename
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

. "$( dirname "$0" )/walk_tree.sh"
check_args "$@"
dup_suffix="${2:-".dup$( date -u +'%y%m%d' )"}"
cmd_path_parallel="\"${cmd_var_target_dir}/"'$( basename "'"${cmd_var_path}"'" )"'
cmd_check="[ -f ${cmd_path_parallel} -a ! \"${cmd_var_path}\" -ef ${cmd_path_parallel} ]"
cmd_compare="cmp \"${cmd_var_path}\" ${cmd_path_parallel}"
cmd_rename="mv -n ${cmd_path_parallel} ${cmd_path_parallel}\"${dup_suffix}\""
walk_tree "${cmd_check} && ${cmd_compare} && ${cmd_rename}" print "${1}"
