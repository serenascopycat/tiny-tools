# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Common tree-walk code
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

check_args() {
	if [ -z "${1}" ]
	then
		echo 'No search path specified.' >&2
		exit 1
	elif [ ! -d "${1}" ]
	then
		echo 'Search path ('"${1}"') is not a directory.' >&2
		exit 1
	elif [ "${1}" -ef . ]
	then
		echo 'Search path is same as current directory.' >&2
		exit 1
	else
		return 0
	fi
}

walk_tree() {
	cmd="${1}"
	do_print="${2}"
	search_dir="${3}"
	if [ -n "${do_print}" ] ; then
		if [ "${do_print}!" = "print!" ] ; then
			do_print=-print
		elif [ "${do_print}!" = "0!" -o "${do_print}!" = "noprint!" ] ; then
			do_print=
		fi
	fi
	nice -n 20 find . -type f -exec sh -c "${cmd}" "${search_dir%/}" {} '{' '}' \; ${do_print}
}
cmd_var_target_dir='$0'
cmd_var_path='$1'
cmd_var_nest_path='$2$3'

