#!/usr/bin/env python2
"""Runs through zip file's entries and either print the names or compare data, depending on how this script is invoked.  Can provide encoding explicitly for zip files with filenames in indeterminate encoding (not UTF-8).

Prints filenames with backslash escapes when encoding can't be determined.

Zip file content comparison will be a byte-wise compare to extracted copy.
Assumes the zip file is extracted with the directory structure maintained.
By default, looks at the directory where the zip file is or subdirectory with same name as the zip file without the .zip suffix.
Renames matched extracted files by appending .dupYYMMDD suffix.
Somewhat similar to 'zcmp' in functionality.

Works on both Python2 (2.7) and Python3 (3.4).
Python2 is preferred because it provides raw filename.

Command line usage:
  python zipcompare.py [<options>] file.zip <encoding>
    Compare mode: Compares zip contents to extracted copy
  python ziplist.py [<options>] file.zip <encoding>
    Implicit list mode: Prints names of files in zip file
  Options include:
    -i	Case-insensitive filename matches (on case-sensitive filesystems)
    -l  List mode (prints out filenames in the zip file)
    -p  Print matches
Tip: Symbolically link ziplist.py to zipcompare.py to access implicit list mode.
Note: Explicitly use 'python2' (preferred) or 'python3' instead of 'python'.
Hint: Standard encodings are listed at:
  https://docs.python.org/2/library/codecs.html#standard-encodings

Copycenter 2021 Serena's Copycat; licensed under CC0
"""
from datetime import date
import os
import sys
import zipfile

class Zip:
    @staticmethod
    def isPython3Unicode():
        """Tests to see if str handles Unicode.
        The existence of unicode type and special handling for that follows."""
        return '\u000a' == '\n'

    class FilenameEncoding:
        # same as inverse of Zip.isPython3Unicode but Zip not yet defined
        if '\u000a' != '\n':    # Python 2 str won't handle Unicode escapes
            # Python 2 needs explicit handling for Unicode strings
            @staticmethod
            def reencode(zefn):
                """Attempts to get original encoded form by recoding as necessary.
                Python2 version of 'zipfile' already returns the raw encoded byte string
                unless filename encoding flag bit is set, so this just passes through.
                """
                # https://python-forum.io/thread-591-post-3489.html#pid3489
                # Python 2 uses File.read()
                return zefn
            @staticmethod
            def decode4print(encoded):
                """Get an ASCII representation of the filename"""
                # encoded original --encode (transform)--> encoded ASCII w/escapes --decode--> decoded Unicode output w/escapes
                return encoded.encode(encoding="string_escape").decode(encoding="ascii")
            @staticmethod
            def _wrap(name):
                """Decode raw filesystem names"""
                return unicode(name, encoding=sys.getfilesystemencoding())
        else:
            # Python 3 str type includes Unicode and no longer has unicode type
            @staticmethod
            def reencode(zefn):
                """Attempts to get original encoded form by recoding as necessary.
                Python3 version of 'zipfile' decodes with cp437
                unless filename encoding flag bit is set,
                so this attempts to retrieve the original by reencoding back with cp437.
                """
                # Python 3 hard-codes non-Unicode filenames to be decoded with cp437
                # https://github.com/python/cpython/blob/3.0/Lib/zipfile.py
                return zefn.encode(encoding="cp437")
            @staticmethod
            def decode4print(encoded):
                """Get an ASCII representation of the filename"""
                # encoded original --decode--> decoded Unicode output w/escapes
                return encoded.decode(encoding="ascii", errors="backslashreplace")
            @staticmethod
            def _wrap(name):
                """Decode raw filesystem names; does nothing --
                Python3 should already be using Unicode filenames."""
                return name

        @staticmethod
        def translator(encoding):
            """Decode zip entry filenames using specified encoding,
            first backing up to original raw string when necessary."""
            return lambda zefn: (
                Zip.FilenameEncoding.reencode(zefn)
                    .decode(encoding=encoding))

    @staticmethod
    def entryfileloop(filename, filenametranslator = None, action = None):
        """Loop over file entries within zip file"""
        if None == action:
            def printName(z, zi, zefn): print(zefn)
            action = printName
            if None == filenametranslator:
                filenametranslator = lambda zefn: Zip.FilenameEncoding.decode4print(Zip.FilenameEncoding.reencode(zefn))
        elif None == filenametranslator:
            # make assumption that the zip entry filename is in the same encoding as the filesystem
            filenametranslator = Zip.FilenameEncoding.translator(sys.getfilesystemencoding())
        with zipfile.ZipFile(filename) as z:
            for zi in z.infolist():
                # redo decoding if filename encoding is not UTF-8
                zefn = zi.filename if zi.flag_bits & 0x800 else filenametranslator(zi.filename)
                if not zefn.endswith("/"): action(z, zi, zefn)

    ## Compare and helpers for compare #########################################
    class PathGetterFactory:
        """Namespace for grouping PathGetter factories"""
        @staticmethod
        def rootdir(zipfilename):
            """Produce a function that returns file paths to look at,
            based on zip path and zip entry path.
            Returned function will list the following paths:
              <zip_dir>/<zip_entry_path>
              <zip_dir>/<zip_base_name>/<zip_entry_path>
            e.g. for zip file at /path/to/zfile.zip containing entry x/y.txt:
              /path/to/x/y.txt
              /path/to/zfile/x/y.txt"""
            dn, fn = os.path.split(zipfilename)
            bn, ext = os.path.splitext(fn)
            return lambda entryname: (
                os.path.join(dn, entryname),
                os.path.join(dn, bn, entryname))

        @staticmethod
        def rootdir_ignorecase(zipfilename):
            """Same as rootdir, but
            for when the extraction happens on a case-insensitive filesystem,
            but the comparison is done on a case-sensitive filesystem."""
            # zipfilename should be Unicode
            dircache = {}
            def getdiritemsignorecase(dn, name):
                name = name.lower()
                if not dn in dircache: dircache[dn] = os.listdir(dn)
                result = []
                for n in dircache[dn]:
                    if n.lower() == name:
                        result.append(n)
                return result
            # split into directory name and filename
            dn, fn = os.path.split(zipfilename)
            # split into base name and extension
            bn, ext = os.path.splitext(fn)

            # important: dn must be Unicode when run with Python2
            # otherwise generated paths will be str,
            # causing os.listdir to return str rather than unicode
            dn = dn or u"."
            # the subdirectory name could still differ from filename by case
            bn = getdiritemsignorecase(dn, bn)
            class PathSplitter:
                def _splitentry(self):
                    edn, ebn = os.path.split(self.path)
                    if ebn: self.split.append(ebn)
                    if edn != self.path:
                        self.path = edn
                        return True
                    elif "/" == edn or not edn:
                        return False
                    else:
                        self.split.append(edn)
                        return False
                def split(self, entryname):
                    self.path = entryname
                    self.split = []	# path components in reverse order
                    while self._splitentry(): pass
                    return self.split

            def pathgetter(entryname):
                result = []
                splitentry = PathSplitter().split(entryname)
                def scanlevel(atpathsplit, level):
                    if 0 == level:
                        def procitem(finalpath):
                            result.append(os.path.join(*finalpath))
                    else:
                        def procitem(atpath):
                            scanlevel(atpath, level=level - 1)
                    for e in getdiritemsignorecase(os.path.join(*atpathsplit), splitentry[level]):
                        procitem(atpathsplit + (e,))
                scanlevel((dn), level=(len(splitentry) - 1))
                for b in bn:
                    scanlevel((dn, b), level=(len(splitentry) - 1))
                return result
            return pathgetter

    class OnMatch:
        """Namespace for grouping match actions"""
        @staticmethod
        def printout(zipfilename, zipfile, zipinfo, zipinfofilename, matchpath):
            print("matched (zip)/%s <==> %s" % (zipinfofilename, matchpath))

        @staticmethod
        def rename(zipfilename, zipfile, zipinfo, zipinfofilename, matchpath):
            newpath = matchpath + ".dup" + date.today().strftime("%y%m%d")
            if not os.path.exists(newpath):
                os.rename(matchpath, newpath)

    @staticmethod
    def _comparereadables(a, b, bufSize = 4 * 1024):
        # from filecmp.py _do_cmp()
        while True:
            ab = a.read(bufSize)
            if b.read(bufSize) != ab:
                return False
            elif not ab: # both reached end
                return True

    @staticmethod
    def compare(filename, filenametranslator = None, pathgetter = None, onmatch = None):
        """Given a filename, open it as a zip file and compare
        to files returned by pathgetter."""
        if None == pathgetter:
            # pathgetter is function
            # to get paths on filesystem that line up with zip file entry
            pathgetter = Zip.PathGetterFactory.rootdir(filename)
        if None == onmatch:
            onmatch = OnMatch.printout
        def docompare(z, zi, zefn):
            for p in pathgetter(zefn):
                if os.path.exists(p):
                    compareOK = False
                    try:
                        compareOK = (os.path.getsize(p) == zi.file_size)
                    except FileNotFoundError:
                        pass
                    if compareOK:
                        with z.open(zi) as zf, open(p, "rb") as f:
                            compareOK = Zip._comparereadables(zf, f)
                    if compareOK: onmatch(filename, z, zi, zefn, p)
        Zip.entryfileloop(filename, filenametranslator, docompare)

if "__main__" == __name__:
    import re
    class MainRunner:
        """Commandline zipcompare client"""
        def __init__(self):
            self.zipfile = None
            self.action = self.comparezip
            self.nametranslator = None
            self.listmode = False
            self.pathgetterfactory = Zip.PathGetterFactory.rootdir
            self.matchaction = Zip.OnMatch.rename

        ENCODING_DOC3_URL = "https://docs.python.org/3.5/library/codecs.html#standard-encodings"
        ENCODING_DOC2_URL = "https://docs.python.org/2/library/codecs.html#standard-encodings"
        _ispy3 = Zip.isPython3Unicode()

        @staticmethod
        def getusage(scriptname, listmode):
            return "\n".join([
                "Usage (%s mode):",
                "    %s %s %s file.zip [encoding]"]) % (
                    "list" if listmode else "compare",
                    sys.executable or
                        ("python3" if MainRunner._ispy3 else "python2"),
                    scriptname, "-l" if listmode else "[-i] [-p]")
        @staticmethod
        def getencodingurl():
            return (MainRunner.ENCODING_DOC3_URL if MainRunner._ispy3
                    else MainRunner.ENCODING_DOC2_URL)
        @staticmethod
        def getencodingdoc():
            return "\n".join([
                "Standard encodings:",
                "    %s" % MainRunner.getencodingurl()])
        def printusage(self, scriptname = sys.argv[0], listmode = None):
            """Prints dynamic usage information"""
            if None == listmode: listmode = self.listmode
            print("\n".join((
                MainRunner.getusage(scriptname, listmode),
                MainRunner.getencodingdoc())))
            if MainRunner._ispy3:
                print("Note: designed for Python 2.7 "
                      "but will work on Python3 (tested with 3.4).")

        def activatelistmode(self):
            self.listmode = True
            self.action = Zip.entryfileloop
        def setencoding(self, encoding):
            self.nameenc = Zip.FilenameEncoding.translator(encoding)

        def setupopts(self, args):
            class OptParser:
                def __init__(self):
                    self.posarghandler = self.optposhandler1

                def _getcurarg(self, offset = 0):
                    return self.args[self.argcur + offset]
                def _moveargcur(self, move = 1):
                    self.argcur = self.argcur + move
                def opthandler_nonamearg(self, runner):
                    self.namearghandler = None
                def opthandler_listmode(self, runner):
                    runner.activatelistmode()
                def opthandler_ignorecase(self, runner):
                    runner.pathgetterfactory = Zip.PathGetterFactory.rootdir_ignorecase
                def opthandler_printmatch(self, runner):
                    runner.matchaction = Zip.OnMatch.printout
                namearghandler = {
                    "-i": opthandler_ignorecase,
                    "-l": opthandler_listmode,
                    "-p": opthandler_printmatch,
                    "--": opthandler_nonamearg,
                    }
                def optposhandler1(self, runner):
                    runner.zipfile = Zip.FilenameEncoding._wrap(self._getcurarg())
                    self.posarghandler = self.optposhandler2
                def optposhandler2(self, runner):
                    runner.setencoding(self._getcurarg())
                    self.posarghandler = None
                def start(self, runner, args):
                    # runner should be MainRunner instance
                    self.args = args
                    self.argcur = 1
                    self.namearghandler = OptParser.namearghandler
                    lastarg = len(args)
                    while self.argcur < lastarg:
                        curarg = self._getcurarg()
                        if self.namearghandler and curarg in self.namearghandler:
                            self.namearghandler[curarg](self, runner)
                        elif self.posarghandler:
                            self.posarghandler(runner)
                        else:
                            print("Unknown option %s" % (self._getcurarg()))
                            return False
                        self._moveargcur()
                    return True
            if not OptParser().start(self, args): return False
            return True

        def setupscriptnamehint(self, scriptname):
            if re.match("^(?:zip)?list(?:.py)?$", scriptname, re.IGNORECASE):
                self.activatelistmode()
            return True

        def comparezip(self, filename, filenametranslator):
            Zip.compare(
                filename, filenametranslator,
                self.pathgetterfactory and
                    self.pathgetterfactory(filename) or None,
                self.matchaction)

        def run(self):
            if not self.zipfile:
                print("No zip file specified")
                return False
            elif not os.path.isfile(self.zipfile):
                print("Can't find zip file: %s" % self.zipfile)
                return False
            else:
                self.action(self.zipfile, self.nametranslator)
                return True

        def go(self, args):
            self.setupscriptnamehint(os.path.basename(args[0]))
            self.setupopts(args) and self.run() or self.printusage(args[0])

    MainRunner().go(sys.argv)
