#!/bin/sh
### WHAT THIS DOES #####################
# Moves files into a parallel directory structure, based on name
### HOWTO ##############################
# 1) CD into directory which has the desired directory structure
# 2) Run script with argument
#  a) directory with files to move into a matching directory structure
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

. "$( dirname "$0" )/walk_tree.sh"
check_args $@
dup_suffix="${2:-".dup$( date -u +'%y%m%d' )"}"
cmd_path_parallel="\"${cmd_var_target_dir}/"'$( basename "'"${cmd_var_path}"'" )"'
cmd_dir_parallel="\"${cmd_var_target_dir}/"'$( dirname "'"${cmd_var_path}"'" )"'
cmd_check="[ -f ${cmd_path_parallel} ]"
cmd_mkdir="mkdir -p ${cmd_dir_parallel}"
cmd_move="mv -n ${cmd_path_parallel} ${cmd_dir_parallel}"
walk_tree "${cmd_check} && ${cmd_mkdir} && ${cmd_move}" print "${1}"
