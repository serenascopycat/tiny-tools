#!/bin/sh
### WHAT THIS DOES #####################
# Goes block-by-block via dd, scanning for zero blocks,
# copying only blocks which are not all zeroes.
# Essentially copies out a file as sparse.
### DEPENDENCIES #######################
# dd (busybox)
### NOTES ##############################
# BusyBox generally doesn't handle sparse files except for its dd.
# cp, mv and tar will all turn a sparse file into a fully-allocated file.
# A bit annoying when trying to move a partially-completed torrent.
# OpenWRT doesn't have utilities like fallocate installed by default,
# so this is a fill-in tool to get a sparse file.
# This doesn't dig holes in place and runs through the file block-by-block,
# extracting blocks multiple times, so this is SLOW.
# IMPORTANT: The final block is currently treated as a data block
# because it doesn't match a full block of zeroes.
### HOWTO ##############################
# 1) Run script
#    - input file (to scan for zero blocks)
#    - output file (defaults to input file with .sparse suffix)
#       - WARNING: will not check for existing file and will just overwrite
#    - block size
#       - should be filesystem allocation size
#          - probably 4kiB for ext*
### LICENSING ##########################
# Copycenter 2024 Serena's Copycat; licensed under CC0

set -e

mk_tempzeroblock() {
    local tempzero="${1}"
    local bs="${2}"
    dd if=/dev/null of="${tempzero}" bs="${bs}" seek=1 count=0 2>/dev/null
}
on_blockzero() {
    local infile="${1}"
    local outfile="${2}"
    local bs="${3}"
    local offsetblock="${4}"
}
on_blockdata() {
    local infile="${1}"
    local outfile="${2}"
    local bs="${3}"
    local offsetblock="${4}"
    dd if="${infile}" of="${outfile}" bs="${bs}" skip="${offsetblock}" seek="${offsetblock}" count=1 2>/dev/null
}
on_zerostatus() {
    [ -n "${zerostart}" ] && echo "Zero blocks: ${zerostart} - ${zeroprev}" || true
}
loop_fileblocks() {
    local infile="${1}"
    local outfile="${2}"
    local bs="${3}"
    local onzero="${4:-on_blockzero}"
    local ondata="${5:-on_blockdata}"
    local stop= offsetblock=0 handler zerostart= zeroprev=
    while [ -z "${stop}" ] ; do
        if dd if="${infile}" bs="${bs}" skip="${offsetblock}" count=1 2>/dev/null | cmp -s "${tempzero}" - ; then
            [ -z "${zerostart}" ] && zerostart="${offsetblock}"
            handler="${onzero}"
            zeroprev="${offsetblock}"
        else
            on_zerostatus
            zerostart=
            zeroprev=
            [ "$( dd if="${infile}" bs="${bs}" skip="${offsetblock}" count=1 2>/dev/null | wc -c )!" = "${bs}!" ] || stop=1
            # TODO Handle partial blocks of zeroes specially
            handler="${ondata}"
        fi
        "${handler}" "${infile}" "${outfile}" "${bs}" "${offsetblock}"
        offsetblock="$(( ${offsetblock} + 1 ))"
    done
    on_zerostatus
}

infile="${1}"
outfile="${2:-"${1}.sparse"}"
[ "${infile}" -ef "${outfile}" ] && {
    echo "In-place hole digging not implemented." >&2
    echo "Must write to new sparse file." >&2
    false
}
bs="${3:-4096}"         # filesystem block size
tempzero="$( mktemp -p /tmp zeroblkXXXXXX )"
mk_tempzeroblock "${tempzero}" "${bs}"
loop_fileblocks "${infile}" "${outfile}" "${bs}"
rm "${tempzero}"
cmp -s "${infile}" "${outfile}" && echo OK || { echo "Failed to recreate sparse file with matching data." >&2 ; false ; }
