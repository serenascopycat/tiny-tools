#!/bin/sh
### WHAT THIS DOES #####################
# Find a file in the search path with the same contents
# and rename files directly under current directory to that.
### HOWTO ##############################
# 1) CD into directory with files to be renamed
# 2) Run script with argument
#  a) directory with files having the desired names
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

. "$( dirname "$0" )/walk_tree.sh"
check_args "$@"

# Inner-find: process each file to be renamed
# $0 is the file to be renamed
# $1 is the file to check against
loop_search_single='[ -f "$0" ]'
loop_search_single="${loop_search_single} && "'[ ! "$0" -ef "$( dirname "$0" )/$( basename "$1" )" ]'
loop_search_single="${loop_search_single} && "'cmp -s "$0" "$1"'
loop_search_single="${loop_search_single} && "'mv -n "$0" "$( dirname "$0" )/$( basename "$1" )"'
# make a link with the old name just in case
# this was called the wrong way and need to name stuff back
loop_search_single="${loop_search_single} && "'ln -s "$( dirname "$0" )/$( basename "$1" )" "$0"'
loop_search_single="${loop_search_single} && "'echo "$0 ==> $( dirname "$0" )/$( basename "$1" )"'

cmd_path_parallel="\"${cmd_var_target_dir}/"'$( basename "'"${cmd_var_path}"'" )"'
cmd_dir_parallel="\"${cmd_var_target_dir}/"'$( dirname "'"${cmd_var_path}"'" )"'

loop_search="[ -d ${cmd_dir_parallel} ]"
loop_search="${loop_search} && find ${cmd_dir_parallel} -maxdepth 1 -type f"
loop_search="${loop_search} -exec sh -c '${loop_search_single}' \"${cmd_var_path}\" \"${cmd_var_nest_path}\" \;"

walk_tree "${loop_search}" noprint "${1}"
