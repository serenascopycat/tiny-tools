#!/bin/sh
### WHAT THIS DOES #####################
# Copies files, retaining hard-links, across 2 ext2/3/4 FS's.
# Done by creating a lookup directory in the "work" directory
# that links the files by their source inode numbers.
# Source files will be looked up and simply linked if found.
# Can also regenerate lookup directory to resume the copy,
# by placing a .stale file in the lookup directory.
### NOTES ##############################
# Designed for files with content that won't update (e.g. Bittorrent downloads).
# If the file content changes (e.g. retagging ID3 in MP3),
# this script will likely behave in undesired ways.
# Note: file matches are checked via cmp; not by checksum/date/size.
# Note: soft links are ignored.
# Note: when rsync is available, can use that instead.
# Caveat: lookup, being a cache, will not handle
#  * changed files (will use cache, creating a link but not update contents),
#  * multiple independent source FS's with the same UUID,
#      as they may have used the same node ID for different files,
#  * or even re-used node IDs within source FS.
#  If in doubt, regenerate lookup by clearing lookup/working directory,
#  then place a .stale file within it.  However, if file contents changed
#  since prior run, hard links may be lost, as the prior run's files will
#  not land in the lookup since its old contents will not match new content.
### HOWTO ##############################
# 1) CD into destination directory [optional]
# 2) Run script with argument
#  a) source directory
#  b) destination directory (will default to $PWD)
#  c) work directory (will default to .src_node within destination)
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

create_find_sh_cmd() {
	local mode="${1}"

	# sh will be called
	# with [0:dst_dir, 1:lookup_dir, 2:src_relative_path]
	# and working directory = source directory
	cmd_set_inode='n="$( ls -i "$2"  | awk '"'"'{ printf "%03i", $1 }'"'"' )"'
	cmd_set_fs_uuid='u="$( awk -v dev="$( df -P "$2" | awk '"'"'END { print $1 }'"'"' )" '"'"'$1 == dev { print $2 ; exit 0 }'"'"' "${1}/fs_uuid.lst" )"'
	cmd_set_lookup='p="$1/${u}/${n:0-3}/${n}"'

	# create lookup node from file in dst
	cmd_setup_node_dir='mkdir -p "$( dirname "$p" )"'
	cmd_setup_node_ln='ln "$0/$2" "$p"'
	cmd_setup_node_from_dst="$cmd_setup_node_dir ; $cmd_setup_node_ln"

	# regen lookup mode
	cmd_check_no_inode='[ ! -e "$p" ]'

	# copy mode
	cmd_check_inode='[ -f "$p" ]'
	cmd_link_inode='ln "$p" "$0/$2"'
	cmd_setup_new_cp='cp -a "$2" "$0/$2"'

	cmd_show_src='echo $2'

	if [ "${mode}!" == "regen!" ] ; then
		cmd_all='if [ -e "$0/$2" ] && cmp "$2" "$0/$2" ; then '"$cmd_set_inode ; $cmd_set_fs_uuid ; $cmd_set_lookup ; if $cmd_check_no_inode ; then $cmd_setup_node_from_dst ; $cmd_show_src ; fi fi"
	else
		cmd_all='if [ ! -e "$0/$2" ] ; then '"$cmd_set_inode ; $cmd_set_fs_uuid ; $cmd_set_lookup ; if $cmd_check_inode ; then $cmd_link_inode ; $cmd_show_src linked ; else $cmd_setup_new_cp ; $cmd_setup_node_from_dst ; $cmd_show_src created ; fi fi"
	fi

	echo $cmd_all
}

if [ -z "${1}" ]
then
	echo 'No source path specified.' >&2
	exit 1
elif [ ! -d "${1}" ]
then
	echo 'Source path is not a directory.' >&2
	exit 1
elif [ "${1}" -ef "${2:-.}" ]
then
	echo 'Source path is same as destination directory.' >&2
	exit 1
else
	src_dir="${1}"
	dst_dir="${2:-${PWD}}"
	wrk_dir="${3:-${dst_dir}}/.src_node"

	[ -e "${wrk_dir}" ] || mkdir "${wrk_dir}"
	(blkid || block info) | sed -n 's/^\(.*\): UUID="\([A-Za-z0-9-]*\)".*$/\1\t\2/p' > "${wrk_dir}/fs_uuid.lst"

	mode=""
	if [ -e "${wrk_dir}/.stale" ] ; then
		echo Regenerate Lookup Mode
		mode=regen
	fi

	( cd "${src_dir}" ; nice -n 20 find . \( \
		\( -type d -exec sh -c 'mkdir -p "$0/$1"' "${dst_dir}" {} \; \) -o \
		\( -type f -exec sh -c "$(create_find_sh_cmd "${mode}")" "${dst_dir}" "${wrk_dir}" {} \; \) \) )

	if [ -e "${wrk_dir}/.stale" ] && cmp "${wrk_dir}/.stale" /dev/null > /dev/null ; then
		# remove flag file (empty file)
		rm "${wrk_dir}/.stale"
	fi
fi
