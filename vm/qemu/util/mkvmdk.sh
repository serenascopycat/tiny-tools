#!/bin/sh
### WHAT THIS DOES #####################
# Creates a VMDK file suitable for use as live CD persistence.
# The setup can both be used as a VM disk or for bare-metal access.
### DEPENDENCIES #######################
# QEMU (qemu-img, qemu-nbd)
# sfdisk (with named-fields format support)
# mkfs.ext3
### NOTES ##############################
# When attempting to resize an extent, need to:
# - backup (optional but full backup recommended)
#  - data loss if tripped up at partition table edit or FS resize
# - resize extent file
# - resize filesystem
#  - can resize as part of partition table edit when using gparted's resize
# - adjust VMDK extent description to match extent file
# - adjust VMDK geometry
# - repartition (edit partition table)
#  - can remove persistent boot option temporarily, boot, edit partition table
#  - inconsistent partition leads to casper boot failure and drop to initramfs shell
# Alternatively, can skip the last 3 steps by generating new VMDK **ELSEWHERE**
# with this script, then replace extents with old ones.
# NOT RECOMMENDED to regenerate VMDK on top of existing one with this script.
# This script WILL CLOBBER the old extents with mkfs.ext3 calls.
# mkfs.ext3 should prompt you if that's the case but don't bet on it.
### HOWTO ##############################
# 1) Adjust partition table image filename (parttblname; optional)
# 2) Adjust filenames and sizes in make_vmdkextents()
#  - defaults to files with older casper naming
#   - note that casper only allows persistence files in FAT filesystems
#    - cannot reach 4GiB
#     - 8388607 512-byte sectors absolute maximum
#     - 8386560 512-byte sectors to keep alignment to MiB
#    - file creation will be slow as the file gets fully allocated and written
# 3) Change mkfs.ext3 to mkfs.ext4 (optional; if preferred)
# 4) Run script with parameters
#  i) path to create VMDK at
#   - defaults to hda.vmdk in current directory
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

vmdkpath="${1:-hda.vmdk}"
parttblname="mbr.img"
geomheads=16
geomsects=63

_make_blankimg() {
	local path="${1:-$( dirname "${vmdkpath}" )}"
	local filename="${2}"
	local sectorcount="${3}"
	dd if=/dev/zero of="$( dirname "${vmdkpath}" )/${filename}" bs=512 seek="${sectorcount}" count=0
	vmdkextents="${vmdkextents}$( [ -n "${vmdkextents}" ] && printf '\n\t' || printf '\t' )RW ${sectorcount} FLAT \"${filename}\" 0"
	sectorsexpr="${sectorsexpr}$( [ -n "${sectorsexpr}" ] && echo ' + ' || : )${sectorcount}"
}
make_parttbl() {
	local path="${1}"
	local filename="${2}"
	local sectorcount="${3:-2048}"	# 1MiB
	_make_blankimg "${path}" "${filename}" "${sectorcount}"
	sfdiskstart="start=${sectorcount}, "
}
mkxt_parttbl() {
	local filename="${1}"
	local sectorcount="${2:-2048}"	# 1MiB
	shift 2
	make_parttbl "$( dirname "${vmdkpath}" )" "${filename}" "${sectorcount}" "$@" > /dev/null
}
make_part() {
	local path="${1}"
	local filename="${2}"
	local sectorcount="${3:-20480}"	# 10MiB
	local fslabel="${4:-${2}}"
	_make_blankimg "${path}" "${filename}" "${sectorcount}"
	sfdiskscript="${sfdiskscript}$( printf "\n${sfdiskstart}size=${sectorcount}, type=83" )"
	sfdiskstart=
	mkfs.ext3 -L "${fslabel}" "${path}/${filename}"
}
mkxt_part() {
	local filename="${1}"
	local sectorcount="${2:-20480}"	# 10MiB
	shift 2
	make_part "$( dirname "${vmdkpath}" )" "${filename}" "${sectorcount}" "$@" > /dev/null
}

make_vmdkextents() {
	local sfdiskstart=
	# last number is 512-byte sector count
	# recommended to keep at multiples of 2048 (1MiB) for alignment
	# casper-rw is likely going to run out of space at 50MiB
	# because of all the temp and log files that would be saved
	# causing subsequent boots to fail, such as with error dialogs
	# in the GUI, or systemd freezing execution
	mkxt_parttbl "${parttblname}" 2048
	mkxt_part "casper-rw" 102400	# 50MiB
	mkxt_part "home-rw" 307200	# 150MiB
}
chext_extractold() {
	local vmdkpath="${1:-${vmdkpath}}"
	sed --posix -n \
		-e '1,/^# Extent description/ ! {
			/^RW [1-9][0-9]* / s/^RW [1-9][0-9]* //p
			/^RDONLY [1-9][0-9]* / s/^RDONLY [1-9][0-9]* //p
			/^NOACCESS [1-9][0-9]* / s/^NOACCESS [1-9][0-9]* //p
		}' \
		"${vmdkpath}" | \
		sed --posix -n 's/^SPARSE "\(.*\)"$/\1/ p'
}
chext_replacenew() {
	local vmdkpath="${1:-${vmdkpath}}"
	local geomcylns="${2}"
	echo "Adjusting VMDK to use C${geomcylns} H${geomheads} S${geomsects}..."
	sed --posix --in-place=.bak \
		-e '1,/^createType=/ {
			# change create type; should not be creating but just in case
			/^createType=/ {
				c\createType="twoGbMaxExtentFlat"
				n
			}
		}' \
		-e '/^ddb\.geometry\.cylinders *=/ {
			s/\(ddb\.geometry\.cylinders\( *\)=\).*/\1\2"'"${geomcylns}"'"/
		}' \
		-e '/^ddb\.geometry\.heads *=/ {
			s/\(ddb\.geometry\.heads\( *\)=\).*/\1\2"'"${geomheads}"'"/
		}' \
		-e '/^ddb\.geometry\.sectors *=/ {
			s/\(ddb\.geometry\.sectors\( *\)=\).*/\1\2"'"${geomsects}"'"/
		}' \
		-e '1,/^# Extent description/ ! {
			# this inverted range has to be before the normal one below
			# knock out all extents
			/^# Extent description/ d
			/^RW [1-9][0-9]*/ d
			/^RDONLY [1-9][0-9]*/ d
			/^NOACCESS [1-9][0-9]*/ d
		}' \
		-e '1,/^# Extent description/ {
			# only tack on the extents the first time around
			/^# Extent description/ {
				r /dev/stdin
			}
		}' \
		"${vmdkpath}" <<- EXTENTS
			${vmdkextents}
		EXTENTS
}
find_nbdfree() {
	for nbd in /dev/nbd* ; do
		# http://www.microhowto.info/howto/connect_to_a_remote_block_device_using_nbd.html#idp27648
		if [ "0" -eq "$( blockdev --getsz "${nbd}" )" ] ; then
			echo "${nbd}"
			return 0
		fi
	done
	return 1
}
manual_sfdisk() {
	local scriptpath="${1:-${vmdkpath}.sfdisk}"
	local sfdiskscript="${2:-${sfdiskscript}}"
	echo "${sfdiskscript}" > "${scriptpath}"
	echo "Automatic partitioning failed.  Partitions listed in ${scriptpath}."
}
edit_parttbl() {
	local vmdkpath="${1:-${vmdkpath}}"
	local sfdiskscript="${2:-${sfdiskscript}}"
	local nbd=
	# can't directly edit partition table with sfdisk, even with --force,
	# because it makes sure the script is valid for what it can see;
	# the partitions are in other files, so it freaks out;
	# need to have the whole disk available...  make that happen with qemu-nbd
	# quite icky because that requires root :-/
	if sudo -l qemu-nbd > /dev/null && modprobe nbd && \
			nbd="$( find_nbdfree )" && \
			sudo qemu-nbd -c "${nbd}" "${vmdkpath}" ; then
		echo "Working with ${nbd}."
		echo "${sfdiskscript}" | sudo sfdisk --no-reread "${nbd}" || \
			manual_sfdisk "${vmdkpath}.sfdisk" "${sfdiskscript}"
		sudo qemu-nbd -d "${nbd}"
	else
		manual_sfdisk "${vmdkpath}.sfdisk" "${sfdiskscript}"
	fi
}
make_vmdk() {
	local vmdkpath="${1:-${vmdkpath}}"
	local sfdiskscript="label: dos"
	local sectorsexpr=
	local vmdkdir="$( dirname ${vmdkpath} )"
	[ -e "${vmdkdir}" ] || mkdir -p "${vmdkdir}"
	qemu-img create -f vmdk -o subformat=twoGbMaxExtentSparse "${vmdkpath}" 1M
	make_vmdkextents
	local oldextents="$( chext_extractold "${vmdkpath}" )"
	# calculate cylinder count (ceiling)
	chext_replacenew "${vmdkpath}" "$( expr \( ${sectorsexpr} + ${geomheads} \* ${geomsects} - 1 \) / ${geomheads} / ${geomsects} )"
	edit_parttbl "${vmdkpath}" "${sfdiskscript}"
	echo "OK."
	echo "The following files are not needed and may be manually removed:"
	echo "VMDK original backup:"
	echo "	${vmdkpath}.bak"
	echo "VMDK original extents:"
	echo "${oldextents}" | awk -v prefix="$( dirname "${vmdkpath}" )/" '{print "\t" prefix $1 }'
	echo "The files are the original VMDK generated by qemu-img."
	echo "Can be used as valid VMDK but will collide with generated $( basename "${vmdkpath}" )."
}

make_vmdk "${vmdkpath}"

