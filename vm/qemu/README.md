## QEMU scripts

### Startup Library

Scripts serving as a library of functions to assist in creating the command line to start QEMU with.

Written because QEMU (as of 2.5.0) is quite picky with empty arguments, and doing

```sh
qmu-system-x86_64 -kvm "${optionalArg}" "${optionalArg2}" -cd "${pathToISO}"
```

doesn't work, when `optionalArg[2]` is set to empty.

### Example

Sample scripts using startup library.

A script consists of 2 stages:

1) bootstrap
   - sources library script files
   - sets up common implementation overrides
1) options setup
   - sets up VM-specific options

### Utilities

Utilities to be used with QEMU.
