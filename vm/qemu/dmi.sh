#!/bin/sh
### WHAT THIS DOES #####################
# Create QEMU SMBIOS command-line options based on BIOS of host machine
# so that OS such as Windows 7 will activate via OEM activation,
# in order to facilitate transfer of license from physical to virtual machine.
# This does NOT include the -acpitable option
# which should come from /sys/firmware/acpi/tables/SLIC.
### HOWTO ##############################
# Run this script as superuser, copying the output into QEMU command-line.
### LICENSING ##########################
# Copycenter 2022 Serena's Copycat; licensed under CC0

set -e

set_restricted() {
	local restricted=
	{ [ "${sysfsname}!" != "${sysfsname%_serial}!" ] || \
		[ "${sysfsname}!" != "${sysfsname%_uuid}!" ] || \
		[ "${dmidecode}!" != "${dmidecode%-serial-number}!" ] || \
		[ "${dmidecode}!" != "${dmidecode%-uuid}!" ]
		} && restricted=true
	"$@"
}

foreach_mapping() {
	local callback="${1:-true}"
	shift
	local smbiostype=
	local sysfs= sysfsname= dmidecode=
	local qemusmbios= vboxextra=
	smbiostype=0 \
		sysfs=dmi sysfsname=bios_vendor dmidecode=bios-vendor \
		qemusmbios=vendor vboxextra=DmiBIOSVendor \
		set_restricted "${callback}" "$@"
	smbiostype=0 \
		sysfs=dmi sysfsname=bios_version dmidecode=bios-version \
		qemusmbios=version vboxextra=DmiBIOSVersion \
		set_restricted "${callback}" "$@"
	smbiostype=0 \
		sysfs=dmi sysfsname=bios_date dmidecode=bios-release-date \
		qemusmbios=date vboxextra=DmiBIOSReleaseDate \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=sys_vendor dmidecode=system-manufacturer \
		qemusmbios=manufacturer vboxextra=DmiSystemVendor \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_name dmidecode=system-product-name \
		qemusmbios=product vboxextra=DmiSystemProduct \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_version dmidecode=system-version \
		qemusmbios=version vboxextra=DmiSystemVersion \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_serial dmidecode=system-serial-number \
		qemusmbios=serial vboxextra=DmiSystemSerial \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_sku dmidecode= \
		qemusmbios=sku vboxextra=DmiSystemSKU \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_family dmidecode= \
		qemusmbios=family vboxextra=DmiSystemFamily \
		set_restricted "${callback}" "$@"
	smbiostype=1 \
		sysfs=dmi sysfsname=product_uuid dmidecode=system-uuid \
		qemusmbios=uuid vboxextra=DmiSystemUuid \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs=dmi sysfsname=board_vendor dmidecode=baseboard-manufacturer \
		qemusmbios=manufacturer vboxextra=DmiBoardVendor \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs=dmi sysfsname=board_name dmidecode=baseboard-product-name \
		qemusmbios=product vboxextra=DmiBoardProduct \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs=dmi sysfsname=board_version dmidecode=baseboard-version \
		qemusmbios=version vboxextra=DmiBoardVersion \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs=dmi sysfsname=board_serial dmidecode=baseboard-serial-number \
		qemusmbios=serial vboxextra=DmiBoardSerial \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs=dmi sysfsname=board_asset_tag dmidecode=baseboard-asset-tag \
		qemusmbios=asset vboxextra=DmiBoardAssetTag \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs= sysfsname= dmidecode= \
		qemusmbios=location vboxextra=DmiBoardLocInChass \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs= sysfsname= dmidecode= \
		qemusmbios= vboxextra=DmiBoardBoardType \
		set_restricted "${callback}" "$@"
	smbiostype=2 \
		sysfs= sysfsname= dmidecode= \
		qemusmbios=family vboxextra= \
		set_restricted "${callback}" "$@"
	smbiostype=3 \
		sysfs=dmi sysfsname=chassis_vendor dmidecode=chassis-manufacturer \
		qemusmbios=manufacturer vboxextra=DmiChassisVendor \
		set_restricted "${callback}" "$@"
	smbiostype=3 \
		sysfs=dmi sysfsname=chassis_type dmidecode=chassis-type \
		qemusmbios= vboxextra=DmiChassisType \
		set_restricted "${callback}" "$@"
	smbiostype=3 \
		sysfs=dmi sysfsname=chassis_version dmidecode=chassis-version \
		qemusmbios=version vboxextra=DmiChassisVersion \
		set_restricted "${callback}" "$@"
	smbiostype=3 \
		sysfs=dmi sysfsname=chassis_serial dmidecode=chassis-serial-number \
		qemusmbios=serial vboxextra=DmiChassisSerial \
		set_restricted "${callback}" "$@"
	smbiostype=3 \
		sysfs=dmi sysfsname=chassis_asset_tag dmidecode=chassis-asset-tag \
		qemusmbios=asset vboxextra=DmiChassisAssetTag \
		set_restricted "${callback}" "$@"
	smbiostype=4 \
		sysfs= sysfsname= dmidecode=processor-manufacturer \
		qemusmbios=manufacturer vboxextra=DmiProcManufacturer \
		set_restricted "${callback}" "$@"
	smbiostype=4 \
		sysfs= sysfsname= dmidecode=processor-family \
		qemusmbios= vboxextra= \
		set_restricted "${callback}" "$@"
	smbiostype=4 \
		sysfs= sysfsname= dmidecode=processor-version \
		qemusmbios=version vboxextra=DmiProcVersion \
		set_restricted "${callback}" "$@"
	smbiostype=4 \
		sysfs= sysfsname= dmidecode=processor-frequency \
		qemusmbios= vboxextra= \
		set_restricted "${callback}" "$@"
}

dmi_sysfs_retriever() {
	local sysfs_path="/sys/devices/virtual/dmi/id/${sysfsname}"
	[ -r "${sysfs_path}" ] && cat "${sysfs_path}"
}

if type dmidecode > /dev/null 2>&1 ; then
	dmi_dmidecode_retriever() {
		dmidecode --string "${dmidecode}"
	}
else
	dmi_dmidecode_retriever() {
		return 1
	}
fi

dmi_retriever() {
	{ [ "dmi!" = "${sysfs}!" ] && dmi_sysfs_retriever ; } || \
		{ [ -n "${dmidecode}" ] && dmi_dmidecode_retriever ; }
}

extract_qemu_smbios_opt() {
	local typefilter="${1}" value=
#	echo "${smbiostype} ${qemusmbios} $( dmi_retriever )"
	if [ "${typefilter}!" != "${smbiostype}!" ] || \
		[ -z "${qemusmbios}" ] ; then
		: 'OK: filtered out'
	elif [ -z "${sysfsname}" ] && [ -z "${dmidecode}" ] ; then
		: 'OK: nothing to get'
	elif ! value="$( dmi_retriever )" ; then
		[ -n "${restricted}" ] && return 2 || return 4
	elif [ -n "${value}" ] ; then
		printf ',%s=%s' "${qemusmbios}" "${value}"
	fi
}

extract_qemu_smbios_opts() {
	local typefilter="${1}"
	local extracted="$( foreach_mapping extract_qemu_smbios_opt "${typefilter}" )"
	[ -n "${extracted}" ] && echo "type=${typefilter}${extracted}"
}

extract_qemu_smbios() {
	local failed_normal= failed_restricted=
	for t in 0 1 2 3 ; do
		extract_qemu_smbios_opts "${t}"
	done
	[ -n "${failed_restricted}" ] && echo "Failed to retrieve restricted value(s)." >&2
	[ -n "${failed_normal}" ] && echo "Failed to retrieve value(s)." >&2
}

extract_qemu_smbios
