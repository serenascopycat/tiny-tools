## Windows 7

An example configuration for running Windows 7
using [SLP](https://en.wikipedia.org/wiki/System_Locked_Pre-installation) license from the host.
The transfer is in accordance with the Windows license terms found at `%windir%\System32\license.rtf`,
section 3d. "Use with Virtualization Technologies".

### HOWTO

1) Transfer license
   - extract SMBIOS values from host
      - [dmi.sh](../../dmi.sh) for Linux hosts
   - extract ACPI table from host
      - `/sys/firmware/acpi/tables/SLIC`
   - apply values to VM in `vm_id()`
      - these should come later in the QEMU command line when done manually
         - to avoid being clobbered by hardware options that modify specified values
1) Adjust RTC and timezone
   - Windows uses local timezone for RTC by default
   - set `TZ` variable when `-rtc base=localtime` (default in `vm_rtc()` in [Windows template](../../startlib/tmpl/windows.sh))
      - [C library timezone format](https://www.gnu.org/software/libc/manual/html_node/TZ-Variable.html)
   - may have 1h difference
      - guest may have a different idea regarding the RTC's DST state
1) Set up storage
   - using an image of original host OS or
   - reinstalling from OEM installation disc or
   - extract `.xrm-ms` from original and reapply to clean install
      - OEM system probably contains unnecessary drivers
      - `.xrm-ms` *may* exist on OEM installation disc or in the original OS
         - e.g. `%windir%\System32\OEM\OEM.xrm-ms`
         - e.g. `/sources/$OEM$/$$/system32/OEM/OEM.xrm-ms` on Dell installation CDs
         - different OEMs leave it at different places (or not at all)
      - can try to [extract from tokens.dat](https://forums.mydigitallife.net/threads/how-do-i-extract-my-oem-cert.144/#post-2179) if not found
