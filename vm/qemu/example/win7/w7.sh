#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Windows 7.
### NOTES ##############################
# Tested on Tiny Core Pure64 11.1 live CD with QEMU extension.
### HOWTO ##############################
# 1) Get cross-script files from live CD example
#  - ../livecd_basic/_bootstrap.sh
#  - ../livecd_basic/_common.sh
#  - customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust script-specific parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2023 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template=windows
	local tmpl_arch_bits="64"
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local scriptdir="$( dirname "$0" )"
	local datadir="$( mnt_uuid_mount "fa57cafe-feed-dead-beef-434decade2d8" "/vm/w7" )"
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'Windows 7'
	vm_id() {
		# Sets up BIOS to match physical machine for transferring SLP license
		# extract from physical machine with dmi.sh
		opt_misc -smbios 'type=0,vendor=Dell Inc.,version=A16,date=05/12/2017'
		opt_misc -smbios 'type=1,manufacturer=Dell Inc.,product=Precision M4500,version=0001,serial=EXAMPLE'
		opt_misc -smbios 'type=2,manufacturer=Dell Inc.,product=041WH1,version=A00,serial=/EXAMPLE/CN000000000000/'
		opt_misc -smbios 'type=3,manufacturer=Dell Inc.,version= ,serial=EXAMPLE'
		# extract from physical machine at /sys/firmware/acpi/tables/SLIC
		opt_misc -acpitable file="${datadir}/slic.bin"
	}
	vm_processor() {
		# allow use of all processors, but save one for other things
		# will break on single-core hosts
		opt_misc -smp "cores=$(( $( grep '^processor\s*:' /proc/cpuinfo | wc -l ) - 1 ))"
	}
	vm_hd_entries() {
		check_file() {
			local uuid="${1}"
			local path="${2}"
			local file="${3}"
			local extradir target
			extradir="$( mnt_uuid_mount "${uuid}" "${path}" )" && \
				target="${extradir}/${file}" && [ -e "${target}" ] && \
				echo "${target}" || return 1
		}
		"$@" "${datadir}/W7.run.qcow2"
		# drive to take heavy writes (e.g. temp, browser cache. page file)
		opt_markpersist "$@" "${datadir}/swap.img" "" "format=raw"
		# second drive to spread heavy writes to, but fall back if absent
		opt_markpersist "$@" "$( \
			check_file 'be5tfade-5afe-ea57-g80f-fabledca571e' '/vm/w7' 'swap2.img' || \
			check_file 'fa57cafe-feed-dead-beef-434decade2d8' '/vm/w7' 'swap2.img' )" "" "format=raw"
	}
	vm_net() {
		# RDP defaults to port 3389
		# forwarded guest IPs must be in same subnet as guest
		# https://bugs.launchpad.net/qemu/+bug/1628971
		# use nc (busybox); direct tcp flakes out
		opt_misc -netdev 'user,id=net1'"$( printf ',%sfwd=%s-%s' \
			'host' 'tcp::3389' ':3389' \
			'host' 'udp::3389' ':3389' \
			'guest' 'tcp:10.0.2.80:80' 'cmd:nc 127.0.186.80 22080' \
			'guest' 'tcp:10.0.2.80:443' 'cmd:nc 127.0.186.80 22443' )"
		opt_misc -device e1000,netdev=net1
	}
	# to reduce writes when initializing:
	#  start with low mem to avoid huge page file
	#  disable network to prevent update checks
	mem=6G \
		vm_setopt
	opt_misc "$@"
	make_vm_start_safer "${datadir}"
	# Set local timezone so RTC localtime makes sense
	export TZ='EST+5EDT,M3.2.0,M11.1.0'
#	export TZ='PST+8PDT,M3.2.0,M11.1.0'
	vm_start
} ; go "$@"
