# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Defines common functions and variables that can be overridden.
### HOWTO ##############################
# Provide implementations for items after the "STUBS TO OVERRIDE" line in _common.local.sh.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if type checkof_no_open_file_in > /dev/null 2>&1 ; then
	make_vm_start_safer() {
		if ! checkof_no_open_file_in "$@" ; then
			vm_start() {
				echo "QEMU VM args: $( opt_getvi )"
				echo "Startup disabled because open file found."
			}
		fi
	}
fi

if [ 'casper' = "${livecd_boot}" ] ; then
    initramfs_mkcasperplus() {
        local initrdname="${1:-'initrd-casper+'}"
        local casperplus="${2:-"${casperplus:-"$( dirname "$0" )/../../../tweak/linux/initramfs/casper+.cpio"}"}"
        local originitrd="${3:-"${loop}${initrd}"}"
        local tmpinitrd="${4:-/tmp/livecd-initrd}"
        local outinitrd="${initrdname}"
        [ "${initrdname#*/}!" != "${initrdname}!" ] || outinitrd="${tmpinitrd}/${initrdname}"
        initramfs_getappended "${outinitrd}" "${originitrd}" "${casperplus}"
    }
fi

## STUBS TO OVERRIDE ##

# to be used in fnmnt.sh mnt_findisobyuuid()
# to specify partition where live CD ISOs live, in the event that
# the ISO cannot be found in the same locations as currently running live CD
fs_uuid_livecd='fa57cafe-feed-dead-beef-434decade2d8'
