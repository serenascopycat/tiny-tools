#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Debian ISO.
### NOTES ##############################
# Tested on Knoppix 7.6 and Fedora 27 live CDs.
# Although set up to use live CD with Cinnamon desktop environment,
# since this is in a VM with no GPU, it will not perform as well.
# Recommend to change to MATE or XFCE.
### HOWTO ##############################
# 1) Customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust script-specific parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template='livecd'
	local tmpl_arch_bits='64'
	local livecd_boot='linux'
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'PCLinuxOS 2022'
	lo_getisomountpathfor() { lo_getisomountpath pclos 2022 ; }
	vm_video() {
		# mostly intended to be run in VirtualBox and VMWare
		# limited compatible drivers as of QEMU 5.2.0: only vmware works
		# everything else crashes Xorg with missing driver or something
		# virtio: drmSetMaster: device or resource busy
		# vmware works fine in QEMU 2.5.0 and 3.1.0, but unfortunately,
		# most are very slow on QEMU 5.2.0; only virtio is fast
		# (as seen during boot-up before Xorg comes in and crashes)
		opt_addconfval -vga vmware
	}
	live_linux() {
		local cd="${1:-${cd}}"
		local kernel="/isolinux/vmlinuz"
		local mnt="$( lo_tryloop "${cd}" "${kernel}" )"
		opt_looplinux "${mnt}" \
				"${kernel}" \
				"livecd=livecd keyb=us quiet splash=silent" \
				"/isolinux/initrd.gz"
	}
	cd="$( mnt_findiso '/iso/pclinuxos64-mate-2022.07.10.iso' )" \
		vm_setopt
	opt_misc "$@"
	vm_start
} ; go "$@"
