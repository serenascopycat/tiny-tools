#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Tiny Core Linux ISO.
### NOTES ##############################
# Tested on Knoppix 7.6 and Fedora 27 live CDs.
### HOWTO ##############################
# 1) Customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust script-specific parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template='livecd'
	local tmpl_arch_bits='32'
	local livecd_boot='linux'
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'Tiny Core Linux 12.0'
	lo_getisomountpathfor() {
		lo_getisomountpath tc 12.0
	}
	# skip plan 9 mount options: no support for mounting plan 9 FS in TC
	# manually transfer files via wget, ftpget, etc.
	vm_fs_entries() { : ; }
	live_linux() {
		local cd="${1:-${cd}}"
		local kernel="/boot/vmlinuz"
		local mnt="$( lo_tryloop "${cd}" "${kernel}" )"
		opt_looplinux "${mnt}" \
				"${kernel}" \
				"cde loglevel=8" \
				"/boot/core.gz"
	}
	mem="1G" \
		cd="$( mnt_findiso '/iso/TinyCore-12.0.iso' )" \
		vm_setopt
	opt_misc "$@"
	vm_start
} ; go "$@"
