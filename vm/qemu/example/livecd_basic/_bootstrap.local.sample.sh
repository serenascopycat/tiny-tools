# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Sample implmentation of bootstrap overrides (e.g. which scripts to include).
### HOWTO ##############################
# Rename to _bootstrap.local.sh and customize as desired.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# Try to locate specified path within a filesystem with specified UUID
incl_trypath() {
	# Mount filesystem with specified UUID; customizable default
	local uuid="${1:-"${bootstrap_startlib_uuid:-"FEED-D065"}"}"
	# Check for specified path within mounted filesystem; customizable default
	local path="${2:-"${bootstrap_startlib_path:-"/persist/qemu/startlib"}"}"
	(	# localize temporary working function _uuid_mount_do()
		if [ -d '/dev/disk/by-uuid' ] ; then
			_uuid_mount_do() {
				local uuid="${1}"
				( cd '/dev/disk/by-uuid/' && [ -e "${uuid}" ] && mount "$( readlink "${uuid}" )" )
			}
		else
			_uuid_mount_do() {
				blkid --match-token "UUID=${1}" --output device | while IFS= read d ; do
					mount "${d}" && break
				done
			}
		fi
		# Note: at this point the startup library has not yet been included
		# so only the bootstrap version of mnt_uuid_resolvepath is available
		mnt_uuid_resolvepath "${uuid}" "${path}" || \
			{ _uuid_mount_do "${uuid}" > /dev/null && \
				mnt_uuid_resolvepath "${uuid}" "${path}" ; } || \
			return
	)
}
incl_getlibdir() {
	local scriptdir="${1:-"$( dirname "$0" )"}"
	local uuid="${2}"
	local path="${3}"
	incl_trypath "${uuid}" "${path}" || (
		# fallback path to use; customizable default
		cd "${bootstrap_startlib:-"${scriptdir}/../../startlib"}" && pwd
	)
}
