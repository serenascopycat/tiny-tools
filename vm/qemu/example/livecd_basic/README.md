## Example startup scripts for live CDs in QEMU

A bunch of scripts that essentially does

```sh
qemu-system-x86_64 -cdrom /path/to/livecd.iso
```

### System Notes

Currently runs on Knoppix 7.6 DVD, Fedora 27, Tiny Core Linux 11 (both x86 and Pure 64) live CDs.
i.e. run Knoppix 7.6 or Fedora 27 or Tiny Core 11 on bare metal, then start a VM to run another live CD.

Can also run on Knoppix 9.1 DVD (QEMU 5.2.0), but laggy when using VGA guest device (virtio-vga works better).

#### Knoppix

Knoppix contains 32-bit QEMU executables, so even when using 64-bit kernel, QEMU cannot support using even 2GiB RAM.

#### Tiny Core Linux

`findmnt` calls have been wrapped around so it is not required.  `findmnt` is available in `util-linux.tcz` extension of Tiny Core Linux.
`tput` is used but will not be available by default.  The command line coloring will thus be unavailable and the shell escape format will be shown instead.

Versions 10 and 12 do not have a working QEMU with KVM, so while the scripts may work, they won't do much good there.

Version 11.0's Pure64 variant has an [issue with library dependencies not being found](http://forum.tinycorelinux.net/index.php/topic,23692.msg148552.html#msg148552), preventing Openbox from loading.  Use 11.1 Pure 64 instead, or use the x86 variant.
The issue might not affect QEMU itself or other GUIs, but not testing them all.

### Tips

The scripts call functions defined in other scripts in the startup library.
The prefix hints at which file it comes from:

- `incl_*` comes from `include.sh`
- `mnt_*` comes from `fnmnt.sh`
- `lo_*` comes from `fnloop.sh`
- `opt_*` comes from `fnopt.sh` and `fncmdln.sh`
- `vm_*` comes from `tmpl/basic.sh`
- `live_*` comes from `tmpl/livecd.sh`
