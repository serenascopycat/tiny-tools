#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Android x86 ISO.
### NOTES ##############################
# Tested on Knoppix 7.6 and Fedora 27 live CDs.
### HOWTO ##############################
# 1) Customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust script-specific parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template='livecd'
	local tmpl_arch_bits='64'
	local livecd_boot='linux'
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'Android x86 4.4 r2'
	lo_getisomountpathfor() { lo_getisomountpath android 4.4r2 ; }
	vm_fs_entries() { : ; }
	live_linux() {
		local cd="${1:-${cd}}"
		local kernel="/kernel"
		local mnt="$( lo_tryloop "${cd}" "${kernel}" )"
		opt_looplinux "${mnt}" "${kernel}" \
			"root=/dev/ram0 androidboot.hardware=android_x86 quiet SRC= DATA=" \
			"/initrd.img"
	}
	mem="1.5G" \
		cd="$( mnt_findiso '/iso/android-x86-4.4-r2.iso' )" \
		vm_setopt
	opt_misc "$@"
	vm_start
} ; go "$@"
