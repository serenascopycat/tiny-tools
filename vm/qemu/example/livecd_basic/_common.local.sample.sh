# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Sample implmentation of common VM option setup overrides.
### HOWTO ##############################
# Rename to _common.local.sh and customize as desired.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

fs_uuid_livecd='fa57cafe-feed-dead-beef-434decade2d8'

vm_fs_entries() {
	local fs_uuid_data='fa57cafe-feed-dead-beef-434decade2d8'
	local mnt=
	if mnt="$( mnt_uuid_mount "${fs_uuid_data}" )" ; then
		"$@" "${mnt}/data/" data fs1 readonly
	else
		echo "Failed to share mounts."
	fi
}

vm_video() {
	local xres=1200 yres=720
	if qemu_isAtLeast 5.2 ; then
		# use virtio-vga instead of vga on QEMU 5.2.0 (in Knoppix 9.1); vga is very laggy
		# https://superuser.com/questions/1435557/how-to-limit-screen-resolution-on-qemu#1448428
		# https://gitlab.com/qemu-project/qemu/-/issues/696
		# can't get specified resolution when using virtio-vga with GTK
		# GTK window just starts small, and guest will see the resolution based on window size
		# borks when specifying x/yres on QEMU 2.5.0 (in Knoppix 7.6)
		# can specify x/yres on QEMU 2.10.0 (in Fedora 27)
		# edid=on freaks out Kali 2021.1 and it goes boot loop
		opt_addconfval -vga none
		opt_addconfval -device "virtio-vga,xres=${xres},yres=${yres}"
	elif qemu_isNewerThan 2.5.0 ; then
		# use VGA for QEMU 3.1.0; prefer over virtio-vga for systems that don't have drivers
		# https://wiki.archlinux.org/title/QEMU#Custom_display_resolution
		opt_addconfval -vga none
		# may need vgamem_mb=32 for larger resolutions under vga
		opt_addconfval -device "VGA,edid=on,xres=${xres},yres=${yres}"
	else
		opt_addconfval -vga none
		# edid and x/yres not available, so can't control resolution
		# set vgamem_mb so it still shows if default goes too high
		opt_addconfval -device "VGA,vgamem_mb=32"
	fi
}
