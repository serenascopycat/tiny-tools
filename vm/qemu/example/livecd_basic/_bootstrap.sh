# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Defines functions to access QEMU startlib.
### HOWTO ##############################
# Provide implementations for items after the "STUBS TO OVERRIDE" line in _bootstrap.local.sh.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# Bootstrap implementation to be replaced by version in fnmnt.sh
if type findmnt >/dev/null ; then
	mnt_uuid_resolvepath() {
		local mnt
		# local always returns true; separate assignment so return value comes through
		mnt="$( findmnt --list --noheadings --output TARGET --source "UUID=${1}" )" && \
			[ -n "${mnt}" ] && [ -e "${mnt}${2}" ] && ( cd "${mnt}${2}" && pwd )
	}
else
	mnt_uuid_resolvepath() {
		blkid -t "UUID=${1}" -o device | awk -v "path=${2}" 'BEGIN {
				exit_code = 1;
				quote = "\047";
			}; 1 == FNR {
				in_blk = 0;
				in_mnt = 0;
				if ("-" == FILENAME) {
					in_blk = 1;
				} else if ("/etc/mtab" == FILENAME) {
					in_mnt = 1;
					if (1 != hasblkdevs) { exit_code = 1 ; exit; }
				}
			}; 1 == in_blk {
				blkdevs[$1] = 1;
				hasblkdevs = 1;
			}; 1 == in_mnt && 1 == blkdevs[$1] {
				cmd = "( cd " quote $2 path quote " && pwd )"
				cmd | getline resolvedpath
				if ("" != resolvedpath) {
					print resolvedpath;
					exit_code = 0;
					exit;
				}
				blkdevs[$1] = 2;
			}; END {
				exit exit_code;
			};' - /etc/mtab
	}
fi

btstrp_include() {
	local libdir="${1:-$( incl_getlibdir )}"
	. "${libdir}/include.sh"
	if [ 0 -lt "$#" ] ; then
		shift
		incl_do "${libdir}" "$@"
	fi
}
btstrp_addlib() {
	"$@" "fnchatty.sh" "fnshl.sh" "fntcolor.sh" "fnmnt.sh" "fnloop.sh" \
		"fncmdln.sh" "fncmdln.local.sh" "qyver.sh" "fnopt.sh"
}

# source specified scripts:
# - parent directory of bootstrap script
# - number of scripts in list to follow
# - list of scripts
# - parameters to pass to scripts
# the front portion before parameters defines which scripts to source
# e.g. /path/to 3 script1.sh script2.sh script3.sh ARG1 ARG2
# would lead to the equivalent of:
#  . /path/to/script1.sh ARG1 ARG2
#  . /path/to/script2.sh ARG1 ARG2
#  . /path/to/script3.sh ARG1 ARG2
btstrp_source() {
	local dir="${1:-"$( dirname "$0" )"}"
	local script_count="${2}"
	shift 2
	if [ 0 -lt "${script_count}" ] ; then
		btstrp_source "${dir}" "-${script_count}" "$@" || : 'allow missing optional script'
		shift
		btstrp_source "${dir}" "$(( ${script_count} - 1 ))" "$@"
	elif [ 0 -gt "${script_count}" ] ; then
		local script="${dir}/${1}"
		shift "$(( - ${script_count} ))"
		[ -r "${script}" ] && . "${script}"
	fi
}

## STUBS TO OVERRIDE ##

# Outputs location of QEMU startup script library
incl_getlibdir() {
	cd "${bootstrap_startlib:-"${scriptdir:-"$( dirname "$0" )"}/../../startlib"}" && pwd
}

btstrp_includelib() {
	case "${bootstrap_template}" in
	base)
		btstrp_addlibtmpl() {
			"$@"
		} ;;
	custom) ;;	# btstrp_addlibtmpl defined elsewhere (e.g. _bootstrap.local.sh)
	headless)
		btstrp_addlibtmpl() {
			"$@" "checkof.sh" "qyexec.sh" "tmpl/basic.sh"
		} ;;
	livecd)
		btstrp_addlibtmpl() {
			"$@" "checkx.sh" "checkof.sh" "qyexec.sh" "tmpl/basic.sh" "tmpl/livecd.sh" "utiramfs.sh"
		} ;;
	windows)
		btstrp_addlibtmpl() {
			"$@" "checkx.sh" "checkof.sh" "qyexec.sh" "tmpl/basic.sh" "tmpl/windows.sh"
		} ;;
	*)
		btstrp_addlibtmpl() {
			"$@" "checkx.sh" "checkof.sh"
		} ;;
	esac
	btstrp_addlib btstrp_addlibtmpl btstrp_include ""
}
btstrp_includecommon() {
	local scriptdir="${1}"
	shift
	btstrp_source "${scriptdir:-"$( dirname "$0" )"}" \
		2 "_common.sh" "_common.local.sh" \
		"$@"
}
# will be given 
# - parent directory of bootstrap script
# - parameters to pass to scripts
btstrp_do() {
	btstrp_includelib "$@"
	btstrp_includecommon "$@"
}

if [ 2 -le "$#" ] ; then
	btstrp_doparams() {
		scriptdir="${1}"
		shift
		shift "${1}"
		shift
		btstrp_do "${scriptdir}" "$@"
	}
	# source pre-inclusion customization file(s)
	# for overriding main sourcing functions such as btstrp_do()
	# so actual inclusion can be customized
	# that'll typically be _bootstrap.local.sh
	btstrp_source "$@"
	# source files in btstrp_do(), which should include
	# the core startlib and, by default, _common{,.local}.sh
	btstrp_doparams "$@"
fi
