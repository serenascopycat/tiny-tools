#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Kali Linux ISO.
### NOTES ##############################
# Tested on Knoppix 7.6 and Fedora 27 live CDs.
### HOWTO ##############################
# 1) Customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust script-specific parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template='livecd'
	local tmpl_arch_bits='64'
	local livecd_boot='live-boot'
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'Kali 2021.1 Live'
	lo_getisomountpathfor() { lo_getisomountpath kali 2021.1 ; }
	cd="$( mnt_findiso '/iso/kali-linux-2021.1-live-amd64.iso' )" \
		vm_setopt
	opt_misc "$@"
	vm_start
} ; go "$@"
