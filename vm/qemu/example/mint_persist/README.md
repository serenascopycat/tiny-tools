## Linux Mint with Persistence

An example live CD configuration that have the persistence shared between running on bare metal and in a VM.

### HOWTO

1) create persistence storage
   - use [`mkvmdk.sh`](../../util/mkvmdk.sh) to create VMDK file
      - extents should include at least one of `casper-rw` and `home-rw`
         - the extent files will serve as the persistence file in a bare-metal run
         - the extent files must live on FAT filesystem for `casper` to see it
      - VMDK should be named `hda.vmdk`
         - need to adjust `mint-mate-casper+.sh` otherwise; see `vm_hd()`
1) generate casper+.cpio
   - run [`mkinitmod.sh`](../../../../tweak/linux/initramfs/mkinitmod.sh)
   - this script is set up to use `casper+` and will fail to boot without it
      - to get version without `casper+`, apply persistence-related changes over the [non-persistent version](../livecd_basic/mint_mate-casper+.sh) to the [version without `casper+`](../livecd_basic/mint_mate.sh)
1) copy or link from [`livecd_basic`](../livecd_basic)
   - [`_bootstrap.sh`](../livecd_basic/_bootstrap.sh)
   - [`_bootstrap.local.sh`](../livecd_basic/_bootstrap.local.sh)
   - [`_common.sh`](../livecd_basic/_common.sh)
   - [`_common.local.sh`](../livecd_basic/_common.local.sh)
1) run live CD
   - run `mint_mate-casper+.sh` to start QEMU
      - it is modified from the [non-persistent version](../livecd_basic/mint_mate-casper+.sh)
   - boot with a bootloader to run on bare metal, with these boot options
      - `persistent`
      - `persistent-path`
         - argument needs to be the path where `casper-rw` / `home-rw` is

Example entry when using [`grub-config`](https://gitlab.com/serenascopycat/grub-config/):

```
make_liveentry '[AMD64] Linux Mint 18.3 [MATE Edition] +Persist' \
    "${livecd_iso_root}"'/linuxmint-18.3-mate-64bit.iso' losetup \
    set gfxpayload=keep \
    linuxlive_casperplus /casper/vmlinuz persistent-path=/persist/Mint_MATE/ \
    initrd_casperplus /casper/initrd.lz
```

Or a more-direct entry, without `casper+`:

```
menuentry '[AMD64] Linux Mint 18.3 [MATE Edition] +Persist' --unrestricted {
    set path_iso='/iso/linuxmint-18.3-mate-64bit.iso'
    losetup loopiso "${path_iso}"
    set gfxpayload=keep
    linux (loopiso)/casper/vmlinuz iso-scan/filename="${path_iso}" boot=casper preseed/file=/cdrom/preseed/linuxmint.seed quiet splash persistent persistent-path=/persist/Mint_MATE/ noprompt --
    initrd (loopiso)/casper/initrd.lz
}
```
