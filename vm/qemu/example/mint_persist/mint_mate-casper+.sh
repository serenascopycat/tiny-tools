#!/bin/sh
### WHAT THIS DOES #####################
# Starts QEMU with Linux Mint MATE edition ISO.
### NOTES ##############################
# Tested on Knoppix 7.6 and Fedora 27 live CDs.
### HOWTO ##############################
# 1) Get cross-script files from live CD example
#  - ../livecd_basic/_bootstrap.sh
#  - ../livecd_basic/_common.sh
#  - customize across scripts via _bootstrap.local.sh and/or _common.local.sh
# 2) Adjust parameters:
#  - name to display in QEMU window (sent to opt_name)
#  - memory (using mem variable in vm_setopt call)
#  - ISO path (using cd variable in vm_setopt call)
#  - others
#    - override functions in startup template(s)
#      - startlib/tmpl/basic.sh and startlib/tmpl/livecd.sh
# 3) Run script
#  - when detected as interactive, will prompt to confirm QEMU arguments
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

bootstrap() {
	local bootstrap_template='livecd'
	local tmpl_arch_bits='64'
	local livecd_boot='casper'
	local scriptdir="$( dirname "$0" )"
	set -- "${scriptdir}" 1 "_bootstrap.local.sh" \
		"$@"	# dash does not support overriding arguments to source with.
	. "${scriptdir}/_bootstrap.sh"
} ; bootstrap "$@"

go() {
	local opt_cmdline= opt_cmdline_visual=

	opt_name 'Linux Mint 18.3 MATE Live +Persist'
	lo_getisomountpathfor() { lo_getisomountpath mint 18.3 mate ; }
	vm_hd_entries() {
		# opt_markpersist is not really needed here
		# since boot options are specified directly below
		# but just to be consistent when using generated boot options
		opt_markpersist "$@" "$( cd "$( dirname "$0" )" ; pwd )/hda.vmdk"
	}
	live_preplinux() {
		initramfs_mkcasperplus 'mint-mate-casper+' "$( dirname "$0" )/../../../../tweak/linux/initramfs/casper+.cpio"
	}
	cd="$( mnt_findiso '/iso/linuxmint-18.3-mate-64bit.iso' )" \
		kernelarg="file=/cdrom/preseed/linuxmint.seed boot=casper+ quiet splash noprompt persistent --" \
		vm_setopt
	opt_misc "$@"
	make_vm_start_safer
	vm_start
} ; go "$@"
