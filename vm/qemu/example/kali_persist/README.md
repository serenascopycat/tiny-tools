## Kali Linux with Persistence

An example live CD configuration that have the persistence shared between running on bare metal and in a VM.

### HOWTO

1) create persistence storage
   - use [`mkvmdk.sh`](../../util/mkvmdk.sh) to create VMDK file
      - extents should include `live-rw` and `home-rw`
         - the extent files will serve as the persistence file in a bare-metal run
         - need to adjust `kali.sh` when using other names/labels
            - should match `kernelargadd="persistence-label=live-rw,home-rw"`
      - VMDK should be named `hda.vmdk`
         - need to adjust `kali.sh` otherwise; see `vm_hd()`
1) remove `-snapshot` option (optional)
   - script includes `-snapshot` option to prevent changes to persistence files
   - typically undesirable, but included to demonstrate fixed startup state
1) copy or link from [`livecd_basic`](../livecd_basic)
   - [`_bootstrap.sh`](../livecd_basic/_bootstrap.sh)
   - [`_bootstrap.local.sh`](../livecd_basic/_bootstrap.local.sh)
   - [`_common.sh`](../livecd_basic/_common.sh)
   - [`_common.local.sh`](../livecd_basic/_common.local.sh)
1) run live CD
   - run `kali.sh` to start QEMU
      - it is modified from the [non-persistent version](../livecd_basic/kali.sh)
   - boot with a bootloader to run on bare metal, with these boot options
      - `persistence`
      - `persistence-path`
         - argument needs to be the path where persistence files are
      - `persistence-label`
         - if persistence file(s) isn't just `persistence`

Example entry when using [`grub-config`](https://gitlab.com/serenascopycat/grub-config/):

```
make_liveentry '[AMD64] Kali Linux 21.1 +Persist' \
    "${livecd_iso_root}"'/kali-linux-2021.1-live-amd64.iso' losetup_uuid \
    set gfxpayload=keep \
    linuxlive_live /live/vmlinuz "persistence-path=/persist/kali/ persistence-label=live-rw,home-rw -- components noeject" \
    initrd /live/initrd.img
```

Or a more-direct entry using `findiso`:

```
menuentry '[AMD64] Kali Linux 21.1 +Persist' --unrestricted {
    set path_iso='/iso/kali-linux-2021.1-live-amd64.iso'
    losetup loopiso "${path_iso}"
    set gfxpayload=keep
    linux (loopiso)/live/vmlinuz findiso="${path_iso}" boot=live quiet splash persistence persistence-path=/persist/kali/ persistence-label=live-rw,home-rw noeject components noeject
    initrd (loopiso)/live/initrd.img
}
```
