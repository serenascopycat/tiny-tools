# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Functions to determine QEMU version.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

ver_compare() {
	local compare_op="${1:-'lt'}"
	local separator="${2:-'.'}"
	local leftVer="${3}"
	local rightVer="${4}"
	local leftHead rightHead
	while [ -n "${leftVer}" ] && [ -n "${rightVer}" ] ; do
		leftHead="${leftVer%%${separator}*}"
		[ "${leftHead}!" = "${leftVer}!" ] && leftVer='' || \
			leftVer="${leftVer#${leftHead}${separator}}"
		rightHead="${rightVer%%${separator}*}"
		[ "${rightHead}!" = "${rightVer}!" ] && rightVer='' || \
			rightVer="${rightVer#${leftHead}${separator}}"
		[ "${leftHead}" -eq "${rightHead}" ] && continue
		[ "${leftHead}" "-${compare_op}" "${rightHead}" ] && return || return
	done
	case "${compare_op}" in
	eq) [ -z "${leftVer}" ] && [ -z "${rightVer}" ] || return ;;
	lt) [ -n "${rightVer}" ] || return ;;
	le) [ -z "${leftVer}" ] || return ;;
	ge) [ -z "${rightVer}" ] || return ;;
	gt) [ -n "${leftVer}" ] || return ;;
	*) false ; return 255 ;;
	esac
}
if false ; then
	compare_case() {
		local lver="${1}" rel="${2}" rver="${3}" op="${4}" result=0 expect=0
		[ "${op#*${rel}*}" != "${op}" ] || expect="${?}"
		[ 0 -eq "${expect}" ] && expect= || expect="not"
		ver_compare "${op}" . "${lver}" "${rver}" || result="$?"
		[ 0 -eq "${result}" ] && result= || result="not"
		[ "${expect}!" = "${result}!" ] || echo "${lver} should ${expect} be -${op} ${rver}"
	}
	compare_test() {
		for op in lt le ge gt eq ; do
			compare_case "$@" "${op}"
		done
	}
	compare_test 1.2.3 e 1.2.3
	compare_test 1.2.3 l 1.2.3.1
	compare_test 1.2.3.1 g 1.2.3
	compare_test 1.2.2 l 1.2.3
	compare_test 1.2.3 l 1.2.23
	compare_test 1.2.23 g 1.2.3
fi
qemu_version() {
	qemu-system-i386 -version | sed -n 's/QEMU.*version\s*\([0-9]\+\(\.[0-9]\+\)\+\).*/\1/p'
}
qemu_isAtLeast() {
	local ver="${qemu_version:-$( qemu_version )}"
	local target="${1}"
	ver_compare ge '.' "${ver}" "${target}" || return
}
qemu_isNewerThan() {
	local ver="${qemu_version:-$( qemu_version )}"
	local target="${1}"
	ver_compare gt '.' "${ver}" "${target}" || return
}

