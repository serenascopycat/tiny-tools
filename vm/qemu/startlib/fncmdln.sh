# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Functions to store the WIP command line.
### NOTES ##############################
# Works with 'opt_cmdline' variable
# and optionally 'opt_cmdline_visual' variable.
### DEPENDENCIES #######################
# fnshl.sh
# fntcolor.sh (optional)
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if [ -t 1 ] && type termcol_color >/dev/null 2>&1 && opt_fmt_opt="$( termcol_color "${termcol_ansi_mgn}" "${termcol_ansi_blu}" )" && opt_fmt_arg="$( termcol_color "${termcol_ansi_ylw}" "${termcol_ansi_blu}" )" && opt_fmt_reset="$( termcol_color )" ; then
	# Enable parameter coloring to tell where each parameter starts and ends
	opt_fmt_clrend="$( termcol_cleartoeol )"
	opt__addvi() {
		local opt_fmt_start="$1"
		local opt_fmt_end="$2"
		shift 2
		[ -n "${opt_cmdline_visual}" ] && opt_cmdline_visual="${opt_cmdline_visual} "
		opt_cmdline_visual="${opt_cmdline_visual}${opt_fmt_start}${1}${opt_fmt_end}"
		while [ 1 -lt "$#" ] ; do
			shift
			opt_cmdline_visual="${opt_cmdline_visual} ${opt_fmt_start}${1}${opt_fmt_end}"
		done
	}
	opt_addviconf() {
		opt__addvi "${opt_fmt_opt}" "${opt_fmt_reset}" "$@"
	}
	opt_addvival() {
		opt__addvi "${opt_fmt_arg}" "${opt_fmt_reset}" "$@"
	}
	opt_getvi() {
		echo "${opt_cmdline_visual}${opt_fmt_clrend}"
	}
else
	# Can't use colors; print out the escaped form
	opt_fmt_opt= ; opt_fmt_arg= ; opt_fmt_reset=
	opt_addviconf() {
		: "no support; just use escaped commandline"
	}
	opt_addvival() {
		: "no support; just use escaped commandline"
	}
	opt_getvi() {
		echo "${opt_cmdline}"
	}
fi
# Adds configuration values
# Values are escaped.
opt_addval() {
	local - >/dev/null 2>&1 && set +x	# disable debug temporarily on ash-like shells
	opt_addvival "$@"
	while [ 0 -lt "$#" ] ; do
		opt_cmdline="${opt_cmdline} $( shl_escape "$1" )"
		shift
	done
}
# Adds a configuration with its value
# First parameter is assumed to be a parameter/switch name
# that does not need to be escaped.
# The rest will be escaped.
opt_addconfval() {
	local - >/dev/null 2>&1 && set +x	# disable debug temporarily on ash-like shells
	opt_addviconf "$1"
	[ -n "${opt_cmdline}" ] && opt_cmdline="${opt_cmdline} "
	opt_cmdline="${opt_cmdline}${1}"
	shift
	[ 1 -le "$#" ] && opt_addval "$@" || : "optional: don't error out"
}

opt_xargs() {
	shl_out "${opt_cmdline}" | xargs "$@"
}
opt_xargs_verbose() {
	echo "QEMU VM args: $( opt_getvi )"
	opt_xargs "$@"
}
opt_xargs_maybeprompt() {
	if [ -t 0 ] ; then
		# output arguments so they can be checked and confirmed
		echo "QEMU VM args: $( opt_getvi )"
		# -p is --interactive, but --interactive not supported by BusyBox
		opt_xargs -p "$@"
	else
		# no input; no way to confirm, so just run it silently
		opt_xargs "$@"
	fi
}
