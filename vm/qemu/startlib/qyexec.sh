# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Predicate functions related to execution environment.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# check whether QEMU binary is 32-bit (can run 32-bit binary on 64-bit system)
# memory is limited when running 32-bit binaries
# https://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/
# https://superuser.com/questions/791506/how-to-determine-if-a-linux-binary-file-is-32-bit-or-64-bit#1268052
if shl_isbusyboxcmd cmp ; then
	elf_is32bit() {
		local testfile="${1}"
		# unfortunately, BusyBox cmp doesn't have -n to limit to first bytes
		# make sure byte count is 1 in hexdump format so we don't see little-endian effect
		[ "7F454C4601!" = "$( hexdump -n 5 -e '1/1 "%02X"' "${testfile}" )!" ]
	}
else
	elf_is32bit() {
		local testfile="${1}"
		printf '\177ELF\001' | cmp -n 5 -s "${testfile}"
	}
fi
qemu_is32bit() {
	local qemu="${1}"
	elf_is32bit "$( which "${qemu}" )"
}
sys_is32bit() {
	# https://superuser.com/questions/791506/how-to-determine-if-a-linux-binary-file-is-32-bit-or-64-bit#791508
	uname -m | grep -q '^i[3-6]86'
}
