# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides functions related to shell, such as shell escaping.
# May enable bashisms when run on bash.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

shl__checkbash() {
	'/proc/self/exe' --version | grep -q -i bash
}

[ '/proc/self/exe' -ef '/bin/dash' ] && shl_usedash=1
[ '/proc/self/exe' -ef '/bin/bash' ] && shl__checkbash && shl_enablebash=1

if ! [ -e '/bin/busybox' ] ; then
	shl_isbusyboxcmd() {
		return 1
	}
elif ! [ '/bin/busybox' -ef '/proc/self/exe' ] ; then
	shl_isbusyboxcmd() {
		[ '/bin/busybox' -ef "$( which "${1}" )" ] || return
	}
else
	# while in BusyBox ash, commands could be considered built-in
	# even if there's an external one in PATH that's not BusyBox
	shl_isbusyboxcmd() {
		local cmd="${1}" resolvecmd=""
		# first, if given a simple name (not path), try to resolve and match
		# otherwise, try to resolve full path and match against busybox
		{ [ "${cmd##*/}!" = "${cmd}!" ] && \
			resolvecmd="$( type "${cmd}" )" && \
			resolvecmd="${resolvecmd#"${cmd} is "}" && \
			{ [ "${resolvecmd}!" = "a shell builtin!" ] || \
				[ "${resolvecmd}!" = "${cmd}!" ] ; } ; } || \
			{ cmd="$( which "${cmd}" )" && \
				[ "${resolvecmd:-"${cmd}"}!" = "${cmd}!" ] && \
				[ '/bin/busybox' -ef "${cmd}" ] ; } || \
			return
	}
fi

shl_out() {
	printf '%s' "${1}"
	[ 1 -ge "$#" ] || { shift ; printf ' %s' "$@" ; }
}

if [ -n "${shl_enablebash}" ] ; then
	shl__escapesquote() {
		# quote with single quote, removing extraneous '' at beginning and end
		# single quotes need to be placed outside the quoted portion
		# e.g. example's silly => 'example'\''s silly'
		local arg="'${1//\'/\'\\\'\'}'"
		# simplify beginning and end by removing empty quote portions
		[ "${arg#"''"}!" != "${arg}!" ] && arg="${arg:2}"
		[ "${arg%"''"}!" != "${arg}!" ] && arg="${arg:0:-2}"
		shl_out "${arg}"
	}
else	# assume shl_usedash, e.g. BusyBox ash
	shl__escapesquote() {
		# quote with single quote, removing extraneous '' at beginning and end
		# single quotes need to be placed outside the quoted portion
		# e.g. example's silly => 'example'\''s silly'
		local arg="${1}"
		local qstart=\'
		local qend=\'
		# first handle the beginning and end so we don't get
		# 'example' => ''\''example'\''', but rather just \''example'\'
		local argtemp
		argtemp="${arg#\'}"
		if [ "${argtemp}!" != "${arg}!" ] ; then
			qstart="\\''"
			arg="${argtemp}"
		fi
		argtemp="${arg%\'}"
		if [ "${argtemp}!" != "${arg}!" ] ; then
			qend="'\\'"
			arg="${argtemp}"
		fi
		arg="${qstart}$( shl_out "${arg}" | sed "s/'/'\\\\''/g" )${qend}"
		shl_out "${arg}"
	}
fi
shl__escapebslash() {
	# quote with backslash only
	shl_out "$1" | sed 's/\([\\\"\'\'']\|\s\)/\\\1/g'
}
# echo the shorter of the first 2 parameters
shl__echoshort() {
	if [ "${#1}" -le "${#2}" ] ; then
		shl_out "$1"
	else
		shl_out "$2"
	fi
}
# escape parameters for interpretation by shell-like processing,
# basically quoting the input by escaping single and double quotes with backslash
# note that this is designed for use with xargs and is *NOT* full shell escape,
# as command substitutions done via backquote and variable subtitutions
# will not be escaped and thus be interpreted by shell
shl_escape() {
	local result
	while [ 0 -lt "$#" ] ; do
		# tack on shorter (presumably more readable) escape
		[ -n "${result}" ] && result="${result} "
		result="${result}$( shl__echoshort "$( shl__escapesquote "$1" )" "$( shl__escapebslash "$1" )" )"
		shift
	done
	shl_out "${result}"
}
