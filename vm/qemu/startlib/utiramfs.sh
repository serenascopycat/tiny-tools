# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Utility functions related to QEMU direct booting options.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# To be used within optlive_preplinux to build initramfs that includes additional cpio archives.
# Needs loop and initrd variables; modifies initrd to the path of built initramfs file.
initramfs_ck() {
	local originitrd="${1:-"${loop}${initrd}"}"
	local initramfs="${2}"	# new initramfs with additions appended
	if shl_isbusyboxcmd cmp ; then
		chatty_info "Skipped checking ${casperplus}: byte limit not supported in busybox cmp."
		[ -r "${initramfs}" ]
	else
		# match the original initrd portion; assume data after original initrd is appended addition
		cmp --bytes="$( stat --format='%s' "${originitrd}" )" "${originitrd}" "${initramfs}"
	fi
}
initramfs_append() {
	local outinitrd="${1:-/tmp/livecd-initrd}"
	local originitrd="${2:-"${loop}${initrd}"}"
	local a
	shift 2
	for a in "$@" ; do
		[ -r "${a}" ] || {
			chatty_err "Error: cannot read ${a}.  Cannot append to initramfs."
			return 2
		}
	done
	local outdir="$( dirname "${outinitrd}" )"
	[ -d "${outdir}" ] || mkdir -p "${outdir}"
	chatty_info "Building new initramfs from ${originitrd} with additions..."
	local tmpinitrd="$( mktemp ${outinitrd}_XXXXXX )"
	# copy initrd file with 512-byte alignment padding, then tack on addition
	dd if="${originitrd}" of="${tmpinitrd}" bs=512 conv=sync || {
			local result="$?"
			rm -f "${tmpinitrd}"
			return "${result}"
		}
	local ddconvopt
	if shl_isbusyboxcmd dd ; then
		ddconvopt=conv=notrunc,sync
	else
		ddconvopt=conv=notrunc,nocreat,sync
	fi
	for a in "$@" ; do
		dd if="${a}" of="${tmpinitrd}" bs=512 "${ddconvopt}" oflag=append || {
				local result="$?"
				rm -f "${tmpinitrd}"
				return "${result}"
			}
	done
	mv -n "${tmpinitrd}" "${outinitrd}"
	# should not exist when renamed successfully
	if [ -e "${tmpinitrd}" ] ; then
		# failed to rename to target file
		rm -f "${tmpinitrd}"
		return 3
	else
		chatty_info "Built new initramfs at ${outinitrd}."
	fi
}
initramfs_getappended() {
	local outinitrd="${1:?Full path required for output initrd.}"
	local originitrd="${2:-"${loop}${initrd}"}"
	if [ 2 -ge "${#}" ] ; then	# nothing to append
		initrd="${originitrd}"
	elif ! [ -r "${originitrd}" ] ; then
		chatty_err "Error: cannot read ${originitrd}.  Cannot build initramfs to append to."
		return 2
	elif initramfs_ck "${originitrd}" "${outinitrd}" ; then
		initrd="${outinitrd}"
		chatty_info "Reusing initramfs at ${initrd}."
	elif [ -e "${outinitrd}" ] ; then
		chatty_err "${outinitrd} does not start with ${originitrd}."
		return 3
	elif initramfs_append "$@" ; then
		initrd="${outinitrd}"
	else
		return 4
	fi
}
