# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides functions to output messages to terminal,
# even within command substitutions.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

chatty_info() {
	echo "$@" > /dev/tty
}

chatty_warn() {
	echo "$@" > /dev/tty
}

chatty_err() {
	echo "$@" >&2
}

chatty_fail() {
	local rcode="${1:-1}"
	shift
	chatty_err "$@"
	return "${rcode}"
}

