# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Functions to dynamically build QEMU command line.
### NOTES ##############################
# Works with 'opt_cmdline' variable
# and optionally 'opt_cmdline_visual' variable.
### DEPENDENCIES #######################
# QEMU 2.5.0 (in Knoppix 7.6)
# fncmdln.sh
# qyver.sh
### HOWTO ##############################
# 1) Source this file
# 2) Call opt_* functions to set up commandline options
# 3) Call either opt_xargs or opt_xargs_verbose to append built commandline arguments
#  e.g.) opt_xargs a b would trigger xargs a b <built_commandline_options>
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

opt_setup() {
	local qemu_version="$( qemu_version )"
	opt_name() {
		opt_addconfval -name "${1}"
	}
	opt_kvm() {
		opt_addconfval -enable-kvm
	}
	opt_memory() {
		opt_addconfval -m "${1}"
	}
	if qemu_isAtLeast 5.1 ; then
		opt_sound() {
			local devname="${1:-all}"
			local outdev="${2}"
			case "${devname}" in
			hda)
				opt_addconfval -device "intel-hda"
				opt_addconfval -device "hda-duplex${outdev:+,audiodev=${outdev}}"
				;;
			pcspk)	# FIXME need to integrate into -machine
				-machine "pcspk-audiodev=${outdev}"
				;;
			ac97) opt_addconfval -device "AC97${outdev:+,audiodev=${outdev}}" ;;
			es1370) opt_addconfval -device "ES1370${outdev:+,audiodev=${outdev}}" ;;
			all) for dev in cs4231a gus hda adlib sb16 ac97 es1370 ; do opt_sound "${dev}" "${outdev}" ; done ;;
			*) opt_addconfval -device "${devname}${outdev:+,audiodev=${outdev}}" ;;
			esac
		}
	else
		opt_sound() {
			opt_addconfval -soundhw "${1:-all}"
		}
	fi
	if qemu_isAtLeast 4 ; then	# 4.0
		# https://wiki.qemu.org/ChangeLog/4.0#New_deprecated_options_and_features
		opt_soundout() {
			local drivername="${1:-alsa}"
			local devid="${2:-${drivername}}"
			local opt="${3}"
			opt_addconfval -audiodev "id=${devid},driver=${drivername}${opt:+",${opt}"}"
		}
	else
		opt_soundout() {
			local drivername="${1:-alsa}"
			# to be exported to environment when starting QEMU
			ENV_QEMU_AUDIO_DRV="${drivername}"
		}
	fi
	opt_soundoutpulse() { opt_soundout pa ; }
	opt_soundoutalsa() { opt_soundout alsa ; }
	opt_soundoutoss() { opt_soundout oss ; }
	opt_soundoutnone() { opt_soundout none ; }
	opt_drive() {
		local mediatype="${3:-disk}"
		local opts="${4}"
		opt_addconfval -drive file="${1}${2:+,index="${2}"},media=${mediatype}${opts:+",${opts}"}"
	}
	opt_hda() {
		opt_addconfval -hda "${1}"
	}
	opt_cd() {
		opt_addconfval -cdrom "${1}"
	}
	opt_fs() {
		local path="${1}"
		local tag="${2}"
		local fsid="${3:-fs1}"
		local opts="${4}"
		local fsopts=
		case " ${opts} " in
		*" readonly "*) fsopts="${fsopts},readonly" ;;
		esac
		opt_addconfval -fsdev "local,path=${path},security_model=mapped-xattr,id=${fsid}${fsopts}"
		opt_addconfval -device "virtio-9p-pci,fsdev=${fsid},mount_tag=${tag}"
	}
	opt_looplinux() {
		local loop="${1}"
		local kernel="${2}"
		local kernopt="${3}"
		local initrd="${4}"
		opt_addconfval -kernel "${loop}${kernel}"
		[ -n "${kernopt}" ] && opt_addconfval -append "${kernopt}"
		if [ -n "${initrd}" ] ; then
			[ -r "${loop}${initrd}" ] && initrd="${loop}${initrd}"
			opt_addconfval -initrd "${initrd}"
		fi
	}
	opt_markpersist() {
		opt_persist=1
		"$@"
	}
	opt_ispersist() {
		case "${opt_persist}" in
		""|0|false|no) return 1 ;;
		*) return 0 ;;
		esac
	}
	opt_daemon() {
		opt_addconfval -daemonize
	}
	opt_misc() {
		opt_addval "$@"
	}
} ; opt_setup
