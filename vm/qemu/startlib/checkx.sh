# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Quickly checks to see if on a terminal with X
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

checkx_warn() {
	local fmterr
	local fmtrst
	if [ -t 1 ] && type termcol_color >/dev/null 2>&1 && fmterr="$( termcol_color "${termcol_ansi_red}" )" && fmtrst="$( termcol_color )" ; then
		: use it
	else
		fmterr= ; fmtrst=
	fi
	echo "${fmterr}Display not set${fmtrst}: are we on a terminal with X running?"
}
if [ -z "${DISPLAY}" ] ; then
	checkx_warn
fi
