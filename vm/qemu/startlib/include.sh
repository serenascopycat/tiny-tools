# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides functions for including scripts in the startlib library.
### HOWTO ##############################
# 1) Define incl_getlibdir() function that prints out location of startlib files
# 2) Source this file
# 3) Call functions to include files
#  i) file sourcing functions
#   *) all functions take location of startlib as first parameter
#   a) incl_do: include specified files (defaults library directory to incl_getlibdir)
#   a) incl_sources: include specified files
#   b) incl_source: include specified file (just one)
#  ii) file lists
#   a) incl_addlib: basic files
#   b) incl_addxlive: files for starting Live CDs in QEMU under X
#  e.g. see incl_addalllive()
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if ! type incl_getlibdir >/dev/null 2>&1 ; then
	incl_getlibdir() {
		cd "$( dirname "$0" )/.." && pwd
	}
fi
incl_source() {
	local dirbase="${1}"
	local srcpath="${2}"
	shift 2	# rest are arguments to sourced script
	[ "${srcpath#/}!" = "${srcpath}!" ] && srcpath="${dirbase}/${srcpath}"
	[ -r "${srcpath}" ] && . "${srcpath}"
}
incl_sources() {
	local dirbase="${1}"
	local srcpath=
	while [ 1 -lt "$#" ] ; do
		shift
		incl_source "${dirbase}" "${1}" || continue
	done
}

incl_do() {
	local libdir="${1:-$( getlibdir )}"
	shift
	incl_sources "${libdir}" "$@"
}
