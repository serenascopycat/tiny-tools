# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Functions to colorize terminal output
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if type tput >/dev/null ; then
	termcol_ansi_blk=0
	termcol_ansi_red=1
	termcol_ansi_grn=2
	termcol_ansi_ylw=3
	termcol_ansi_blu=4
	termcol_ansi_mgn=5
	termcol_ansi_cyn=6
	termcol_ansi_wht=7

	termcol_fgcolor() {
		tput setaf "$1"
	}
	termcol_bgcolor() {
		tput setab "$1"
	}
	termcol__colorpair() {
		tput -S <<- ~
			setaf ${1}
			setab ${2}
		~
	}
	termcol_cleartoeol() {
		# https://stackoverflow.com/questions/7548135/terminal-emulator-that-allows-dynamically-change-of-background-color#7555518
		tput bce && tput el
	}
	termcol_reset() {
		# might not reset quite right if lines are scrolled
		# the last line might be fully colored even though
		# the reset code is there before newline
		tput sgr0
	}
	termcol_color() {
		case "$#" in
		0)
			termcol_reset
			;;
		1)
			termcol_fgcolor "$1"
			;;
		*)
			termcol__colorpair "$@"
			;;
		esac
	}
fi
