# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides functions related to loop device.
### DEPENDENCIES #######################
# fnchatty.sh
# fnshl.sh
# udisksctl
# sfdisk (optional; read partition table to find partition to mount)
# lsblk (check filesystem info)
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if type findmnt >/dev/null ; then
	lo__checkmnt() {
		[ -d "$1" ] && findmnt --output SOURCE --noheadings --mountpoint "$1"
	}
	lo__getmntpts() {
		findmnt --list --noheadings --output TARGET --source "${1}"
	}
else
	lo__checkmnt() {
		[ -d "$1" ] && awk -v mntpt="${1}" '$2 == mntpt { print $1 }' /etc/mtab
	}
	lo__getmntpts() {
		awk -v dev="${1}" '$1 == dev { print $2 }' /etc/mtab
	}
fi
if shl_isbusyboxcmd losetup ; then
	# BusyBox losetup doesn't support -j (--associated)
	lo__foreachloopdevof() {
		local cd="${1}"
		losetup -a | awk -v cd="${cd}" '$3 == cd { print $1 }' | sed 's/:$//'
	}
else
	lo__foreachloopdevof() {
		local cd="${1}"
		# losetup may not support --noheadings (e.g. on Fedora 19)
		losetup -j "${cd}" --output NAME --noheadings || \
			losetup -j "${cd}" --output NAME | tail -n +2
	}
fi
lo__foreachloopof() {
	local cd="${1}"
	local cmd="${2}"
	local loopdev=
	shift 2
	lo__foreachloopdevof "${cd}" | while IFS= read loopdev ; do
		"${cmd}" "${cd}" "${loopdev}" "$@" || break
	done
}
lo_findloop() {
	local cd="${1}"
	local kernel="${2}"
	lo__findmnt() {
		local cd="${1}"
		local loopdev="${2}"
		local kernel="${3}"
		local partdev= mnt=
		# zsh borks when it can't find any element within the list instead of just skipping or using it literally
		for partdev in "${loopdev}"* ; do
			if { [ "${partdev}!" = "${loopdev}!" ] || [ "${partdev#${loopdev}p}!" != "${partdev}!" ] ; } && [ -e "${partdev}" ] ; then
				lo__getmntpts "${partdev}" | while IFS= read mnt ; do
					if [ -r "${mnt}${kernel}" ] ; then
						echo "${mnt}"
						chatty_info "${mnt} device is ${partdev}, backed by ${cd}."
						return 1
					fi
				done
			fi
		done
		return 0
	}
	lo__foreachloopof "${cd}" lo__findmnt "${kernel}"
}
if type udisksctl > /dev/null && type lsblk > /dev/null ; then
	# use udisks to avoid having to use root privileges
	lo_mountloop() {
		local cd="${1}"
		local mnt=
		lo__mountlooppart() {
			local targetpart= targetpts=0 part= partpts= fstype= fslabel=
			while IFS= read part ; do
				partpts="1"
				# prefer the first one or the device itself
				if [ "${part}!" = "${loopdev}!" ] ; then
					partpts="${partpts} + 4"
				elif [ "${part}!" = "${loopdev}p1!" ] ; then
					partpts="${partpts} + 2"
				fi
				# prefer labeled ones
				partpts="${partpts} $( lsblk "${part}" --list --output FSTYPE,LABEL --noheadings | (
					local partpts2=
					read -r fstype fslabel
					[ "iso9660!" = "${fstype}!" ] && partpts2="${partpts2} + 8"
					[ -n "${fslabel}" ] && partpts2="${partpts2} + 16"
					echo ${partpts2}
				) )"
				partpts="$(( ${partpts} ))"
#				chatty_info "${part} has ${partpts} points."
				if [ "${targetpts}" -lt "${partpts}" ] ; then
					targetpart="${part}"
					targetpts="${partpts}"
				fi
			done
			[ -n "${targetpart}" ] || return 1
			chatty_info "Mounting partition in ${targetpart}."
			local mountout=
			# udisksctl freaks out on Fedora 27 when using --no-user-interaction
			{ mountout="$( udisksctl mount --no-user-interaction --options ro --block-device "${targetpart}" 2> /dev/null )" || \
				mountout="$( udisksctl mount --options ro --block-device "${targetpart}" )" ; } && \
				echo "${mountout}"
		}
		lo__mountloop() {
			local cd="${1}"
			local loopdev="${2}"
			# send in partition(s) that're tagged bootable to pick from
			# falling back to all partition and even the device itself
			# for parititioned ISOs, assumes ISO is the hybrid writable-to-USB kind
			# so the system should be at the bootable partition
			# Debian live-boot and Ubuntu casper are on hybrid ISOs (partitioned)
			# Knoppix is a regular (non-partitioned) ISO
			# sfdisk needs to read loop device, but it may or may not be readable
			# readable in Knoppix 7.6 but not Fedora 27
			# use lsblk as fallback but have to parse partition flags
			# rather than parsing for a 0x80 bit, match all combinations of it
			{ { [ -r "${loopdev}" ] && sfdisk --activate "${loopdev}" 2> /dev/null ; } || \
				lsblk "${loopdev}" --list --paths --output NAME,PARTFLAGS --noheadings | \
					awk '$2 ~ /0x[89A-Fa-f][0-9A-Fa-f]/ { print $1 }' ; } | lo__mountlooppart || \
				lsblk "${loopdev}" --list --paths --output NAME --noheadings | \
					lo__mountlooppart || return
		}
		# mounted is output of successful udisksctl mount
		local mounted="$( lo__foreachloopof "${cd}" lo__mountloop )"
		if [ -z "${mounted}" ] ; then
			# FIXME udisksctl freaks out on Fedora 19
			# Error setting up loop device for /run/initramfs/isoscan/iso/dsl-4.4.10.iso: The connection is closed
			udisksctl loop-setup --no-user-interaction --read-only --file "${cd}" > /dev/null
			mounted="$( lo__foreachloopof "${cd}" lo__mountloop )"
		fi
		lo_findloop "${cd}"
	}
else	# mount with root privileges
	if [ -d '/media' ] ; then
		lo_getisomountroot() {
			echo /media/iso	
		}
	else
		lo_getisomountroot() {
			echo /mnt/iso
		}
	fi
	lo_getisomountpath() {
		local result="$( lo_getisomountroot )"
		while [ 0 -lt "$#" ] ; do
			[ -n "${1}" ] && result="${result}/${1}"
			shift
		done
		echo "${result}"
	}
	if sudo -l mkdir > /dev/null ; then
		: "OK, can make dirs on the fly"
	elif [ -e "$( lo_getisomountpath )" ] ; then
		echo "Warning: Mount points for the ISOs must already exist."
	else
		echo "Warning: Mount point for the ISO does not exist and cannot be created."
	fi
	if sudo -l mount > /dev/null ; then
		: "OK, can mount with options"
	else
		echo "Warning: ISOs must already be mounted."
	fi
	lo_getisomountpathfor() {
		local cd="${1}"
		lo_getisomountpath "$( basename "${cd}" .iso )"
	}
	lo_mountloop() {
		local cd="${1}"
		local mnt="$( lo_getisomountpathfor "${cd}" )"
		if [ ! -e "${mnt}" ] ; then
			# try to make it ready
			sudo mkdir -p "${mnt}" || mkdir -p "${mnt}"
		fi
		chatty_info "Mounting ${cd} at ${mnt}..."
		sudo mount -o ro,loop "${cd}" "${mnt}"
		echo "${mnt}"
	}
fi
lo_tryloop() {
	local cd="${1}"
	local kernel="${2}"
	local mntpath="$( lo_findloop "${cd}" "${kernel}" )"
	[ -n "${mntpath}" ] || mntpath="$( lo_mountloop "${cd}" )"
	[ -r "${mntpath}${kernel}" ] || chatty_fail 1 "Kernel not readable at ${mntpath}${kernel}."
	echo "${mntpath}"
}

