# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Template for setting up QEMU command-line options.
### DEPENDENCIES #######################
# fnchatty.sh
# qyexec.sh
### HOWTO ##############################
# 1) Source this file
#   - tmpl_nosound: Skips sound setup when set to anything not blank (including 0/no/false)
# 2) Override live_*() functions
# 3) Call live_setopt
#  - can use shell variables to pass parameters into template
#   - mem, mem64
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

vm_qemu64() { qemu=qemu-system-x86_64 ; }
vm_qemu32() { qemu=qemu-system-i386 ; }
# assume tmpl_arch_bits to be available only during sourcing
case "${tmpl_arch_bits}" in
64|'') vm_qemu() { vm_qemu64 "$@" ; } ;;
32) vm_qemu() { vm_qemu32 "$@" ; } ;;
*) chatty_fail 32 'Unknown bitness' ;;
esac
vm_kvm() { opt_kvm ; }

vm_portal() { : 'options for experience on host' ; }

vm_rtc() { : 'options for real time clock' ; }

vm_processor() { : 'options for processors' ; }

# $mem is the memory to use, regardless of 32-bit or 64-bit,
# defaulting to maximum for 32-bit host QEMU process using KVM (2047M).
# Should be kept strictly below 2048M / 2G or will likely bomb on 32-bit QEMU.
# $mem64 is the memory to use on 64-bit systems; ignored for 32-bit systems.
# Should be 2048M+ / 2G+; else use $mem for same memory size for both 32-bit and 64-bit.
# tmpl_arch_bits is only available during sourcing
if [ 64 != "${tmpl_arch_bits}" ] || sys_is32bit || ( vm_qemu ; qemu_is32bit "${qemu}" ) ; then
	# No real point to assign more than 2GiB to 32-bit systems
	# Some can use 4GiB or have address extension, but being a bit conservative here
	# 32-bit QEMU binary can only support 2047MB
	vm_mem() { opt_memory "${mem:-2047M}" ; }
else
	vm_mem() { opt_memory "${mem:-"${mem64:-3G}"}" ; }
fi

vm_input() { : 'input hardware options' ; }

vm_video() { : 'video hardware options' ; }

if [ -n "${tmpl_nosound}" ] ; then
	vm_sound() { : 'sound hardware options' ; }
else
	if [ -n "${tmpl_arch_bits}" ] && [ 64 -eq "${tmpl_arch_bits}" ] ; then
		# pick more recent hardware for the more recent 64-bit systems
		vm_sound_hw() {
			# set audiohw to the soundhw option to use
			# default to hda; Kali 2021.1 doesn't know sb16
			audiohw="${1:-hda}"
		}
	else
		vm_sound_hw() {
			# set audiohw to the soundhw option to use
			audiohw="${1:-ac97}"
		}
	fi
	vm_sound_out() {
		# set audioout to host sound output driver: pa, alsa, oss, none
		# optionally set audioopt to extra options to send with audioout
		# optionally set audioid to reference id to use
		# https://askubuntu.com/questions/426983/how-can-i-tell-if-im-using-alsa-or-pulse-audio-by-default-switching-to-i3-wm#427036
		if [ -x '/usr/bin/pactl' ] && \
				pasrv="$( /usr/bin/pactl info 2> /dev/null | sed -n 's/^Server String: //p;q' )" && \
				[ -n "${pasrv}" ]; then
			audioout=pa
			audioopt="server=${PULSE_SERVER:-"${pasrv}"}"
		elif [ -x '/usr/bin/aplay' ] && /usr/bin/aplay -l > /dev/null 2>&1 ; then
			audioout=alsa
		fi
	}
	vm_sound() {
		local audioout=none
		local audioopt=
		local audioid=
		local pasrv=
		local audiohw=
		vm_sound_hw "${soundhw}"
		vm_sound_out
		if [ -n "${audiohw}" ] && [ -n "${audioout}" ]; then
			opt_sound "${audiohw}" "${audioout}"
			opt_soundout "${audioout}" "${audioid}" "${audioopt}"
		fi
	}
fi

vm_drive_apply() {
	local mediatype="${1:-disk}"
	local file="${2}"
	local index="${3}"
	local opts="${4}"
	opt_drive "${file}" "${index}" "${mediatype}" "${opts}"
}

vm_cd_entries() {
	: 'To be overridden in format: "$@" file [driveindex]'
}
vm_cd_apply() {
	vm_drive_apply cdrom "$@"
}
vm_cd() {
	vm_cd_entries vm_cd_apply
}

vm_hd_entries() {
	: 'To be overridden in format: "$@" file [driveindex]'
}
vm_hd_apply() {
	vm_drive_apply disk "$@"
}
vm_hd() {
	vm_hd_entries vm_hd_apply
}

vm_fs_entries() {
	: 'To be overridden in format: "$@" hostdir tagname fsid [opts]'
}
vm_fs_apply() {
	local mntpt="${1}" tag="${2}" id="${3}" opts="${4}"
	opt_fs "${mntpt}" "${tag}" "${id}" "${opts}"
}
vm_fs() {
	vm_fs_entries vm_fs_apply
}

vm_net() { : ; }

# Main options setup
vm_setopt() {
	vm_qemu
	vm_kvm
	vm_portal
	vm_rtc
	vm_processor
	vm_mem
	vm_input
	vm_video
	vm_sound
	vm_hd
	vm_fs
	vm_net
	opt_daemon
}

# Start with options that were set up
vm_start() { (
	[ -n "${ENV_QEMU_AUDIO_DRV}" ] && export QEMU_AUDIO_DRV="${ENV_QEMU_AUDIO_DRV}"
	opt_xargs_maybeprompt "${qemu}"
) ; }
