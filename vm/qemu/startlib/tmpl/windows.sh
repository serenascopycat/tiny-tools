# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Template for setting up QEMU command-line options so QEMU can start MS Windows.
### DEPENDENCIES #######################
# tmpl/basic
### HOWTO ##############################
# 1) Source this file
# 2) Override vm_*() functions
# 3) Call live_setopt
#  - can use shell variables to pass parameters into template
#   - mem, mem64
### LICENSING ##########################
# Copycenter 2023 Serena's Copycat; licensed under CC0

# -smbios and -acpitable options for transferring SLP license to VM
vm_id() { : 'machine identifier options' ; }

vm_rtc() {
	opt_misc -rtc 'base=localtime'
}

vm_input() {
	opt_misc -device usb-ehci	# no support for qemu-xhci
	opt_misc -device usb-tablet
}

vm_fs_apply() {
	local mntpt="${1}" tag="${2}" id="${3}" opts="${4}"
	# TODO set up samba if available
}

# Main options setup
vm_setopt() {
	vm_qemu
	vm_kvm
	vm_portal
	vm_rtc
	vm_processor
	vm_mem
	vm_input
	vm_video
	vm_sound
	vm_hd
	vm_fs
	vm_net
	vm_id	# do this last; adding hw like RTC afterwards messes with it
	opt_daemon
}
