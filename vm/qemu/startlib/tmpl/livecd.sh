# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Template for setting up QEMU command-line options so QEMU can start Live CDs.
### DEPENDENCIES #######################
# tmpl/basic
### HOWTO ##############################
# 1) Source this file
# 2) Override live_*() functions
# 3) Call live_setopt
#  - can use shell variables to pass parameters into template
#   - mem, mem64
#   - cd: Live CD ISO path
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# pick more recent hardware for live CD systems
vm_sound_hw() {
	# set audiohw to the soundhw option to use
	# default to hda; Kali 2021.1 doesn't know sb16
	audiohw="${1:-hda}"
}

live_cd_base() {
	local cd="${1}"
	opt_cd "${cd}"
}
vm_cd() {
	local cd="${1}"
	live_cd_base "${cd}"
	vm_cd_entries vm_cd_apply
}

case "${livecd_boot}" in
casper)
	live_preplinux() { : ; }
	# opt_markpersist, if needed, must already be called before this.
	live_linux() {
		local cd="${1:-${cd}}"
		local kernel="${2:-${kernel:-/casper/vmlinuz}}"
		local optpersist=
		opt_ispersist && optpersist="persistent"
		local kernelarg="${4:-${kernelarg:-"file=/cdrom/preseed/linuxmint.seed boot=casper quiet splash ${optpersist} noprompt ${kernelargadd} --"}}"
		local initrd="${5:-${initrd:-/casper/initrd.lz}}"
		local loop
		loop="$( lo_tryloop "${cd}" "${kernel}" )" && \
			live_preplinux "${cd}" "${loop}" "${kernel}" "${kernelarg}" "${initrd}" && \
			opt_looplinux "${loop}" "${kernel}" "${kernelarg}" "${initrd}"
	} ;;
live-boot)
	live_preplinux() { : ; }
	# opt_markpersist, if needed, must already be called before this.
	live_linux() {
		local cd="${1:-${cd}}"
		local kernel="${2:-${kernel:-/live/vmlinuz}}"
		local optpersist=
		opt_ispersist && optpersist="persistence"
		local kernelarg="${4:-${kernelarg:-"boot=live quiet splash ${optpersist} noeject ${kernelargadd}"}}"
		local initrd="${5:-${initrd:-/live/initrd.img}}"
		local loop
		loop="$( lo_tryloop "${cd}" "${kernel}" )" && \
			live_preplinux "${cd}" "${loop}" "${kernel}" "${kernelarg}" "${initrd}" && \
			opt_looplinux "${loop}" "${kernel}" "${kernelarg}" "${initrd}"
	} ;;
*) live_linux() { : ; } ;;
esac

# Main options setup
vm_setopt() {
	local cd="${1:-${cd}}"
	[ -r "${cd}" ] || chatty_fail 1 "Cannot read ISO at ${cd}."
	vm_qemu
	vm_kvm
	vm_portal
	vm_processor
	vm_mem
	vm_input
	vm_video
	vm_sound
	vm_cd "${cd}"
	vm_hd
	vm_fs
	vm_net
	live_linux "${cd}"
	opt_daemon
}

