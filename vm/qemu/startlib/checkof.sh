# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Check for open files in QEMU.
### LICENSING ##########################
# Copycenter 2023 Serena's Copycat; licensed under CC0

# Check for QEMU using files directly in specified dir to prevent persistence corruption
# This is NOT designed to be fool-proof but only as a courtesy check
# esp. because this can only check files in processes accessible by current user
# a simpler way would be to do: lsof -a -c qemu +D "$( dirname "$0" )"
# but busybox lsof does not filter whatsoever
checkof_no_open_file_in() {
	local contentdir="${1:-"$( dirname "$0" )"}"
	local fd procdir procexe
	local sedflag=
	# busybox won't take -z, but needed on full sed to get only the first word
	shl_isbusyboxcmd sed || sedflag="-z"
	local qemu32="$( which qemu-system-i386 )"
	local qemu64="$( which qemu-system-x86_64 )"
	for fd in $( find /proc -mindepth 2 -maxdepth 2 -type d -name 'fd' ) ; do
		[ -r "${fd}" ] || continue	# no point checking unreadable processes
		procdir="$( dirname "${fd}" )"
		procexe="${procdir}/exe"
		# try various ways to check for QEMU executable
		[ "${procexe}" -ef "${qemu64}" ] || \
			[ "${procexe}" -ef "${qemu32}" ] || \
			[ -n "$( sed -n ${sedflag} '1s/.*qemu.*/qemu/p;q' "${procdir}/cmdline" 2> /dev/null )" ] || \
			readlink "${procexe}" 2> /dev/null | grep -q qemu || \
			continue
		# first cache inode numbers within the directory to match against
		# (because the filenames are less predictable and quoting reliably in awk is hard),
		# then match them against the FDs in the procs, first quickly via inode number,
		# then going back to contentdir and testing for file equivalence,
		# in case there are duplicate inode numbers across multiple filesystems.
		# do it all while in contentdir so path references to it are just ".",
		# again because quoting in awk is hard.
		( cd "${contentdir}" ; { ls -ALi "." ; echo 'MATCH' ; ls -Li "${fd}" ; } | awk '
			"MATCH" == $0 { mode = "match"; next; }
			"cache" == mode { inodes[$1] = 1; }
			"match" == mode {
				if ($1 in inodes && 0 == system("find . -maxdepth 2 -inum " $1 " -exec [ {} -ef " $2 " ] \\;")) { exit 1; }
			}' mode=cache - || return ) || return
	done
}
