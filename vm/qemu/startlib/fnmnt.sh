# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Get the path where device with specified UUID is mounted,
# with an optional subpath under said path.
# Will try to actually cd into it to make sure it's accessible
# and links are resolved.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

if type findmnt >/dev/null ; then
	mnt__uuid_resolvedev() {
		local uuid="${1}"
		findmnt --list --noheadings --output TARGET --source "UUID=${uuid}"
	}
else
	mnt__uuid_resolvedev() {
		blkid -t "UUID=${1}" -o device | awk -v "path=${2}" 'BEGIN {
				exit_code = 1;
				quote = "\047";
			}; 1 == FNR {
				in_blk = 0;
				in_mnt = 0;
				if ("-" == FILENAME) {
					in_blk = 1;
				} else if ("/etc/mtab" == FILENAME) {
					in_mnt = 1;
					if (1 != hasblkdevs) { exit_code = 1 ; exit; }
				}
			}; 1 == in_blk {
				blkdevs[$1] = 1;
				hasblkdevs = 1;
			}; 1 == in_mnt && 1 == blkdevs[$1] {
				cmd = "( cd " quote $2 path quote " && pwd )"
				cmd | getline resolvedpath
				if ("" != resolvedpath) {
					print resolvedpath;
					exit_code = 0;
					exit;
				}
				blkdevs[$1] = 2;
			}; END {
				exit exit_code;
			};' - /etc/mtab
	}
fi

mnt_uuid_resolvepath() {
	local uuid="${1}" path="${2}" mnt
	if [ -z "${uuid}" ] ; then
		return 128	# mount uses return codes 127 and below
	elif ! mnt="$( mnt__uuid_resolvedev "${uuid}" )" ; then
		return 129	# not yet mounted?
	elif ( cd "${mnt}${path}" && pwd ) ; then
		: "OK"
	else
		return 130
	fi
}
if [ -d '/dev/disk/by-uuid' ] ; then
	mnt__uuid_mount_do() {
		local uuid="${1}"
		( cd '/dev/disk/by-uuid/' ; mount "$( readlink "${uuid}" )" )
	}
else
	# Tiny Core Linux does not have /dev/disk/by-uuid
	mnt__uuid_mount_do() {
		blkid --match-token "UUID=${1}" --output device | while IFS= read d ; do
			mount "${d}" && break
		done
	}
fi
mnt_uuid_mount() {
	local uuid="${1}" path="${2}" rcode=0
	mnt_uuid_resolvepath "${uuid}" "${path}" || rcode="$?"
	[ "129" -eq "${rcode}" ] || return "${rcode}"
	mnt__uuid_mount_do "${uuid}" && mnt_uuid_resolvepath "${uuid}" "${path}"
}

## ISO search
# When on live CD systems, check the same directory as the live CD for the ISO
mnt_checkiso() {
	local iso_path="${1}"
	[ -r "${iso_path}" ] && echo "${iso_path}" || return 1
}
mnt_findisoknoppix() {
	local iso_path="${1}"
	# /mnt-iso is used on Knoppix
	mnt_checkiso "/mnt-iso${iso_path}"
}
mnt_findisodracut() {
	local iso_path="${1}"
	# /run/initramfs/isoscan is used by dracut (Fedora)
	mnt_checkiso "/run/initramfs/isoscan${iso_path}"
}
mnt_findisocasper() {
	local iso_path="${1}"
	# /isodevice is used by casper (Ubuntu)
	mnt_checkiso "/isodevice${iso_path}"
}
mnt_findisoliveboot() {
	local iso_path="${1}"
	# /run/live/fromiso or /run/live/findiso is used by live-boot (Debian)
	# depends on whether fromiso or findiso is used for boot parameter
	# isofrom is equivalent to fromiso; path used is the same
	mnt_checkiso "/run/live/fromiso${iso_path}" || mnt_checkiso "/run/live/findiso${iso_path}"
}
mnt_findisobyuuid() {
	local iso_path="${1}"
	mnt_checkiso "$( mnt_uuid_resolvepath "${fs_uuid_livecd}" )${iso_path}"
}
mnt_findiso() {
	local iso_path="${1}"
	mnt_findisocasper "${iso_path}" || \
		mnt_findisoliveboot "${iso_path}" || \
		mnt_findisodracut "${iso_path}" || \
		mnt_findisoknoppix "${iso_path}" || \
		mnt_findisobyuuid "${iso_path}"
}

