#!/bin/sh
### WHAT THIS DOES #####################
# Copies data from BIOS of host machine into a VirtualBox VM
# so that OS such as Windows 7 will activate via OEM activation,
# in order to facilitate transfer of license from physical to virtual machine.
### HOWTO ##############################
# 1) Run this script as superuser with the following parameters:
#   a) VM_NAME
#     Name of VM, as used by VBoxManage to reference a VM
#   b) VM_USER
#     User who owns the VM into which data is to be copied
#     When not specified, assumes calling user is owner, and
#       portions requiring superuser privileges will be skipped.
#       (Assumes superuser doesn't own VMs; that's dangerous.)
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -o noclobber -o errexit

. "$( dirname "${0}" )/dmiread.sh"
. "$( dirname "${0}" )/vrtlbox.sh"

extractslic() {
	local VM_CFG_PATH="$( vrtlbox_getCfgFileAsUser )"
	VM_CFG_PATH="$( dirname "$VM_CFG_PATH" )"
	SLIC_PATH="${VM_CFG_PATH}/slic.bin"

	# give closed stdin to umask to make sure it eats nothing
	# so the entire SLIC data goes to cat
	# normally umask doesn't take anything from stdin anyway
	if [ ! -e "${SLIC_PATH}" ] ; then
		echo Saving SLIC at ${SLIC_PATH}
		cat '/sys/firmware/acpi/tables/SLIC' | su -c 'umask 377 <&- ; cat > "${0}"' "${VM_USER}" "${SLIC_PATH}"
	fi
	# 9.13 Configuring the custom ACPI table
	[ -e "${SLIC_PATH}" ] && vrtlbox_setSLICAsUser "${SLIC_PATH}"
}

process() {
	local dataname="$1"
	local filename="$2"
	local dmidecode_string="$3"
	dmi_processDMIData "${filename}" "${dmidecode_string}" \
		vrtlbox_process vrtlbox_setDMIAsUser "${dataname}" || :
}

# tag for steps needing superuser privilege
superpriv() {
	"$@"
}

VM_NAME="$1"
VM_USER="$2"

if [ -z "${VM_USER}" ]
then
	echo Username not specified.  Assuming current user can both read DMI and has VM.
	echo Skipping portions requiring root privileges.
	process() {
		local dataname="$1"
		local filename="$2"
		local dmidecode_string="$3"
		dmi_processDMIData "${filename}" "${dmidecode_string}" \
			vrtlbox_process vrtlbox_setDMI "${dataname}" || :
	}
	superpriv() {
		echo Skipping "$@" >&2
	}
fi
if [ -z "${VM_NAME}" ]
then
	echo VM name not specified.
	echo "Usage: \"$0\" "'"<VM_Name>" "<VM_User>"'
	echo "  <VM_Name>: identifier of VM to copy DMI values to"
	echo "  <VM_User>: name of user whose VM is to be looked at"
	echo "    May be blank to use current user; however script will skip superuser steps."
	echo "    Running VirtualBox with superuser privileges is discouraged."
	false
fi

if false ; then	# debug dump
	vrtlbox_setDMI() {
		echo "DMI to set:" "$@"
	}
	vrtlbox_setExtraAsUser() {
		echo "Extra to set:" "$@"
	}
	process() {
		local dataname="$1"
		local filename="$2"
		local dmidecode_string="$3"
		dmi_processDMIData "${filename}" "${dmidecode_string}" \
			echo "${dataname}" || :
	}
fi



superpriv extractslic
process 'DmiBIOSVendor' 'bios_vendor' 'bios-vendor'
process 'DmiBIOSVersion' 'bios_version' 'bios-version'
process 'DmiBIOSReleaseDate' 'bios_date' 'bios-release-date'
#process 'DmiBIOSReleaseMajor'  1
#process 'DmiBIOSReleaseMinor'  2
#process 'DmiBIOSFirmwareMajor' 3
#process 'DmiBIOSFirmwareMinor' 4
process 'DmiSystemVendor' 'sys_vendor' 'system-manufacturer'
process 'DmiSystemProduct' 'product_name' 'system-product-name'
process 'DmiSystemVersion' 'product_version' 'system-version'
superpriv process 'DmiSystemSerial' 'product_serial' 'system-serial-number'
#process 'DmiSystemSKU'         "System SKU"
#process 'DmiSystemFamily'      "System Family"
superpriv process 'DmiSystemUuid' 'product_uuid' 'system-uuid'
process 'DmiBoardVendor' 'board_vendor' 'baseboard-manufacturer'
process 'DmiBoardProduct' 'board_name' 'baseboard-product-name'
process 'DmiBoardVersion' 'board_version' 'baseboard-version'
superpriv process 'DmiBoardSerial' 'board_serial' 'baseboard-serial-number'
process 'DmiBoardAssetTag' 'board_asset_tag' 'baseboard-asset-tag'
#process 'DmiBoardLocInChass'   "Board Location"
#process 'DmiBoardBoardType'    10
process 'DmiChassisVendor' 'chassis_vendor' 'chassis-manufacturer'
process 'DmiChassisType' 'chassis_type' 'chassis-type'
process 'DmiChassisVersion' 'chassis_version' 'chassis-version'
superpriv process 'DmiChassisSerial' 'chassis_serial' 'chassis-serial-number'
process 'DmiChassisAssetTag' 'chassis_asset_tag' 'chassis-asset-tag'
#process 'DmiProcManufacturer"  "GenuineIntel" 'processor-manufacturer'
#process 'DmiProcVersion"       "Pentium(R) III" 'processor-version'
echo Complete.
