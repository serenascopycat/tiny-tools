# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides API to access DMI data via /sys FS (or dmidecode, as fallback).
# Please use dmicopy.sh entry point.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

dmi__getSysFS() {
	local filename="$1"
	[ -n "${filename}" ] && [ -r "/sys/devices/virtual/dmi/id/${filename}" ] && cat "/sys/devices/virtual/dmi/id/${filename}" 2> /dev/null
}
dmi__getDecode() {
	local dmidecode_string="$1"
	dmidecode --string "${dmidecode_string}"
}
dmi_get() {
	local filename="$1"
	local dmidecode_string="$2"
	dmi__getSysFS "${filename}" || dmi__getDecode "${dmidecode_string}"
}
dmi_processDMIData() {
	local filename="${1}"
	local dmidecode_string="$2"
	shift 2
	local value
	if ! value="$( dmi_get "${filename}" "${dmidecode_string}" )" ; then
		echo "Unable to get ${dataname}." >&2
		false
	elif [ -z "${value}" ] ; then
		echo "${dataname} has no value." >&2
		false
	else
		"$@" "${value}"
	fi
}

