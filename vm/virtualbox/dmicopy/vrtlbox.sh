# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Provides API to access VirtualBox and make edits to VM.
# Please use dmicopy.sh entry point.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

# 9.12. Configuring the BIOS DMI information
vrtlbox_process() {
	local processor="${1}"
	local dataname="${2}"
	local value="${3}"
	# tag numeric strings as strings so VirtualBox doesn't get confused
	local checkdataname="${dataname}"
	# Names ending in Type/Major/Minor are numeric
	checkdataname="${checkdataname%Type}"
	checkdataname="${checkdataname%Major}"
	checkdataname="${checkdataname%Minor}"
	if [ "${dataname}" == "${checkdataname}" ] && [ -z "$( echo "${value}" | tr -d '[0-9]' )" ]
	then
		value="string:${value}"
	fi
	"${processor}" "${dataname}" "${value}"
}
vrtlbox_setDMI() {
	local dataname="${1}"
	local value="${2}"
	VBoxManage setextradata "${VM_NAME}" "VBoxInternal/Devices/pcbios/0/Config/${dataname}" "${value}"
}
vrtlbox_setExtraAsUser() {
	local datapath="${1}"
	local dataname="${2}"
	su -c 'VBoxManage setextradata "${0}" "${1}" "${2}"' "${VM_USER}" "${VM_NAME}" "${datapath}" "${dataname}"
}
vrtlbox_setDMIAsUser() {
	local dataname="${1}"
	local value="${2}"
	vrtlbox_setExtraAsUser "VBoxInternal/Devices/pcbios/0/Config/${dataname}" "${value}"
}
vrtlbox_setSLICAsUser() {
	local SLIC_PATH="${1}"
	vrtlbox_setExtraAsUser "VBoxInternal/Devices/acpi/0/Config/CustomTable" "${SLIC_PATH}"
}

vrtlbox_getCfgFileAsUser() {
	# use read to unescape
	su -c 'VBoxManage showvminfo "${0}" --machinereadable 2> /dev/null' "${VM_USER}" "${VM_NAME}" | sed -n '/^CfgFile=/ { s/^CfgFile=\("\?\)\(.*\)\1/\2/p ; q }' | ( IFS= read VM_CFG_PATH && echo ${VM_CFG_PATH} )
}
