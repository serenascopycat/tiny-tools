#!/bin/sh
### WHAT THIS DOES #####################
# Mangles Firefox addons blocklist.
### DEPENDENCIES #######################
# Runs on OpenWRT:
#   Requires ca_certificates for SSL connection
#   Requires xsltproc for XSLT tranform
# Blocklist URL is in the preference "extensions.blocklist.url"
# Works for Firefox 33
### HOWTO ##############################
# To use after installing dependencies:
#   1) Edit files
#      - .list file should have the URL of the original blocklist
#         - use Firefox about:config page to retrieve preference
#      - .xslt file should have the transforms to be applied
#         - default is to strip Adobe/Macromedia Flash from blocklist
#   2) Run this script
#      - Output will be generated at TARGET_PATH
#   3) Change about:config preference entry to point at generated blocklist
#      - e.g. http://router/blocklist/3/blocklist.xml
### LICENSING ##########################
# Copycenter 2020 Serena's Copycat; licensed under CC0

SCRIPT_BASENAME="$( dirname $0 )/$( basename $0 .sh )"
DOWNLOAD_PATH="/tmp/$( basename "$0" .sh ).tmp.xml"
TARGET_PATH="/www/blocklist/3/blocklist.xml"
wget -O "$DOWNLOAD_PATH" "$( cat "${SCRIPT_BASENAME}.list" )" && xsltproc --novalid --nonet --nomkdir -o "$TARGET_PATH" "${SCRIPT_BASENAME}.xslt" "$DOWNLOAD_PATH"
