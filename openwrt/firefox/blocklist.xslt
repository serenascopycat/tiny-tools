<?xml version="1.0" encoding="ASCII"?>
<!-- 
Filters Flash from Firefox blocklist so it can be used without warnings.
LICENSING
Copycenter 2020 Serena's Copycat; Licensed under CC0
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:bl="http://www.mozilla.org/2006/addons-blocklist">
<!--debugging xsl:template match="//*[self::bl:emItem or self::bl:gfxBlacklistEntry or self::bl:certItem]" /-->
<xsl:template match="*">
	<xsl:copy>
		<!-- https://blog.dob.sk/2009/11/11/xslt-copy-node-with-attributes-without-childs -->
		<xsl:copy-of select="@*" />
		<xsl:apply-templates />
	</xsl:copy>
</xsl:template>
<xsl:template match="/bl:blocklist/bl:pluginItems/bl:pluginItem
    [bl:infoURL='https://get.adobe.com/flashplayer/' or
     (bl:match/@name='filename' and contains(bl:match/@exp, 'NPSWF'))]">
	<xsl:comment>
		<xsl:text>drop Flash Player block </xsl:text>
		<xsl:value-of select="@blockID" />
	</xsl:comment>
	<xsl:for-each select="bl:match">
		<xsl:comment>
			<xsl:value-of select="@name" />
			<xsl:text>=</xsl:text>
			<xsl:value-of select="@exp" />
		</xsl:comment>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>

