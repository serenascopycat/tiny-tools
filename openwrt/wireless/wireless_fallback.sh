#!/bin/sh
### WHAT THIS DOES #####################
# Calls wireless_recover.sh when no wireless interfaces are up.
### DEPENDENCIES #######################
# OpenWRT 15
### HOWTO ##############################
# Call after a delay after wireless interface is requested to be brought up.
# See tiny_tools/tweak/linux/openwrtfs/etc/hotplug.d/net/99-wireless_recover
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

# if no 802.11 (wireless) devices are up
if ! find /sys/class/net -maxdepth 1 -exec [ -d '{}/phy80211' ] \; -exec grep -q '^up$' '{}/operstate' \; -print | head -n 1 | grep -q .
then
	logger -t "fallback" "did not find wireless up"
	# fix wireless config
	$( dirname "$0" )/wireless_recover.sh
fi
