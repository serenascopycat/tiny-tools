#!/bin/sh
### WHAT THIS DOES #####################
# Cuts down on wifi interfaces in an attempt to get something up.
# Doesn't check whether wifi interfaces are all down; just cuts down regardless.
# Example cases when an interface prevents others from coming up:
# - both AP and client on the same radio
#  - client prevents the AP from going up because it can't connect to its AP
#  - https://forum.openwrt.org/t/2-radios-client-and-ap-master-no-ssid-being-broadcast/41041/2#post_2
# - interfaces don't have unique MACs and they go into conflict
### DEPENDENCIES #######################
# OpenWRT 15
### HOWTO ##############################
# 1) Adjust country
#  - config_foreach fix_radio wifi-device "US"
# 2) Add checks for preferred interfaces
#  - could add a special option to preferred interface
#  - could check interface name cur_iface_name
# 3) Run script
### LICENSING ##########################
# Copycenter 2019 Serena's Copycat; licensed under CC0

. /lib/functions.sh

save_iface=""
save_iface_pts=-1

config_cb() {
	local type="$1"
	local name="$2"
	if [ -n "$cur_iface_is_ap" ] && [ "$cur_iface_is_ap" -ne 0 ] && \
		[ -n "$cur_iface_has_ssid" ] && [ "$cur_iface_has_ssid" -ne 0 ] && \
		[ -n "$cur_iface_is_disabled" ] && [ "$cur_iface_is_disabled" -eq 0 ]
	then	# done with a config section for an acceptable wifi interface
		local new_pts=0
		# TODO Add to new_pts for preferred interfaces
		if [ "$cur_iface_has_mac" -ne 0 ]
		then
			logger -t "wireless_recover" "Candidate config ${cur_iface_name}: AP=${cur_iface_is_ap} SSID=${cur_iface_has_ssid} Disabled=${cur_iface_is_disabled} MAC=${cur_iface_has_mac}"
			new_pts=$( expr $new_pts + 1 )
			if [ "$save_iface_pts" -lt "$new_pts" ]
			then
				save_iface="$cur_iface_name"
				save_iface_pts="$new_pts"
			fi
		fi
	fi
	cur_iface_is_ap=0
	cur_iface_has_mac=0
	cur_iface_is_disabled=0
	if [ "$type" = "wifi-iface" ]
	then	# set up callback to cache wifi interface details
		cur_iface_name="$name"
		option_cb() {
			local option="$1"
			local value="$2"
			if [ "$option" = "mode" ] && [ "$value" = "ap" ]
			then
				cur_iface_is_ap=1
			elif [ "$option" = "disabled" ] && [ "$value" = 1 ]
			then
				cur_iface_is_disabled=1
			elif [ "$option" = "ssid" ] && [ -n "$value" ]
			then
				cur_iface_has_ssid=1
			elif [ "$option" = "macaddr" ] && [ -n "$value" ]
			then
				cur_iface_has_mac=1
			fi
		}
	elif [ "$type" = "wifi-device" ]
	then	# skip radio config
		cur_iface_name=""
		option_cb() { return ; }
	# FIXME possibly other types of config sections?
	fi
}
disable_interfaces_except() {
	local config="$1"
	local data="$2"
	local is_disabled_iface

	if [ "$config" != "$data" ]
	then
		config_get is_disabled_iface "$config" disabled
		if [ "${is_disabled_iface}!" != "1!" ]
		then
			uci set "wireless.$config.disabled=1"
			has_changed_config=1
		fi
	fi
}
enable_radio_hosting_iface() {
	local save_iface="$1"
	local save_radio
	lofal is_disabled_radio
	config_get save_radio "${save_iface}" device
	config_get is_disabled_radio "${save_radio}" disabled
	if [ -n "${is_disabled_radio}" ]
	then
		uci delete "wireless.${save_radio}.disabled"
		has_changed_config=1
	fi
}
fix_radio() {
	local config="$1"
	local data="$2"
	local country
	config_get country "${config}" country
	if [ "${country}" != "${data}" ]
	then
		uci set "wireless.${config}.country=$data"
		has_changed_config=1
	fi
}

logger -t "wireless_recover" "Scanning wireless config..."
config_load wireless
reset_cb

if [ -n "${save_iface}" ]
then
	logger -t "wireless_recover" "Recovering wireless by keeping only \"${save_iface}\"..."
	config_foreach disable_interfaces_except wifi-iface "${save_iface}"
	# make sure the radio itself is not disabled
	enable_radio_hosting_iface "${save_iface}"
fi
config_foreach fix_radio wifi-device "US"
if [ "${has_changed_config}!" == "1!" ]
then
	uci commit wireless
	/sbin/wifi
fi
