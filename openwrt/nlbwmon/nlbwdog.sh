#!/bin/sh
### WHAT THIS DOES #####################
# Got a hanging / frozen nlbwmon?  Doesn't write out stats because it's stuck?
# Looks at CPU use of nlbwmon, and restarts it if it's hogging CPU.
# A workaround until nlbwmon stops running into left field every so often.
# This script polls for CPU use information over a few seconds via /proc/[pid].
### DEPENDENCIES #######################
# OpenWRT 18
# nlbwmon
### NOTES ##############################
# Could check for hung nlbwmon by seeing if 'nlbw -c list' times out,
# but timeout (coreutils-timeout) not available by default.
# Will not handle multiple instance of nlbwmon...  is that possible?
# Will probably not handle multiple CPUs.  Need to adjust threshold for that.
# Maybe take max of individual CPU jiffies to handle big/small cores?
# Was going to call this nlbwmonmon...  as in the monitor of the monitor...
# but that's too long and so just decided to shorten the watchdog name.
# /proc/ file docs are availalble at 'man 5 proc'; this uses:
#   /proc/uptime      system uptime and jiffies rate determination
#   /proc/stat        overall CPU stats
#   /proc/[pid]/exe   to verify directory is for the right executable
#   /proc/[pid]/stat  stats for [nlbwmon] process
### HOWTO ##############################
# 1) Run this whenever nlbwmon should be checked
#    - e.g. in /etc/crontabs/root
#       - 7 * * * *       sh "/path/to/nlbwdog.sh"
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

nlbwmon="/usr/sbin/nlbwmon"
nlbwmon_init="/etc/init.d/nlbwmon"

error() {
    echo "$@"
    false
}
find_nlbwmon_proc() {
    # nlbwmon doesn't have a PID file...
    if type pidof > /dev/null ; then
	# use 'pidof' to speed up the search
	for p in $( pidof "$( basename "${nlbwmon}" )" ) ; do
	    if [ "/proc/${p}/exe" -ef "${nlbwmon}" ] ; then
		echo "/proc/${p}"
		return
	    fi
	done
    else
	# crawl /proc as fallback
	find /proc -mindepth 2 -maxdepth 2 -type l -iname 'exe' \
	     -exec sh -c '[ "$0" -ef "${nlbwmon}" ] && echo $( dirname "$0" )' {} \;
    fi
}
get_nlbwmon_stat() {
    sed -n 's/[0-9]\+\s(.*)\s[A-Za-z]\s//p' "${1}"
}
dump_cpu_time_split() {
    # dump times over 2 lines:
    #   1) busy jiffies / idle jiffies
    #   2) uptime in seconds
    # try to do it all with 1 awk to reduce difference
    # caused by forking awk separately
    awk '/^cpu\s/ { print $2 + $3 + $4 "/" $5 ; getline <"/proc/uptime" ; print $1 ; exit }' "/proc/stat"
}
set_baseline() {
    local sys_times_base="$( dump_cpu_time_split )"
    uptime_base="$( echo "${sys_times_base}" | tail -n 1 )"
    sys_times_base="$( echo "${sys_times_base}" | head -n 1 )"
    sys_busy_time="${sys_times_base%/*}"
    sys_idle_time="${sys_times_base#*/}"
}
get_cpu_time_limit() {
    # requires set_baseline to be called earlier
    # will output number of busy (non-idle) jiffies if system is busy
    # busy = less than 5% CPU time is idle
    local act_get_cpu_times='busy_jif = $2 + $3 + $4 ; idle_jif = $5'
    local act_get_uptime='getline <"/proc/uptime" ; jif_per_sec = idle_jif / $2 ; up_dur = $1 - uptime_base'
    # if idle time is less than 5% (1/20) of uptime, print busy jiffies over duration
    local act_print_limit='if (idle_jif - idle_base < up_dur * jif_per_sec / 20) print busy_jif - busy_base'
    awk -v uptime_base="${uptime_base}" \
	-v busy_base="${sys_busy_time}" \
	-v idle_base="${sys_idle_time}" \
	'/^cpu\s/ { '"${act_get_cpu_times} ; ${act_get_uptime} ; ${act_print_limit} ; exit"' }' "/proc/stat"
}
check_nlbwmon_hog_usage() {
    local act_get_portion='nlbwmon_portion = ($11 + $12 - nlbwmon_utime_base) / limit_jiffies'
    # limit_jiffies = busy CPU jiffies
    # if nlbwmon takes 90%+ (override with 4th parameter) of busy CPU,
    # awk will exit with false/0, which is true in shell
    get_nlbwmon_stat "${1}" | awk \
        -v nlbwmon_utime_base="${2}" \
        -v limit_jiffies="${3}" \
	-v limit_portion="${4:-0.9}" \
	"{ ${act_get_portion} ; print nlbwmon_portion ; exit (nlbwmon_portion < limit_portion) }"
}

# initial setup: find nlbwmon stats
nlbwmon_stat="$( find_nlbwmon_proc )/stat"
[ "${nlbwmon_stat}!" != "/stat!" ] || error "No nlbwmon process found."

# read baseline stats
set_baseline
nlbwmon_time_base="$( get_nlbwmon_stat "${nlbwmon_stat}" | awk '{ print $11 + $12 }' )"

# wait a bit to see usage
sleep 3

[ -e "${nlbwmon_stat}" ] || error "Can't get new stats."
# jiffies since baseline uptime
limit_jiffies="$( get_cpu_time_limit )"
if [ -n "${limit_jiffies}" ] && nlbwmon_portion="$( check_nlbwmon_hog_usage "${nlbwmon_stat}" "${nlbwmon_time_base}" "${limit_jiffies}" )" ; then
    # if we have number of jiffies since baseline, system is busy
    # determine whether it's nlbwmon, restarting if it is
    logger -t "$( basename "${0}" )" "Restarting nlbwmon, which is using ${nlbwmon_portion} of CPU."
    "${nlbwmon_init}" restart
fi
