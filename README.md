
## Introduction

Cue the Tiny Toon Adventures opening!

```
It's tiny; it's tool-y; will fit where it's less roomy:
It does things a bit quickly that'd otherwise be crazy!
It's mostly -- a specialty; designed especially:
Some of them would even run on OpenWRT!
Practically public domain that license CC0:
Feel free to use in any way; there's nothing you would owe!
It's expected; part tested; but hope it cuts time wasted;
The research is documented with the code!

Just quirky; and dodgy; it sometimes could be kludge-y;
It doesn't necessarily read that intuitively!
Debug it -- it's easy; turn up the verbosity;
Tweak it to your liking; couldn't go more simpl-y!
When it doesn't do what you want, it's not like you are tied;
No lines too sly, they are described, there's nothing they would hide!
It's tiny; it's tool-y; will fit where it's less roomy:
It's time to try; don't even bother asking why!

Don't you dare toss it aside!
```

Horribly mangled; not as smooth as the original, but sure was fun.  ^_^;

A bit more serious...  this is a collection of scripts and configurations to be used with a variety of software.  Some of them are special re-implementations of \[portions of\] certain software, to allow for quick customizations and to cut down on unnecessary features \[and thus size\].

## Sample Listing

This is in no way a full list, but a quick sampling of what you can find here.

- File utilities
   - de-duplication
      - paranoid full binary comparison
      - parallel tree duplicate tagging
      - duplicate tagging of files extracted from zip archives (along lines of zcmp)
   - copy with hard links
- Tweaks
   - configurations
   - initramfs for certain Linux live CD setups
- VM
   - startup scripts
   - DMI copy (allow license from physical machine to be used on VM)
- OpenWRT
   - hacks and workarounds
   - Firefox add-on blocklist mangling
      - for full blocklist only
      - [FF77 and older](https://blog.mozilla.org/addons/2020/08/24/introducing-a-scalable-add-ons-blocklist/)

## Notes

Aims to have scripts running on as many systems as possible.
One of the ways is to keep shell scripts `[d]ash`-compatible (required anyway for OpenWRT which uses `busybox` `ash`).

## Licensing

CC0, with possible exceptions for code adapted from various sources.  Exceptions will be minimal.
