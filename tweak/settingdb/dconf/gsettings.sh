#!/bin/sh
### WHAT THIS DOES #####################
# Parses a gsettings entry list, in the format is produced by
#   gsettings list-recursively
# and applies one of several actions specified as the parameter of this script,
# Meant to allow saving gsettings entries that matter instead of copying the
# entire underlying database and carrying over environment-specific or temporary stuff.
### NOTES ##############################
# Sometimes things don't have a schema, so wouldn't show up in gsettings.
# MATE panel is one example.  Need to directly access backend:
#   dconf dump /org/mate/panel/
# and restore with dconf's load command in order to cover the item settings.
# This is primarily intended to be run on live CD systems.
### HOWTO ##############################
# 1) Acquire gsettings entries
#   a) Run 'gsettings list-recursively'
#   b) Filter entries that are important
#     - can diff the same output from a fresh user to see defaults
#   c) Pass into script
#     *) as filename in 2nd parameter
#       - '-' means use stdin so it can be piped in
#     *) embed into script
#       - set_embed_conf function should assign embedded input as fd 3
#         - See set_embed_conf_mint_mate for example of how
#       - included embedded config dynamically sets up entries based on system
#         - Linux Mint 18.3 Cinnamon edition
#         - Linux Mint 18.3 MATE edition
#         - Knoppix 7.6.0
# 2) Run script with "showdiff" as parameter (interactive only)
#   - to see if the changes make sense
# 3) Run script with "update" as parameter
#   - to actually apply the changes
#   - default if not detected as interactive
# Note: Backup the original values; this script will just overwrite.
# Can save output to have something to recover with but it's not guaranteed.
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

dump_embed_conf_app_xed() {
    # Text Editor
    echo \
"org.x.editor.plugins active-plugins ['textsize', 'sort', 'modelines', 'filebrowser', 'docinfo']
org.x.editor.preferences.editor wrap-mode 'none'
org.x.editor.preferences.editor display-right-margin true
org.x.editor.preferences.editor right-margin-position uint32 80
org.x.editor.preferences.editor highlight-current-line true
org.x.editor.preferences.editor bracket-matching true
org.x.editor.preferences.editor display-line-numbers true
org.x.editor.preferences.editor prefer-dark-theme true
org.x.editor.preferences.editor scheme 'cobalt'
org.x.editor.preferences.ui minimap-visible true"
}

dump_embed_conf_app_gedit() {
    echo \
"org.gnome.gedit.plugins active-plugins ['modelines', 'filebrowser', 'docinfo']
org.gnome.gedit.preferences.editor wrap-mode 'none'
org.gnome.gedit.preferences.editor display-right-margin true
org.gnome.gedit.preferences.editor highlight-current-line true
org.gnome.gedit.preferences.editor display-line-numbers true
org.gnome.gedit.preferences.editor bracket-matching true
org.gnome.gedit.preferences.editor scheme 'cobalt'
org.gnome.gedit.preferences.editor display-overview-map true"
}

dump_embed_conf_app_cinnamon_nemo() {
    # File Manager: Nemo
    echo \
"org.nemo.preferences default-folder-viewer 'list-view'
org.nemo.preferences date-format 'iso'
org.nemo.preferences ignore-view-metadata true
org.nemo.preferences show-reload-icon-toolbar true
org.nemo.preferences size-prefixes 'base-2'
org.nemo.preferences show-search-icon-toolbar false
org.nemo.preferences show-open-in-terminal-toolbar true
org.nemo.preferences show-full-path-titles true
org.nemo.list-view default-visible-columns ['date_modified', 'size', 'name', 'type']
org.nemo.list-view default-column-order ['octal_permissions', 'permissions', 'owner', 'group', 'date_accessed', 'date_modified', 'date_modified_with_time', 'size', 'name', 'where', 'type', 'detailed_type', 'mime_type']
org.nemo.plugins disabled-actions ['mintstick.nemo_action', 'change-background.nemo_action', 'send-by-mail.nemo_action', 'set-as-background.nemo_action', 'mintstick-format.nemo_action']
org.nemo.plugins disabled-extensions ['EmblemPropertyPage+NemoPython', 'ChangeColorFolder+NemoPython', 'NemoShare']"
}

dump_embed_conf_app_mate_caja() {
    # File Manager: Caja
    echo \
"org.mate.caja.desktop home-icon-visible false
org.mate.caja.extensions disabled-extensions ['libcaja-wallpaper', 'libcaja-sendto']
org.mate.caja.list-view default-visible-columns ['date_modified', 'size', 'name', 'type']
org.mate.caja.list-view default-column-order ['selinux_context', 'octal_permissions', 'permissions', 'owner', 'group', 'date_accessed', 'date_modified', 'size', 'name', 'where', 'type', 'mime_type']
org.mate.caja.preferences date-format 'iso'
org.mate.caja.preferences default-folder-viewer 'list-view'
org.mate.caja.preferences use-iec-units true"
}

get_embed_conf_app() {
    local app="${1}"
    local schema="${2}"
    if gsettings list-children "${schema}" >/dev/null 2>&1 ; then
        "dump_embed_conf_app_${app}"
    fi
}

get_embed_conf_cinnamon_bg() {
    local imgpath="${1}"
    if [ -r "${imgpath}" ] ; then
        echo "org.cinnamon.desktop.background picture-uri 'file://${imgpath}'"
    fi
}

set_embed_conf_mint_x_cinnamon() {
    exec 3<<- !GSETTINGS_ENTRIES!
$( get_embed_conf_cinnamon_bg "/usr/share/backgrounds/linuxmint/edesigner_linuxmint.png" )
org.cinnamon.desktop.wm.preferences theme 'Mint-Y-Dark'
org.cinnamon.desktop.media-handling automount-open false
org.cinnamon.desktop.media-handling automount false
org.cinnamon.desktop.media-handling autorun-never true
org.cinnamon.desktop.interface icon-theme 'Mint-Y'
org.cinnamon.desktop.interface gtk-theme 'Mint-Y-Dark'
org.cinnamon.settings-daemon.plugins.power lid-close-ac-action 'nothing'
org.cinnamon.settings-daemon.plugins.power idle-brightness 5
org.cinnamon.settings-daemon.plugins.power lid-close-battery-action 'nothing'
org.cinnamon.settings-daemon.plugins.power sleep-display-battery 300
org.cinnamon.settings-daemon.plugins.power sleep-display-ac 600
org.cinnamon.settings-daemon.peripherals.touchpad tap-to-click false
org.cinnamon.settings-daemon.peripherals.mouse middle-button-enabled false
org.cinnamon.theme name 'Mint-Y-Dark'
org.gnome.desktop.screensaver idle-activation-enabled false
org.gnome.desktop.screensaver lock-enabled false
$( get_embed_conf_app cinnamon_nemo org.nemo.preferences )
$( get_embed_conf_app xed org.x.editor.preferences.editor )
!GSETTINGS_ENTRIES!
}

get_embed_conf_mate_bg() {
    local imgpath="${1}"
    if [ -r "${imgpath}" ] ; then
        echo "org.mate.background picture-filename '${imgpath}'"
    fi
}

set_embed_conf_mint_mate() {
#org.mate.panel object-id-list ['menu-bar', 'show-desktop', 'separator', 'launcher-caja', 'launcher-terminal', 'launcher-firefox', 'window-list', 'notification-area', 'clock', 'object-1', 'object-2']
    exec 3<<- !GSETTINGS_ENTRIES!
$( get_embed_conf_mate_bg "/usr/share/backgrounds/linuxmint/edesigner_linuxmint.png" )
com.linuxmint.mintmenu.plugins.places custom-names ['ISO Device']
com.linuxmint.mintmenu.plugins.places custom-paths ['/isodevice']
com.linuxmint.mintmenu.plugins.places show-home-folder false
com.linuxmint.mintmenu.plugins.places show-desktop false
com.linuxmint.mintmenu.plugins.applications enable-internet-search false
com.linuxmint.mintmenu.plugins.applications last-active-tab 1
com.linuxmint.mintmenu.plugins.applications use-apt false
org.mate.engrampa.listing show-path false
org.mate.interface gtk-theme 'Mint-Y-Dark'
org.mate.interface icon-theme 'Mint-Y'
org.mate.Marco.general theme 'Mint-Y-Dark'
org.mate.Marco.general num-workspaces 2
org.mate.Marco.general compositing-fast-alt-tab true
org.mate.media-handling automount-open false
org.mate.media-handling autorun-never true
org.mate.peripherals-mouse cursor-size 24
org.mate.peripherals-mouse cursor-theme 'DMZ-Black'
org.mate.peripherals-mouse middle-button-enabled false
org.mate.peripherals-touchpad disable-while-typing true
org.mate.peripherals-touchpad motion-acceleration 3.179930795847751
org.mate.peripherals-touchpad tap-to-click false
org.mate.power-manager brightness-ac 10.0
org.mate.power-manager idle-dim-ac true
org.mate.power-manager sleep-display-ac 600
org.mate.power-manager button-lid-battery 'blank'
org.mate.power-manager button-lid-ac 'blank'
org.mate.screensaver idle-activation-enabled false
org.mate.screensaver lock-enabled false
org.mate.session.required-components windowmanager 'mint-window-manager'
$( get_embed_conf_app mate_caja org.mate.caja.preferences )
$( get_embed_conf_app xed org.x.editor.preferences.editor )
!GSETTINGS_ENTRIES!
}

set_embed_conf_mint_xfce() {
    exec 3<<- !GSETTINGS_ENTRIES!
$( get_embed_conf_app xed org.x.editor.preferences.editor )
$( get_embed_conf_app gedit org.gnome.gedit.preferences.editor )
!GSETTINGS_ENTRIES!
}

set_embed_conf_knoppix() {
    exec 3<<- !GSETTINGS_ENTRIES!
$( get_embed_conf_app gedit org.gnome.gedit.preferences.editor )
!GSETTINGS_ENTRIES!
}

get_knoppix_dir() {
    for c in "$@" ; do
        case $c in
           knoppix_dir=*)
               echo ${c#knoppix_dir=}
               return
               ;;
        esac
    done
    echo "KNOPPIX"
}

get_unique_known_session() {
    local sessUnique=""
    for sess in cinnamon mate xfce ; do
        if [ -e "/usr/share/xsessions/${sess}.desktop" ] ; then
            if [ -z "${sessUnique}" ] ; then
                sessUnique="${sess}"
            else
                sessUnique=""
                break
            fi
        fi
    done
    echo "${sessUnique}"
}

set_embed_conf() {
    local variant=unknown
    if grep -q '^NAME="\(LMDE\|Linux Mint\)"$' "/usr/lib/os-release" ; then
        # Mint 18.3, LMDE4
        # https://tecadmin.net/detect-the-desktop-environment-in-linux-command-line/
        # Mint has several desktop environment variants
        if [ -n "${XDG_CURRENT_DESKTOP}" ] ; then
            variant="mint_$( echo ${XDG_CURRENT_DESKTOP} | tr '[A-Z]-' '[a-z]_' )"
        else
            # Unfortunately, this breaks when run outside the GUI
            # or when run by a different user (sharing the X display) (e.g. root shell)
            # Check installed sessions and try to guess
            # https://wiki.ubuntu.com/LightDM#Changing_the_Default_Session
            # TODO figure out what's running on X display $DISPLAY
            # One ugly way would be to scan process tree to check for DE processes
            # https://unix.stackexchange.com/questions/506490/how-can-i-find-out-the-x-servers-command-or-pid-for-display-numbers
            local sessUnique="$( get_unique_known_session )"
            case "${sessUnique}" in
                cinnamon)
                    variant="mint_x_cinnamon"
                    ;;
                mate|xfce)
                    variant="mint_${sessUnique}"
                    ;;
                *)
                    echo "Unable to determine which Mint variant is in use."
                    ;;
            esac
        fi
    elif [ -e "/mnt-system/$( get_knoppix_dir $( cat /proc/cmdline ) )/knoppix-version" ] ; then
        # Knoppix 7.6.0
        variant=knoppix
    fi
    if [ "unknown!" = "${variant}!" ] ; then
        echo "Unable to determine which embedded config to use."
        false
    elif type "set_embed_conf_${variant}" >/dev/null 2>&1 ; then
        "set_embed_conf_${variant}" "$@"
    else
        echo "No embedded config for ${variant}."
        false
    fi
}

set_input_conf_fd3() {
    local input="${1}"
    # https://stackoverflow.com/questions/8756535/conditional-redirection-in-bash#8756552
    # use exec to set up new fd so it's no longer conditional at input
    if [ -z "${input}" ] ; then
        set_embed_conf
    elif [ "-!" = "${input}!" ] ; then
        exec 3<&0
    elif [ -r "${input}" ] ; then
        exec 3<"${input}"
    else
        echo "Unable to read ${input}."
        false
    fi
}

apply_gsettings_showtarget() {
	local schema="${1}"
	local key="${2}"
	local val="${3}"
	printf '%-30s %-20s : %s\n' "${schema}" "${key}" "${val}"
}
apply_gsettings_showdiff() {
	local schema="${1}"
	local key="${2}"
	local val="${3}"
	local curval="$( gsettings get "${schema}" "${key}" )"
	if [ "${val}!" != "${curval}!" ] ; then
		printf '%-30s %-20s\n\t%s -> %s\n' "${schema}" "${key}" "${curval}" "${val}"
	fi
}
apply_gsettings_update() {
	local schema="${1}"
	local key="${2}"
	local val="${3}"
	local curval="$( gsettings get "${schema}" "${key}" )"
	if [ "${val}!" = "${curval}!" ] ; then
		printf '%-30s %-20s : %s\n' "${schema}" "${key}" "${val}"
	else
		printf '%-30s %-20s\n\t%s -> %s\n' "${schema}" "${key}" "${curval}" "${val}"
		gsettings set "${schema}" "${key}" "${val}"
	fi
}
parse_gsettings() {
	[ -z "${1}" ] && return
	# lines beginning with # are comments (doesn't handle whitespace in front)
	[ "${1#'#'}!" != "${1}!" ] && return
	local val="${1#* * }"
	local nonval="${1%" ${val}"}"
	local schema="${nonval% *}"
	local key="${nonval#* }"
	"${apply_gsettings}" "${schema}" "${key}" "${val}"
}

if [ -n "${1}" ] ; then
	apply_gsettings="apply_gsettings_${1}"
elif [ -t 1 ] ; then
	apply_gsettings="apply_gsettings_showdiff"
else	# no output to show diff, so just update
	apply_gsettings="apply_gsettings_update"
fi
if ! type "${apply_gsettings}" >/dev/null 2>&1 ; then
	echo "Unknown command ${1}"
	echo "Usage: ${0} <Command> [<entry file>]"
	echo "Supported commands are:"
	echo " showtarget: shows entries in embedded config"
	echo " showdiff: shows entries that have different value than embedded config"
	echo " update: changes values to match embedded config"
	false
fi

set_input_conf_fd3 "${2}"

while IFS= read l ; do
	parse_gsettings "${l}"
done <&3
echo "Completed ${apply_gsettings#apply_gsettings_}."
