#!/bin/sh
### WHAT THIS DOES #####################
# Restore settings for MATE panel under Linux Mint 18.3.
### NOTES ##############################
# This really just calls 'dconf load', so could easily generate it yourself.
# Not really useful for anyone other than author, but serves as a hint of what to do.
### HOWTO ##############################
# 1) Extract dconf config
#    dconf dump /org/mate/panel/
# 2) Embed between !DCONF! markers
#   a) possibly change entries to customize further
# 3) Run to restore
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

dconf load /org/mate/panel/ <<- "!DCONF!"
[general]
object-id-list=['menu-bar', 'launcher-caja', 'launcher-terminal', 'launcher-firefox', 'window-list', 'separator-r', 'show-desktop', 'ws-switcher', 'sys-monitor', 'notification-area', 'clock']
toplevel-id-list=['bottom']

[toplevels/bottom]
expand=true
orientation='bottom'
screen=0
y-bottom=0
size=24
y=744

[objects/menu-bar]
applet-iid='MintMenuAppletFactory::MintMenuApplet'
object-type='applet'
locked=true
position=0
toplevel-id='bottom'

[objects/launcher-caja]
launcher-location='/usr/share/applications/caja-browser.desktop'
menu-path='applications:/'
object-type='launcher'
locked=true
position=41
toplevel-id='bottom'

[objects/launcher-terminal]
launcher-location='/usr/share/applications/mate-terminal.desktop'
menu-path='applications:/'
locked=true
position=42
object-type='launcher'
toplevel-id='bottom'

[objects/launcher-firefox]
launcher-location='/usr/share/applications/firefox.desktop'
menu-path='applications:/'
object-type='launcher'
locked=true
position=43
toplevel-id='bottom'

[objects/window-list]
applet-iid='WnckletFactory::WindowListApplet'
object-type='applet'
locked=true
position=50
toplevel-id='bottom'

[objects/window-list/prefs]
group-windows='auto'

[objects/separator]
object-type='separator'
locked=true
position=50
panel-right-stick=true
toplevel-id='bottom'

[objects/notification-area]
applet-iid='NotificationAreaAppletFactory::NotificationArea'
object-type='applet'
locked=true
position=30
panel-right-stick=true
toplevel-id='bottom'

[objects/sys-monitor]
applet-iid='MultiLoadAppletFactory::MultiLoadApplet'
object-type='applet'
locked=true
position=20
panel-right-stick=true
toplevel-id='bottom'

[objects/sys-monitor/prefs]
cpuload-color0='rgb(115,210,22)'
cpuload-color1='rgb(237,212,0)'
cpuload-color2='rgb(117,80,123)'
cpuload-color3='rgb(164,0,0)'
speed=1000
size=35
diskload-color0='rgb(115,210,22)'
view-diskload=true

[objects/show-desktop]
applet-iid='WnckletFactory::ShowDesktopApplet'
object-type='applet'
locked=true
position=15
panel-right-stick=true
toplevel-id='bottom'

[objects/ws-switcher]
applet-iid='WnckletFactory::WorkspaceSwitcherApplet'
object-type='applet'
locked=true
position=10
panel-right-stick=true
toplevel-id='bottom'

[objects/clock]
applet-iid='ClockAppletFactory::ClockApplet'
object-type='applet'
locked=true
position=0
panel-right-stick=true
toplevel-id='bottom'

[objects/clock/prefs]
show-date=false
show-weather=false
show-temperature=false
format='24-hour'
custom-format=''
!DCONF!
