## Linux tweaks

Various tweaks for Linux systems.

### `initramfs`

Tweaks to the initramfs.

### system tweaks

The following are system tweaks:
- openwrtfs
   - Tweaks for OpenWRT
- centosfs
   - Tweaks for CentOS
- knoppixfs
   - Tweaks for Knoppix
   - to be placed in persistence
      - knoppix-data.img or the like

Files are laid out as they would be on the system.
