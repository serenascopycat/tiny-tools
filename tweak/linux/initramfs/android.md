## Modifications over Android x86

Additional changes over Android x86 boot.

- Allow mounting of partitions via UUID or label for data
- Allow loop mounting of image file for data

## How to use

Add to the `initrd.img` used to boot up Android x86.

See scripts for additional usage instructions.

See [README](./README.md#adding-the-initramfs-archive) for information regarding addition of the generated initramfs archive.
