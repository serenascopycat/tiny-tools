## Knoppix persistence data tweak

Knoppix assumes that the only persistence is one that lives with the running medium.
Unfortunately, when the media is the disc (ISO or as an [emulated] disc in VM), the default settings make it near impossible to use only boot options.

Note that there is no directory for this, as it's all setup-specific configuration.

### Using additional mounts on Knoppix 7.6

Knoppix `init` focuses on mounting `mnt-system` and mounts leading to it;
doesn't really get any other mounts ready.
Tricky to get data persistence when it doesn't live with the Knoppix distribution data
(i.e. when booting off ISO or an [actual / emulated] DVD drive).
Idea is to inject files into the initramfs to get enough to get the mounts ready
before it gets to do the overlays.

1) Inject into initramfs by creating cpio with:
   1) a symbolic link in `/dev/bootdevwithoutnumberthenaddsuffix` back to `/dev`
      - required by `.inf` in next step
      - `bootdevwithoutnumberthenaddsuffix` is
         - boot device
         - number suffix removed
         - suffix of your choice appended
      - if the boot device is `/dev/sr0`, can be `/dev/srdevs`
   1) an `.inf` file (in fstab-like format) that lists the partitions to mount
      - normally this file lives with the Knoppix distribution when on a USB drive
      - "partition to mount" field needs special hack value
         - see notes below
1) Add a `home=/path/to/generated` boot option
   - adjust `/path/to/generated` to the location of the injected file in initramfs
   - note the lack of `.inf` suffix
      - `mountdata()` will add it

Sample `.inf` file to mount `/dev/sda2` (`/dev/disk/by-uuid` not available in initramfs) for step 2:

```
devs/sda2	/KNOPPIX-DATA	ext2	
```

When booting off `/dev/sr0` (e.g. emulated DVD in a VM),
the symbolic to create in step 1 should be `/dev/srdevs`
for the corresponding `.inf` file above, so that
`/init` would try to mount `/dev/srdevs/sda2`,
where `/dev/srdevs` is the symbolic link hack that points back to `/dev`.

To boot, place `.inf` into initramfs as `/etc/knoppix-data.inf`,
link `/dev/srdevs` to `/dev` in initramfs,
and add `home=/etc/knoppix-data` as a boot option.

`/init` handling of this file in `mountdata()` in Knoppix 7.6:

```sh
    inf) # Contains partition information in fstab format
     local part="" mp="" fs="" opts=""
     while read part mp fs opts; do
      [ -n "$part" ] || continue
      case "$mp" in *[Hh][Oo][Mm][Ee]) mp="/KNOPPIX-DATA" ;; esac
      case "$opts" in
       *aes*) mountaes "${ROOTDEV%[0-9]*}${part#/dev/}" "${mp:-/KNOPPIX-DATA}" "${fs:-ext2}" "-o $rw" && return 0 ;;
       *)     checkfs "${ROOTDEV%[0-9]*}${part#/dev/}" "$fs"
              message -e "\r${CRE}${GREEN}${USING} ${YELLOW}${ROOTDEV%[0-9]*}${part#/dev/} ($rw)${NORMAL}"
              mount -t "${fs:-ext2}" -o $rw "${ROOTDEV%[0-9]*}${part#/dev/}" "${mp:-/KNOPPIX-DATA}" && return 0 ;;
      esac
     done <"$img"
    ;;
```

The `.inf` handling code assumes that the "partition to mount" is a partition number only instead of full device reference (makes sense in normal use case where the USB device may move around).
[Post](http://knoppix.net/forum/threads/31406-What-codes-can-be-passed-on-to-Knoppix-during-boot-time-by-knoppix-data-inf?p=132309&viewfull=1#post132309) hints that `/usr/bin/flash-knoppix` generates this file.

Step 1 serves as a workaround to provide a symbolic link that is compatible with the path that the `.inf` file handling will come up with.

### Using persistence on Knoppix 9.1 ISO boot

`/init` on 9.1 contains a `check_data_partition()` function which gets triggered by the default `/mnt-system/KNOPPIX/knoppix-data.inf`,
which points to a non-existent device `3` (because, for ISO boot, ROOTDEV is not set).
`check_data_partition()` will see that and flag that the persistence is unusable, causing it to skip `mountdata`.

Here is a workaround for using a `.img` file so there is no need to touch the ISO:

1) Inject into initramfs by creating cpio with:
   1) a dummy node
      - create with `mknod 800 b 0x7F 0xFFFFF`
         - `800` is a positive numeric-only name
            - `check_data_partition()` requires this to be positive numeric partition number
            - may be `3` to match the default name
         - `b` is block device
            - `check_data_partition()` requires a block device for the partition
         - MAJOR `0x7F` and MINOR `0xFFFFF` of your choice
            - Recommend to use numbers that don't conflict with existing nodes
            - see `/proc/devices` for list of major numbers in use
            - Major ranges 120-127 and 240-254 are allocated as local/experimental
               - [Unix & Linux StackExchange question](https://unix.stackexchange.com/questions/73988/linux-major-and-minor-device-numbers#495999)
                  - [Mirror of LANANA allocated devices list](http://mirrors.mit.edu/kernel/linux/docs/lanana/device-list/devices-2.6.txt)
      - must live in `/`
         - because ROOTDEV is not set for ISO boot
      - required by `.inf` in next step
   1) an `.inf` file (in fstab-like format) that lists the partitions to mount
      - normally this file lives with the Knoppix distribution when on a USB drive
      - make dummy entry
         - must be before other entries that define `/KNOPPIX-DATA` mount points
            - `check_data_partition()` just checks the first one
         - "partition to mount" field needs to be name of dummy node
         - "mount point" field must be either `/KNOPPIX-DATA` or variants of `home`
            - `check_data_partition()` only checks for `/KNOPPIX-DATA`
         - "fs type" field should avoid reiserfs
            - prevents `check_data_partition()` from attempting an fs resize check
         - this file doesn't need to be injected and may live outside initramfs
            - the `.img` file needs to be in the same place
1) Add a `home=/path/to/generated` boot option
   - adjust `/path/to/generated` to the location of the injected file in initramfs
   - note the lack of `.inf` suffix
      - `mountdata()` will add it

What the above steps does is to cause `check_data_partition()` to see an earlier `.inf` file that would pass its checks.
One of the checks is for read-only, and it happens to fail when trying to check `/sys/block//ro` because we pass 800,
which, after stripping the last 3 digits, causes it to be a blank name so it checks `/sys/block/"$disk"/ro`.
(Theoretically any name should work as they're required to be numeric,
and there is no /sys/block subdirectory that is numeric-only.)
This inf will get used in `mountdata()`, but only after a `.img` is mounted first, as it will first try files `$home`, in order of `.aes`, `.img`, `.inf`.
The process will error out when it tries to do the dummy `.inf`, but it does get /KNOPPIX-DATA mounted so we have [read-only] persistence.
