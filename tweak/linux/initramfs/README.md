## `initramfs` Modifications

Sets of files to add over existing `initramfs`'s to modify their behavior.

Each set is for a specific `initramfs`.  See corresponding Markdown files to see what it's for.

Use `mkinitmod.sh` to generate the `cpio` archive which can then be specified as part of `initrd` parameter or appended to existing `initramfs` file.

## Adding the initramfs archive

There are several ways of including an `initramfs` additions archive:
- Repacking
   - lots of work
   - not maintainable
   - easy to understand
   - required if the original is an initrd image
      - i.e. an `ext` filesystem image
      - rather than `cpio` archives used to populate initramfs
   1) extract the original
   1) add the files within the generated additions archive
      - or just add the files from source directory
   1) regenerate archive or filesystem
   1) use in place of original
- Concatenating
   - use for QEMU
      - each `-initrd` parameter completely overrides prior one(s)
      - can someone figure out how to use generic loader?
         - [see QEMU initrd usage below](#qemu-initrd-usage) for link hinting at the use of loader
         - how to specify new initrd size after loading the archive as addition?
   1) pad original file until it is [aligned](#cpio-archive-alignment)
   1) append archive to original
   1) use appended file in place of original
- Addition to `initrd` parameter
   - no extra work required when regenerating archive
      - except possibly needing alignment for GRUB 2.04 [^1]
   - only available for certain bootloaders, such as
      - SYSLINUX
      - GRUB2
   1) specify additional file to `initrd`

### GRUB2 initrd usage

Just add as another parameter to `initrd` command.

e.g. if the original was:

```
	initrd /casper/initrd.lz
```

then could change to:

```
	initrd /casper/initrd.lz casper+.cpio
```

Adjust path to `casper+.cpio` as necessary.

See [example in `initrd_casperplus()`](https://gitlab.com/serenascopycat/grub-config/-/tree/main/grub2/conf.d/livefunc.cfg)

Note: manual alignment may be required depending on the version of GRUB [^1].

[^1]: GRUB 2.04 may not add the files correctly, meaning the kernel will not add the `cpio` contents at all for files after one that is not padded correctly.  See [`cpio` archive alignment](#cpio-archive-alignment).  GRUB 2.03 appears to work fine even after a compressed `cpio` that has an odd number of bytes. 

### QEMU initrd usage

Instead of booting the CD by specifying the `-cdrom` and letting it run through the boot sequence (isolinux), can specify the kernel, initrd/initramfs and boot parameters directly and let QEMU directly start the kernel.
See the `-kernel`, `-initrd` and `-append` parameters for QEMU.  Can use them by first mounting the ISO image, then specifying those parameters, pointing them at the files within the mounted ISO file.  See the isolinux config file in the ISO to see which files and parameters to use.

The initramfs is a \[list of concatenated\] `cpio` archive(s).  As long as they're [aligned correctly](#cpio-archive-alignment), the kernel should be able to see all the concatenated archives and load them all in turn.
Typically, `cpio` archives are created in blocks, so should already be aligned.  However, the ones that are in the ISO image may be compressed, so may need to pad them before concatenating.

See [example in `optlive_mkcasperplus()`](vm/qemu/startlib/tmpllive.sh) used in [live CD example](vm/qemu/example/livecd_basic/mint_mate-casper+.sh).

### `cpio` archive alignment

The `cpio` files should be minimally aligned to 4 bytes ([seen in 3rd comment in request](https://bugs.launchpad.net/qemu/+bug/393569)?).  It may not work, and may need to be 8-byte for 64-bit kernel.  (TODO: find documentation.)

Recommended to align to 512 bytes, which happens to be how `cpio` files are generated with.  Other software, such as SYSLINUX, will align to 4096 bytes.  In any case, these higher alignment sizes are compatible with either the 4-byte or 8-byte alignment requirement.

## Including files from filesystem

GRUB2 and SYSLINUX both support on-the-fly `cpio` generation, meaning they can take a file in the filesystem, and "put it into `initrd` via a `cpio`".

### SYSLINUX `initrdfile=`

SYSLINUX has [initrdfile=](https://wiki.syslinux.org/wiki/index.php?title=Linux.c32#initrdfile.3D) configuration option.

### GRUB2 `initrd newc:`

An \[undocumented?\] feature implemented in [commit 92750e4c6019989151b675d1feadba100faba19d](https://git.savannah.gnu.org/cgit/grub.git/commit/grub-core/loader/linux.c?id=92750e4c6019989151b675d1feadba100faba19d) (available in [GRUB 2.02-beta1](https://git.savannah.gnu.org/cgit/grub.git/tag/?h=grub-2.02-beta1)) allows for:

```
initrd newc:path/in/initramfs:(hd0,msdos1)/path/in/filesys
```

to put `/path/in/filesys` in the first partition of the first drive during legacy (BIOS) boot into `initramfs` at `/path/in/initramfs`.  The leading slashes (`/`) are ignored in the initramfs path, so `///path/in/initramfs` yields the same effect.

`initrd` may have multiple arguments, some of which may be `newc:`, so could include an original `initrd` file, then use a few `newc:` ones to tack on a few other files, then add on yet another `cpio` archive.  Directories are automatically added into the `cpio` archive for `newc:` entries, so only the file(s) need to be specified.  Adjacent `newc:` ones would all be placed into a single `cpio` archive.
