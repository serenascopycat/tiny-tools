## Modifications over `casper`

Adds a `casper+` script which advises (wraps around) the existing `casper` script, modifying initramfs before `casper` functions get called.

- Prevent clobbering of fstab
- Disable hardware RTC update
  - by disabling ntp service

## How to use

Replace `boot=casper` boot parameter with `boot=casper+`, along with adding the initramfs `.cpio` archive on top of the original one.

Tip: add `break=premount` (or just `break` as it's a synonym) to spawn a shell during init, before the casper+ script gets sourced.
Look in `init` for the `maybe_break` calls to see what breaks can be set.
The new `casper+` script introduces a new shell break point that can be triggered by `break=casper+`.  It breaks after the normal `casper` script but before the other scripts that `casper+` drags in (i.e. before `casper+` does its thing).

See [README](./README.md#adding-the-initramfs-archive) for information regarding addition of the generated initramfs archive.
