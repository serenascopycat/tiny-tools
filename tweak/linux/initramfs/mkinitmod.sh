#!/bin/sh
### WHAT THIS DOES #####################
# Creates 'cpio' archives for use as initramfs with Linux bootloaders
# from '*.d' directories in the same directory as this script.
### HOWTO ##############################
# 1) Ensure this script has sibling '*.d' directories
#   - e.g. example.d/scripts/ex.sh will land in example.cpio
#     - containing /scripts/ex.sh
# 2) Run this script
#   - output will be generated in the same directory as this script
#   - warning: existing .cpio files will be clobbered without warning
### LICENSING ##########################
# Copycenter 2021 Serena's Copycat; licensed under CC0

set -e

for d in ./*.d ; do
	if [ -d "${d}" ] ; then
		target_file="$( cd "$( dirname "${d}" )" ; pwd )/$( basename "${d}" ".d" ).cpio"
		# Build initramfs with files owned by root
		# https://stackoverflow.com/questions/33241022/change-ownership-in-cpio-archive-without-root
		( cd "${d}" ; find | fakeroot cpio -o -H newc --file="${target_file}" )
	fi
done

