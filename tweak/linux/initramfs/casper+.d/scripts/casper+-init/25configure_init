#!/bin/sh

# Advises 25configure_init to disable ntp.
# The original 25configure_init tries to prevent RTC clobbering but doesn't quite do it.
# Copycenter 2021 Serena's Copycat; licensed under CC0

orig_path="/scripts/casper-bottom/25configure_init"

if [ -f "${orig_path}" ] ; then
	mv "${orig_path}" "${orig_path}.orig"
	# generate script (with substitutions)
	cat >"${orig_path}" <<- EOF
		#!/bin/sh

		ORIG_SCRIPT="${orig_path}.orig"
	EOF
	# generate script (literal only)
	cat >>"${orig_path}" << "EOF"
SYSTEMD_PATH="/root/etc/systemd/system/multi-user.target.wants/ntp.service"
HWCLOCK_DEFAULTS_PATH="/root/etc/default/hwclock"

# must run separately because of exit call
sh "${ORIG_SCRIPT}"

if [ -L "${SYSTEMD_PATH}" ] ; then
	# manually 'systemctl disable ntp' via the filesystem
	rm "${SYSTEMD_PATH}"
fi

if [ ! -e "${HWCLOCK_DEFAULTS_PATH}" ] || ! grep -q -x -F 'HWCLOCKACCESS=no' "${HWCLOCK_DEFAULTS_PATH}" ; then
	cat >>"${HWCLOCK_DEFAULTS_PATH}" <<- "HWCLOCKEOF"
		# don't touch hardware clock
		# live system not installed so not tied to hardware
		HWCLOCKACCESS=no
	HWCLOCKEOF
fi
EOF
	chmod a+x "${orig_path}"
fi

