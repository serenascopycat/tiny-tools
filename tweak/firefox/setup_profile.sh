# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Creates profile and customizes prefs.js.
# Firefox likes to pop privacy notice for new profiles,
# which is every time after starting a fresh live CD run.
# Don't want to burn network bandwidth on it repeatedly.
# Also, Linux Mint likes to rub in the fact that
# the old live CD is unsupported.
### HOWTO ##############################
# 1) Source this file
# 2) Define callback with desired preferences
#  - e.g. source prefs.sh
# 3) Call ffprfl_prepareFFProfile() with parameters
#  1) profile name
#  2) callback command
#   - should take the following parameters:
#    1) isProfileNew
#       non-empty when profile is just created
#       empty if already existing
#    2) profileName
#       profile name passed to ffprfl_prepareFFProfile()
#    3) profileDir
#       path to profile directory
#    4+) additional parameters
#  3+) additional parameters to callback command
# Examples at end of file
### LICENSING ##########################
# Copycenter 2022 Serena's Copycat; licensed under CC0

# set -e

profileParentDir=/tmp
profileNamePrefix="Firefox_"

# process a list of preference names (and optionally values)
# with the specified callback command
# arguments are: callbackCmd defaultVal prefs
# where prefs is a list of preference names;
# each preference name may be followed by an equal sign
# and the value for that preference, overriding defaultVal
# e.g.
#   ffprfl_forEachPref writePref '"testVal"' \
#     pref.one pref.two = '"prefTwo"' pref.three
# would make these calls:
#   writePref pref.one '"testVal"'
#   writePref pref.two '"prefTwo"'
#   writePref pref.three '"testVal"'
ffprfl_forEachPref() {
    local callbackCmd="${1}" defaultVal="${2}"
    shift 2
    while [ 1 -le "$#" ] ; do
        if [ 3 -le "$#" ] && [ '=' = "${2}" ] ; then
            "${callbackCmd}" "${1}" "${3}" || :
            shift 3
        else
            "${callbackCmd}" "${1}" "${defaultVal}" || :
            shift
        fi
    done
}

ffprfl_overridePrefs() {
    local profileDir="${1}"
    local forPrefs="${2:-ffprfl_doPrefs}"
    local prefsJSPath="${3:-"${profileDir}/prefs.js"}"
    local prefNames="" prefOut=""
    ffprfl_overridePrefs_setPref() {
        [ -n "${prefNames}" ] && prefNames="${prefNames}\|"
        prefNames="${prefNames}$( echo "$1" | sed 's/\([.^$]\|\[\|\]\)/\\\1/g' )"
        [ -n "${prefOut}" ] && prefOut="${prefOut}\n"
        prefOut="${prefOut}user_pref(\"$1\", $2);"
    }
    "${forPrefs}" ffprfl_overridePrefs_setPref

    local prefsJSPathWIP="$( mktemp --suffix=".js" "${profileDir}/prefs_XXXXXX" )"
    {   # drop existing preferences that we're about to override
        [ -e "${prefsJSPath}" ] && grep -v '^user_pref("\('"${prefNames}"'\)", \("\(\\.\|[^\\"]\)*"\|[^"]*\)\+);$' "${prefsJSPath}" ;
        # include the desired preferences
        echo ${prefOut}
    } > "${prefsJSPathWIP}" && \
        cat "${prefsJSPathWIP}" > "${prefsJSPath}"
    rm "${prefsJSPathWIP}"
}

# Starts Firefox with profile
# Arguments should be:
#  1) non-empty to indicate the profile is newly created (and needs to be modified)
#  2) profile name
#  3) path to profile
# Environment/shell variables
#  - persistentProfile
#    keep the profile after the script is done
#  - startFFCmd=[command]
#    command to start Firefox
#    defaults to internal command when not specified
#  - startURL=https://example.com/
#    start Firefox with specific URL
#    used by internal command to start Firefox
ffprfl_startWithProfile() {
    local isProfileNew="${1}"
    local profileName="${2}"
    local profileDir="${3}"
    shift 3

    if [ -z "${startFFCmd}" ] ; then
        ffprfl_startFF() {
            # when startURL is set, start with that page in private browsing
            # don't start in private browsing otherwise because it will load "about private browsing" page instead of home page
            firefox ${startURL:+-private} "$@" ${startURL:+"${startURL}"}
        }
        startFFCmd=ffprfl_startFF
    fi

    if [ -n "${isProfileNew}" ] ; then
        ffprfl_overridePrefs "${profileDir}" "$@"
        "${startFFCmd}" -profile "${profileDir}"
        [ -n "${persistentProfile}" ] || rm -rf "${profileDir}"
    else
        echo "Profile ${profileName} already exists at ${profileDir}."
        "${startFFCmd}" -P "${profileName}"
    fi
}

ffprfl_getExistingProfileNamed() {
    local profileName="${1}"
    local profileDir="$( cd ~/.mozilla/firefox ; pwd )"
    awk -v ProfileName="${profileName}" -v ProfileDir="${profileDir}" -F '' '
BEGIN { matched = 0 ; relative = 0; profilePath = "" }
/^\[.*\]$/ { if (matched) { exit } else { matched = 0 ; relative = 0 ; profilePath = "" } }
/^Name=/ { if ($0 == "Name=" ProfileName) matched = 1 }
/^Default=1$/ { if ("" == ProfileName) matched = 1 }
/^IsRelative=1$/ { relative = 1 }
/^Path=/ { if (sub(/^Path=/, "")) { profilePath = $0 } }
END {
    if (matched) {
        if (relative) {
            print profileDir "/" profilePath
        } else {
            print profilePath
        }
    } ; exit !matched }' \
            "${profileDir}/profiles.ini"
}

ffprfl_prepareFFProfile() {
    local profileName=${1:-temp}    #profileName can't have space; CreateProfile doesn't handle it
    local callbackCmd="${2}"
    type "${2}" >/dev/null || callbackCmd=""
    shift 2

    local profileDir created=
    { profileDir="$( ffprfl_getExistingProfileNamed "${profileName}" )" && \
        [ -d "${profileDir}" ] ; } || {
            profileDir="$( mktemp -d -p "${profileParentDir}" "${profileNamePrefix}${profileName}_XXXXXX" )"
            firefox -CreateProfile "${profileName} ${profileDir}" && created=1
        }
    [ -n "${callbackCmd}" ] && "${callbackCmd}" "${created}" "${profileName}" "${profileDir}" "$@"
}

# use case: start FF with profile (create it with fixed prefs.js if not there)
# ffprfl_doPrefs come from prefs.sh
#. setup_profile.sh
#. prefs.sh
#startURL=https://example.com/ \
#    persistentProfile=1 \
#    startFFCmd=startFF \
#    ffprfl_prepareFFProfile example_profile_name ffprfl_startWithProfile ffprfl_doPrefs

# use case: fix prefs.js of existing profile
#. setup_profile.sh
#. prefs.sh
#. prefs_personal.sh
#fixProfilePrefs() {
#    local profileName="${1}"
#    local profileDir="$( ffprfl_getExistingProfileNamed "${profileName}" )"
#    doPrefs() {
#        ffprfl_doPrefs "$@"
#        ffprfl_doPrefsPersonal "$@"
#    }
#    ffprfl_overridePrefs "${profileDir}" doPrefs
#}
#
#fixProfilePrefs example_profile_name
