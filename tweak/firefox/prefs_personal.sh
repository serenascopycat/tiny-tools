# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Sample preferences listing to be used with setup_profile.sh.
# Firefox likes to pop privacy notice for new profiles,
# which is every time after starting a fresh live CD run.
# Don't want to burn network bandwidth on it repeatedly.
# Also, Linux Mint likes to rub in the fact that
# the old live CD is unsupported.
### HOWTO ##############################
# 1) Source this file
# 2) Customize preferences in ffprfl_doPrefsPersonal()
# 3) Use with setup_profile.sh
### LICENSING ##########################
# Copycenter 2022 Serena's Copycat; licensed under CC0

# set -e

# personal preferences
ffprfl_doPrefsPersonal() {
    local callbackCmd="${1}"

    # foreground/background colors affect about:blank
    ffprfl_forEachPref "${callbackCmd}" '' \
        extensions.activeThemeID = '"firefox-compact-dark@mozilla.org"' \
        devtools.theme = '"dark"' \
        reader.color_scheme = '"dark"' \
        browser.display.background_color = '"#222222"' \
        browser.display.foreground_color = '"#CCCCCC"'

    # could use system colors instead but colors defined above will be ignored
    #    browser.display.use_system_colors = 'true'

    # https://support.mozilla.org/en-US/questions/1067915
    "${callbackCmd}" \
        browser.tabs.closeWindowWithLastTab \
        'false'

    # set to false to reduce nag
    "${callbackCmd}" \
        browser.aboutConfig.showWarning \
        'true'

#    "${callbackCmd}" \
#        browser.newtabpage.activity-stream.default.sites \
#        '"https://duckduckgo.com"'

    "${callbackCmd}" \
        browser.shell.checkDefaultBrowser \
        'false'
}
