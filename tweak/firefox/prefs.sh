# to be sourced (/bin/sh)
### WHAT THIS DOES #####################
# Sample preferences listing to be used with setup_profile.sh.
# Firefox likes to pop privacy notice for new profiles,
# which is every time after starting a fresh live CD run.
# Don't want to burn network bandwidth on it repeatedly.
# Also, Linux Mint likes to rub in the fact that
# the old live CD is unsupported.
### HOWTO ##############################
# 1) Source this file
# 2) Customize preferences in ffprfl_doPrefs()
# 3) Use with setup_profile.sh
### LICENSING ##########################
# Copycenter 2022 Serena's Copycat; licensed under CC0

# set -e

ffprfl_doPrefs() {
    local callbackCmd="${1}"

    # change to no-op command (e.g. true) to bypass entire section
    # avoid saving [junk/sensitive] data
    local callbackCmdOffRecord="${callbackCmd}"
    # reduce network activity (also helps prevent detection/hijack)
    local callbackCmdNetReduce="${callbackCmd}"
    # reduce UI distractions
    local callbackCmdUIFocus="${callbackCmd}"
    # reduce messages
    local callbackCmdNagReduce="${callbackCmdUIFocus}"

    # General locations for documentation about what the preferences do
    # https://hg.mozilla.org/mozilla-central/file/502619bc75a86049e772eb9f85ffd1c32aad3a30/browser/app/profile/firefox.js
    # http://kb.mozillazine.org/About:config_entries

    # TODO Check these preferences
    #   browser.discovery.enabled
    #   browser.uitour.enabled

    # https://support.mozilla.org/en-US/questions/1158069
    # http://kb.mozillazine.org/Signon.autofillForms
    ffprfl_forEachPref "${callbackCmdOffRecord}" 'false' \
        browser.formfill.enable \
        signon.autofillForms \
        signon.rememberSignons

    # http://kb.mozillazine.org/App.update.enabled
    # http://kb.mozillazine.org/App.update.auto
    # stop updates, esp. on live CDs where they'll be lost anyway
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        app.update.enabled \
        app.update.auto
    # https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_add-on-metadata-updating
    "${callbackCmdNetReduce}" \
        extensions.getAddons.cache.enabled 'false'
    # browser/app/profile/firefox.js (general locations; above)
    "${callbackCmdNetReduce}" \
        extensions.systemAddon.update.enabled 'false'
    # https://support.mozilla.org/en-US/questions/952162
    # Add-ons Manager: Update Add-ons Automatically
    # auto-install if update detected
    "${callbackCmdNetReduce}" \
        extensions.update.autoUpdateDefault 'false'
    # https://blog.mozilla.org/addons/how-to-turn-off-add-on-updates/
    # check for updates to extensions
    "${callbackCmdNetReduce}" \
        extensions.update.enabled 'false'

    # https://wiki.mozilla.org/Security/Features/Application_Reputation_Design_Doc#How_to_turn_off_this_feature
    # Windows-only, but setting it anyways
    # only disable the remote check; keep doing the local checks
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        browser.safebrowsing.downloads.remote.enabled

    # may be slower with this off, but on very limited connections burning bandwidth is even worse
    "${callbackCmdNetReduce}" \
        network.predictor.enabled \
        'false'

    # https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections#w_network-detection
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        network.captive-portal-service.enabled \
        network.connectivity-service.enabled

    # https://stackoverflow.com/questions/47352151/prevent-firefox-from-opening-privacy-notice-when-running-web-ext#47353456
    ffprfl_forEachPref "${callbackCmdNetReduce}" '' \
        datareporting.policy.firstRunURL = '""' \
        browser.startup.page = '0' \
        browser.startup.homepage = '"about:blank"'

    # https://www.ghacks.net/overview-firefox-aboutconfig-security-privacy-preferences/
    # possible for some sites to depend on beacon.enabled
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        beacon.enabled \
        browser.send_pings

    # https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/internals/preferences.html
    # setting datareporting.policy.dataSubmissionEnabled to false should render the rest extraneous
    # but setting them anyways in case it got flipped by accident
    # datareporting.healthreport.uploadEnabled not used because toolkit.telemetry.unified is false
    # however, it is the preference "Allow Firefox to send technical and interaction data to Mozilla"
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        datareporting.policy.dataSubmissionEnabled \
        datareporting.healthreport.uploadEnabled \
        toolkit.telemetry.enabled \
        toolkit.telemetry.unified \
        toolkit.telemetry.shutdownPingSender.enabled \
        toolkit.telemetry.shutdownPingSender.enabledFirstSession \
        toolkit.telemetry.firstShutdownPing.enabled \
        toolkit.telemetry.newProfilePing.enabled \
        toolkit.telemetry.updatePing.enabled \
        toolkit.telemetry.prioping.enabled
    "${callbackCmdNagReduce}" \
        datareporting.policy.dataSubmissionPolicyBypassNotification	'true'
    # Allow Firefox to send backlogged crash reports on your behalf
    "${callbackCmdNetReduce}" \
        browser.crashReports.unsubmittedCheck.autoSubmit2 'false'

    # https://support.mozilla.org/en-US/kb/mozillacrashreporter
    # default setting for checkbox for whether crash report is sent?
    "${callbackCmdNetReduce}" \
        browser.tabs.crashReporting.sendReport 'false'

    # https://mozilla.github.io/normandy/user/end_user_interaction.html
    # don't spend bandwidth on Mozilla-initiated control
    # optoutstudies.enabled is confusing: true = enable studies, false = opt out
    # available in regular preferences as "Allow Firefox to install and run studies"
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        app.normandy.enabled \
        app.shield.optoutstudies.enabled

    # https://firefox-source-docs.mozilla.org/browser/urlbar/preferences.html
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        browser.urlbar.speculativeConnect.enabled
    ffprfl_forEachPref "${callbackCmdUIFocus}" 'false' \
        browser.urlbar.suggest.history \
        browser.urlbar.suggest.searches \
        browser.urlbar.suggest.topsites
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        browser.search.suggest.enabled \
        browser.search.update
    # prevent searching accidental entries into URL bar
    "${callbackCmdNetReduce}" \
        keyword.enabled 'false'

    # just turn off the new tab page
    "${callbackCmdUIFocus}" \
        browser.newtabpage.enabled 'false'
    # turning off new tab page above makes the newtabpage settings below moot
    # but change them anyways in case manually visiting about:home
    # https://support.mozilla.org/en-US/kb/how-stop-firefox-making-automatic-connections
    ffprfl_forEachPref "${callbackCmdNetReduce}" 'false' \
        browser.newtabpage.activity-stream.feeds.asrouterfeed
    ffprfl_forEachPref "${callbackCmdUIFocus}" 'false' \
        browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons \
        browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features \
        browser.newtabpage.activity-stream.feeds.section.highlights \
        browser.newtabpage.activity-stream.feeds.snippets \
        browser.newtabpage.activity-stream.feeds.topsites \
        browser.newtabpage.activity-stream.section.highlights.includeDownloads \
        browser.newtabpage.activity-stream.section.highlights.includePocket \
        browser.newtabpage.activity-stream.section.highlights.includeVisited \
        browser.newtabpage.activity-stream.showSearch \
        browser.startup.homepage_override.mstone = '"ignore"'

    # mark notice as shown already:
    #  Change to extensions in Private Windows
    #  (default extensions [un]availability in private window)
    "${callbackCmdNagReduce}" \
        extensions.privatebrowsing.notification 'true'

    # browser/app/profile/firefox.js (general locations; above)
    "${callbackCmdNagReduce}" \
        app.update.checkInstallTime 'false'

}
